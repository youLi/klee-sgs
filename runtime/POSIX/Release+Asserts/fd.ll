; ModuleID = 'fd.c'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-n8:16:32"
target triple = "i386-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i32, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i32, i32, [40 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.__fsid_t = type { [2 x i32] }
%struct.dirent64 = type { i64, i64, i16, i8, [256 x i8] }
%struct.exe_disk_file_t = type { i32, i8*, %struct.stat64* }
%struct.exe_file_system_t = type { i32, %struct.exe_disk_file_t*, %struct.exe_disk_file_t*, i32, %struct.exe_disk_file_t*, i32, i32*, i32*, i32*, i32*, i32*, i32*, i32* }
%struct.exe_file_t = type { i32, i32, i64, %struct.exe_disk_file_t* }
%struct.exe_sym_env_t = type { [32 x %struct.exe_file_t], i32, i32, i32 }
%struct.fd_set = type { [32 x i32] }
%struct.stat64 = type { i64, i32, i32, i32, i32, i32, i32, i64, i32, i64, i32, i64, %struct.timespec, %struct.timespec, %struct.timespec, i64 }
%struct.statfs = type { i32, i32, i32, i32, i32, i32, i32, %struct.__fsid_t, i32, i32, i32, [4 x i32] }
%struct.timespec = type { i32, i32 }

@__exe_fs = external global %struct.exe_file_system_t
@__exe_env = external global %struct.exe_sym_env_t
@.str = private constant [18 x i8] c"ignoring (ENOENT)\00", align 1
@.str1 = private constant [17 x i8] c"ignoring (EPERM)\00", align 1
@.str2 = private constant [32 x i8] c"symbolic file, ignoring (EPERM)\00", align 4
@.str3 = private constant [32 x i8] c"symbolic file, ignoring (EBADF)\00", align 4
@n_calls.4755 = internal global i32 0
@.str4 = private constant [30 x i8] c"symbolic file, ignoring (EIO)\00", align 1
@.str5 = private constant [33 x i8] c"symbolic file, ignoring (ENOENT)\00", align 4
@n_calls.5273 = internal global i32 0
@n_calls.4387 = internal global i32 0
@.str6 = private constant [33 x i8] c"symbolic file, ignoring (EINVAL)\00", align 4
@.str7 = private constant [41 x i8] c"(TCGETS) symbolic file, incomplete model\00", align 4
@.str8 = private constant [42 x i8] c"(TCSETS) symbolic file, silently ignoring\00", align 4
@.str9 = private constant [43 x i8] c"(TCSETSW) symbolic file, silently ignoring\00", align 4
@.str10 = private constant [43 x i8] c"(TCSETSF) symbolic file, silently ignoring\00", align 4
@.str11 = private constant [45 x i8] c"(TIOCGWINSZ) symbolic file, incomplete model\00", align 4
@.str12 = private constant [46 x i8] c"(TIOCSWINSZ) symbolic file, ignoring (EINVAL)\00", align 4
@.str13 = private constant [43 x i8] c"(FIONREAD) symbolic file, incomplete model\00", align 4
@.str14 = private constant [44 x i8] c"(MTIOCGET) symbolic file, ignoring (EINVAL)\00", align 4
@.str15 = private constant [18 x i8] c"s != (off64_t) -1\00", align 1
@.str16 = private constant [5 x i8] c"fd.c\00", align 1
@__PRETTY_FUNCTION__.4792 = internal constant [14 x i8] c"__fd_getdents\00"
@.str17 = private constant [18 x i8] c"new_off == f->off\00", align 1
@__PRETTY_FUNCTION__.4536 = internal constant [11 x i8] c"__fd_lseek\00"
@n_calls.4407 = internal global i32 0
@.str18 = private constant [12 x i8] c"f->off >= 0\00", align 1
@__PRETTY_FUNCTION__.4410 = internal constant [5 x i8] c"read\00"
@n_calls.4662 = internal global i32 0
@n_calls.4639 = internal global i32 0
@n_calls.4466 = internal global i32 0
@.str19 = private constant [7 x i8] c"r >= 0\00", align 1
@__PRETTY_FUNCTION__.4469 = internal constant [6 x i8] c"write\00"
@.str20 = private constant [2 x i8] c"0\00", align 1
@stderr = external global %struct._IO_FILE*
@.str21 = private constant [33 x i8] c"WARNING: write() ignores bytes.\0A\00", align 4
@.str22 = private constant [47 x i8] c"Undefined call to open(): O_EXCL w/o O_RDONLY\0A\00", align 4

define i32 @access(i8* %pathname, i32 %mode) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !345), !dbg !545
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !346), !dbg !545
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !230), !dbg !546
  %0 = load i8* %pathname, align 1, !dbg !548
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !548
  %1 = icmp eq i8 %0, 0, !dbg !549
  br i1 %1, label %bb1, label %bb.i, !dbg !549

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %pathname, i32 1, !dbg !549
  %3 = load i8* %2, align 1, !dbg !549
  %4 = icmp eq i8 %3, 0, !dbg !549
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !549

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !550
  %6 = sext i8 %0 to i32, !dbg !551
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !551
  %9 = icmp eq i32 %6, %8, !dbg !551
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !551

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !552
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !552
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !554
  %12 = load %struct.stat64** %11, align 4, !dbg !554
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !554
  %14 = load i64* %13, align 4, !dbg !554
  %15 = icmp eq i64 %14, 0, !dbg !554
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !554

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !550
  br label %bb8.i, !dbg !550

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !550
  br i1 %18, label %bb3.i, label %bb1, !dbg !550

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !552
  %phitmp = icmp eq %struct.exe_disk_file_t* %19, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !347), !dbg !547
  br i1 %phitmp, label %bb1, label %bb4, !dbg !555

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !312) nounwind, !dbg !556
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !302) nounwind, !dbg !558
  %20 = ptrtoint i8* %pathname to i32, !dbg !560
  %21 = tail call i32 @klee_get_valuel(i32 %20) nounwind, !dbg !560
  %22 = inttoptr i32 %21 to i8*, !dbg !560
  tail call void @llvm.dbg.value(metadata !{i8* %22}, i64 0, metadata !303) nounwind, !dbg !560
  %23 = icmp eq i8* %22, %pathname, !dbg !561
  %24 = zext i1 %23 to i32, !dbg !561
  tail call void @klee_assume(i32 %24) nounwind, !dbg !561
  tail call void @llvm.dbg.value(metadata !{i8* %22}, i64 0, metadata !313) nounwind, !dbg !559
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !563
  br label %bb.i6, !dbg !563

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %22, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %25 = phi i32 [ 0, %bb1 ], [ %37, %bb6.i8 ]
  %tmp.i = add i32 %25, -1
  %26 = load i8* %sc.1.i, align 1, !dbg !564
  %27 = and i32 %tmp.i, %25, !dbg !565
  %28 = icmp eq i32 %27, 0, !dbg !565
  br i1 %28, label %bb1.i, label %bb5.i, !dbg !565

bb1.i:                                            ; preds = %bb.i6
  switch i8 %26, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %26}, i64 0, metadata !316) nounwind, !dbg !564
  store i8 0, i8* %sc.1.i, align 1, !dbg !566
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !566
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !567
  %29 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !567
  br label %bb6.i8, !dbg !567

bb5.i:                                            ; preds = %bb.i6
  %30 = sext i8 %26 to i32, !dbg !568
  %31 = tail call i32 @klee_get_valuel(i32 %30) nounwind, !dbg !568
  %32 = trunc i32 %31 to i8, !dbg !568
  %33 = icmp eq i8 %32, %26, !dbg !569
  %34 = zext i1 %33 to i32, !dbg !569
  tail call void @klee_assume(i32 %34) nounwind, !dbg !569
  store i8 %32, i8* %sc.1.i, align 1, !dbg !570
  %35 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !570
  %36 = icmp eq i8 %32, 0, !dbg !571
  br i1 %36, label %__concretize_string.exit, label %bb6.i8, !dbg !571

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %29, %bb4.i7 ], [ %35, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %37 = add i32 %25, 1, !dbg !563
  br label %bb.i6, !dbg !563

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %38 = tail call i32 (i32, ...)* @syscall(i32 33, i8* %pathname, i32 %mode) nounwind, !dbg !557
  tail call void @llvm.dbg.value(metadata !{i32 %38}, i64 0, metadata !349), !dbg !557
  %39 = icmp eq i32 %38, -1, !dbg !572
  br i1 %39, label %bb2, label %bb4, !dbg !572

bb2:                                              ; preds = %__concretize_string.exit
  %40 = tail call i32* @__errno_location() nounwind readnone, !dbg !573
  %41 = tail call i32 @klee_get_errno() nounwind, !dbg !573
  store i32 %41, i32* %40, align 4, !dbg !573
  ret i32 %38, !dbg !574

bb4:                                              ; preds = %__concretize_string.exit, %__get_sym_file.exit
  %.0 = phi i32 [ 0, %__get_sym_file.exit ], [ %38, %__concretize_string.exit ]
  ret i32 %.0, !dbg !574
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define i32 @umask(i32 %mask) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %mask}, i64 0, metadata !240), !dbg !575
  %0 = load i32* getelementptr inbounds (%struct.exe_sym_env_t* @__exe_env, i32 0, i32 1), align 4, !dbg !576
  tail call void @llvm.dbg.value(metadata !{i32 %0}, i64 0, metadata !241), !dbg !576
  %1 = and i32 %mask, 511, !dbg !577
  store i32 %1, i32* getelementptr inbounds (%struct.exe_sym_env_t* @__exe_env, i32 0, i32 1), align 4, !dbg !577
  ret i32 %0, !dbg !578
}

define i32 @chroot(i8* nocapture %path) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !249), !dbg !579
  %0 = load i8* %path, align 1, !dbg !580
  switch i8 %0, label %bb4 [
    i8 0, label %bb
    i8 47, label %bb2
  ]

bb:                                               ; preds = %entry
  %1 = tail call i32* @__errno_location() nounwind readnone, !dbg !582
  store i32 2, i32* %1, align 4, !dbg !582
  ret i32 -1, !dbg !583

bb2:                                              ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !584
  %3 = load i8* %2, align 1, !dbg !584
  %4 = icmp eq i8 %3, 0, !dbg !584
  br i1 %4, label %bb5, label %bb4, !dbg !584

bb4:                                              ; preds = %entry, %bb2
  tail call void @klee_warning(i8* getelementptr inbounds ([18 x i8]* @.str, i32 0, i32 0)) nounwind, !dbg !585
  %5 = tail call i32* @__errno_location() nounwind readnone, !dbg !586
  store i32 2, i32* %5, align 4, !dbg !586
  ret i32 -1, !dbg !583

bb5:                                              ; preds = %bb2
  ret i32 0, !dbg !583
}

declare i32* @__errno_location() nounwind readnone

declare void @klee_warning(i8*)

define i32 @unlink(i8* nocapture %pathname) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !250), !dbg !587
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !230), !dbg !588
  %0 = load i8* %pathname, align 1, !dbg !590
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !590
  %1 = icmp eq i8 %0, 0, !dbg !591
  br i1 %1, label %bb5, label %bb.i, !dbg !591

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %pathname, i32 1, !dbg !591
  %3 = load i8* %2, align 1, !dbg !591
  %4 = icmp eq i8 %3, 0, !dbg !591
  br i1 %4, label %bb8.preheader.i, label %bb5, !dbg !591

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !592
  %6 = sext i8 %0 to i32, !dbg !593
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !593
  %9 = icmp eq i32 %6, %8, !dbg !593
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !593

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !594
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !594
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !595
  %12 = load %struct.stat64** %11, align 4, !dbg !595
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !595
  %14 = load i64* %13, align 4, !dbg !595
  %15 = icmp eq i64 %14, 0, !dbg !595
  br i1 %15, label %bb5, label %__get_sym_file.exit, !dbg !595

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !592
  br label %bb8.i, !dbg !592

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !592
  br i1 %18, label %bb3.i, label %bb5, !dbg !592

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !594
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %19}, i64 0, metadata !251), !dbg !589
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !596
  br i1 %20, label %bb5, label %bb, !dbg !596

bb:                                               ; preds = %__get_sym_file.exit
  %21 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 3, !dbg !597
  %22 = load i32* %21, align 4, !dbg !597
  %23 = and i32 %22, 61440, !dbg !597
  %24 = icmp eq i32 %23, 32768, !dbg !597
  br i1 %24, label %bb1, label %bb2, !dbg !597

bb1:                                              ; preds = %bb
  store i64 0, i64* %13, align 4, !dbg !598
  ret i32 0, !dbg !599

bb2:                                              ; preds = %bb
  %25 = icmp eq i32 %23, 16384, !dbg !600
  %26 = tail call i32* @__errno_location() nounwind readnone, !dbg !601
  br i1 %25, label %bb3, label %bb4, !dbg !600

bb3:                                              ; preds = %bb2
  store i32 21, i32* %26, align 4, !dbg !601
  ret i32 -1, !dbg !599

bb4:                                              ; preds = %bb2
  store i32 1, i32* %26, align 4, !dbg !602
  ret i32 -1, !dbg !599

bb5:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @klee_warning(i8* getelementptr inbounds ([17 x i8]* @.str1, i32 0, i32 0)) nounwind, !dbg !603
  %27 = tail call i32* @__errno_location() nounwind readnone, !dbg !604
  store i32 1, i32* %27, align 4, !dbg !604
  ret i32 -1, !dbg !599
}

define i32 @rmdir(i8* nocapture %pathname) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !253), !dbg !605
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !230), !dbg !606
  %0 = load i8* %pathname, align 1, !dbg !608
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !608
  %1 = icmp eq i8 %0, 0, !dbg !609
  br i1 %1, label %bb3, label %bb.i, !dbg !609

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %pathname, i32 1, !dbg !609
  %3 = load i8* %2, align 1, !dbg !609
  %4 = icmp eq i8 %3, 0, !dbg !609
  br i1 %4, label %bb8.preheader.i, label %bb3, !dbg !609

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !610
  %6 = sext i8 %0 to i32, !dbg !611
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !611
  %9 = icmp eq i32 %6, %8, !dbg !611
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !611

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !612
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !612
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !613
  %12 = load %struct.stat64** %11, align 4, !dbg !613
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !613
  %14 = load i64* %13, align 4, !dbg !613
  %15 = icmp eq i64 %14, 0, !dbg !613
  br i1 %15, label %bb3, label %__get_sym_file.exit, !dbg !613

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !610
  br label %bb8.i, !dbg !610

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !610
  br i1 %18, label %bb3.i, label %bb3, !dbg !610

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !612
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %19}, i64 0, metadata !254), !dbg !607
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !614
  br i1 %20, label %bb3, label %bb, !dbg !614

bb:                                               ; preds = %__get_sym_file.exit
  %21 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 3, !dbg !615
  %22 = load i32* %21, align 4, !dbg !615
  %23 = and i32 %22, 61440, !dbg !615
  %24 = icmp eq i32 %23, 16384, !dbg !615
  br i1 %24, label %bb1, label %bb2, !dbg !615

bb1:                                              ; preds = %bb
  store i64 0, i64* %13, align 4, !dbg !616
  ret i32 0, !dbg !617

bb2:                                              ; preds = %bb
  %25 = tail call i32* @__errno_location() nounwind readnone, !dbg !618
  store i32 20, i32* %25, align 4, !dbg !618
  ret i32 -1, !dbg !617

bb3:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @klee_warning(i8* getelementptr inbounds ([17 x i8]* @.str1, i32 0, i32 0)) nounwind, !dbg !619
  %26 = tail call i32* @__errno_location() nounwind readnone, !dbg !620
  store i32 1, i32* %26, align 4, !dbg !620
  ret i32 -1, !dbg !617
}

define i32 @readlink(i8* %path, i8* %buf, i32 %bufsize) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !259), !dbg !621
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !260), !dbg !621
  tail call void @llvm.dbg.value(metadata !{i32 %bufsize}, i64 0, metadata !261), !dbg !621
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !622
  %0 = load i8* %path, align 1, !dbg !624
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !624
  %1 = icmp eq i8 %0, 0, !dbg !625
  br i1 %1, label %bb12, label %bb.i, !dbg !625

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !625
  %3 = load i8* %2, align 1, !dbg !625
  %4 = icmp eq i8 %3, 0, !dbg !625
  br i1 %4, label %bb8.preheader.i, label %bb12, !dbg !625

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !626
  %6 = sext i8 %0 to i32, !dbg !627
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !627
  %9 = icmp eq i32 %6, %8, !dbg !627
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !627

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !628
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !628
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !629
  %12 = load %struct.stat64** %11, align 4, !dbg !629
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !629
  %14 = load i64* %13, align 4, !dbg !629
  %15 = icmp eq i64 %14, 0, !dbg !629
  br i1 %15, label %bb12, label %__get_sym_file.exit, !dbg !629

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !626
  br label %bb8.i, !dbg !626

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !626
  br i1 %18, label %bb3.i, label %bb12, !dbg !626

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !628
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %19}, i64 0, metadata !262), !dbg !623
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !630
  br i1 %20, label %bb12, label %bb, !dbg !630

bb:                                               ; preds = %__get_sym_file.exit
  %21 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 3, !dbg !631
  %22 = load i32* %21, align 4, !dbg !631
  %23 = and i32 %22, 61440, !dbg !631
  %24 = icmp eq i32 %23, 40960, !dbg !631
  br i1 %24, label %bb1, label %bb11, !dbg !631

bb1:                                              ; preds = %bb
  store i8 %0, i8* %buf, align 1, !dbg !632
  %25 = icmp ugt i32 %bufsize, 1, !dbg !633
  br i1 %25, label %bb3, label %bb9, !dbg !633

bb3:                                              ; preds = %bb1
  %26 = getelementptr inbounds i8* %buf, i32 1, !dbg !633
  store i8 46, i8* %26, align 1, !dbg !633
  %27 = icmp ugt i32 %bufsize, 2, !dbg !634
  br i1 %27, label %bb5, label %bb9, !dbg !634

bb5:                                              ; preds = %bb3
  %28 = getelementptr inbounds i8* %buf, i32 2, !dbg !634
  store i8 108, i8* %28, align 1, !dbg !634
  %29 = icmp ugt i32 %bufsize, 3, !dbg !635
  br i1 %29, label %bb7, label %bb9, !dbg !635

bb7:                                              ; preds = %bb5
  %30 = getelementptr inbounds i8* %buf, i32 3, !dbg !635
  store i8 110, i8* %30, align 1, !dbg !635
  %31 = icmp ugt i32 %bufsize, 4, !dbg !636
  br i1 %31, label %bb8, label %bb9, !dbg !636

bb8:                                              ; preds = %bb7
  %32 = getelementptr inbounds i8* %buf, i32 4, !dbg !636
  store i8 107, i8* %32, align 1, !dbg !636
  br label %bb9, !dbg !636

bb9:                                              ; preds = %bb1, %bb3, %bb5, %bb8, %bb7
  %33 = icmp ugt i32 %bufsize, 5
  %min = select i1 %33, i32 5, i32 %bufsize, !dbg !637
  ret i32 %min, !dbg !637

bb11:                                             ; preds = %bb
  %34 = tail call i32* @__errno_location() nounwind readnone, !dbg !638
  store i32 22, i32* %34, align 4, !dbg !638
  ret i32 -1, !dbg !637

bb12:                                             ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  %35 = tail call i32 (i32, ...)* @syscall(i32 85, i8* %path, i8* %buf, i32 %bufsize) nounwind, !dbg !639
  tail call void @llvm.dbg.value(metadata !{i32 %35}, i64 0, metadata !264), !dbg !639
  %36 = icmp eq i32 %35, -1, !dbg !640
  br i1 %36, label %bb13, label %bb15, !dbg !640

bb13:                                             ; preds = %bb12
  %37 = tail call i32* @__errno_location() nounwind readnone, !dbg !641
  %38 = tail call i32 @klee_get_errno() nounwind, !dbg !641
  store i32 %38, i32* %37, align 4, !dbg !641
  ret i32 %35, !dbg !637

bb15:                                             ; preds = %bb12
  ret i32 %35, !dbg !637
}

declare i32 @syscall(i32, ...) nounwind

declare i32 @klee_get_errno()

define i32 @fsync(i32 %fd) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !266), !dbg !642
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !643
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !645

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !646
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !647
  %3 = and i32 %2, 1, !dbg !647
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !647

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !267), !dbg !644
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !648
  br i1 %5, label %bb, label %bb1, !dbg !648

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !649
  store i32 9, i32* %6, align 4, !dbg !649
  ret i32 -1, !dbg !650

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !651
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !651
  br i1 %9, label %bb3, label %bb6, !dbg !651

bb3:                                              ; preds = %bb1
  %10 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %11 = load i32* %10, align 4, !dbg !652
  %12 = tail call i32 (i32, ...)* @syscall(i32 118, i32 %11) nounwind, !dbg !652
  tail call void @llvm.dbg.value(metadata !{i32 %12}, i64 0, metadata !269), !dbg !652
  %13 = icmp eq i32 %12, -1, !dbg !653
  br i1 %13, label %bb4, label %bb6, !dbg !653

bb4:                                              ; preds = %bb3
  %14 = tail call i32* @__errno_location() nounwind readnone, !dbg !654
  %15 = tail call i32 @klee_get_errno() nounwind, !dbg !654
  store i32 %15, i32* %14, align 4, !dbg !654
  ret i32 %12, !dbg !650

bb6:                                              ; preds = %bb3, %bb1
  %.0 = phi i32 [ 0, %bb1 ], [ %12, %bb3 ]
  ret i32 %.0, !dbg !650
}

define i32 @fstatfs(i32 %fd, %struct.statfs* %buf) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !271), !dbg !655
  tail call void @llvm.dbg.value(metadata !{%struct.statfs* %buf}, i64 0, metadata !272), !dbg !655
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !656
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !658

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !659
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !660
  %3 = and i32 %2, 1, !dbg !660
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !660

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !273), !dbg !657
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !661
  br i1 %5, label %bb, label %bb1, !dbg !661

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !662
  store i32 9, i32* %6, align 4, !dbg !662
  ret i32 -1, !dbg !663

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !664
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !664
  br i1 %9, label %bb3, label %bb2, !dbg !664

bb2:                                              ; preds = %bb1
  tail call void @klee_warning(i8* getelementptr inbounds ([32 x i8]* @.str3, i32 0, i32 0)) nounwind, !dbg !665
  %10 = tail call i32* @__errno_location() nounwind readnone, !dbg !666
  store i32 9, i32* %10, align 4, !dbg !666
  ret i32 -1, !dbg !663

bb3:                                              ; preds = %bb1
  %11 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %12 = load i32* %11, align 4, !dbg !667
  %13 = tail call i32 (i32, ...)* @syscall(i32 100, i32 %12, %struct.statfs* %buf) nounwind, !dbg !667
  tail call void @llvm.dbg.value(metadata !{i32 %13}, i64 0, metadata !275), !dbg !667
  %14 = icmp eq i32 %13, -1, !dbg !668
  br i1 %14, label %bb4, label %bb6, !dbg !668

bb4:                                              ; preds = %bb3
  %15 = tail call i32* @__errno_location() nounwind readnone, !dbg !669
  %16 = tail call i32 @klee_get_errno() nounwind, !dbg !669
  store i32 %16, i32* %15, align 4, !dbg !669
  ret i32 %13, !dbg !663

bb6:                                              ; preds = %bb3
  ret i32 %13, !dbg !663
}

define i32 @__fd_ftruncate(i32 %fd, i64 %length) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !277), !dbg !670
  tail call void @llvm.dbg.value(metadata !{i64 %length}, i64 0, metadata !278), !dbg !670
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !671
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %__get_file.exit.thread, !dbg !673

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !674
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !675
  %3 = and i32 %2, 1, !dbg !675
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %__get_file.exit.thread, label %__get_file.exit, !dbg !675

__get_file.exit.thread:                           ; preds = %bb.i, %entry
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %6}, i64 0, metadata !279), !dbg !672
  %4 = load i32* @n_calls.4755, align 4, !dbg !676
  %5 = add nsw i32 %4, 1, !dbg !676
  store i32 %5, i32* @n_calls.4755, align 4, !dbg !676
  br label %bb

__get_file.exit:                                  ; preds = %bb.i
  %6 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %6}, i64 0, metadata !279), !dbg !672
  %7 = load i32* @n_calls.4755, align 4, !dbg !676
  %8 = add nsw i32 %7, 1, !dbg !676
  store i32 %8, i32* @n_calls.4755, align 4, !dbg !676
  %9 = icmp eq %struct.exe_file_t* %6, null, !dbg !677
  br i1 %9, label %bb, label %bb1, !dbg !677

bb:                                               ; preds = %__get_file.exit.thread, %__get_file.exit
  %10 = tail call i32* @__errno_location() nounwind readnone, !dbg !678
  store i32 9, i32* %10, align 4, !dbg !678
  ret i32 -1, !dbg !679

bb1:                                              ; preds = %__get_file.exit
  %11 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !680
  %12 = icmp eq i32 %11, 0, !dbg !680
  br i1 %12, label %bb4, label %bb2, !dbg !680

bb2:                                              ; preds = %bb1
  %13 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 9), align 4, !dbg !680
  %14 = load i32* %13, align 4, !dbg !680
  %15 = icmp eq i32 %14, %8, !dbg !680
  br i1 %15, label %bb3, label %bb4, !dbg !680

bb3:                                              ; preds = %bb2
  %16 = add i32 %11, -1
  store i32 %16, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !681
  %17 = tail call i32* @__errno_location() nounwind readnone, !dbg !682
  store i32 5, i32* %17, align 4, !dbg !682
  ret i32 -1, !dbg !679

bb4:                                              ; preds = %bb1, %bb2
  %18 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %19 = load %struct.exe_disk_file_t** %18, align 4, !dbg !683
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !683
  br i1 %20, label %bb6, label %bb5, !dbg !683

bb5:                                              ; preds = %bb4
  tail call void @klee_warning(i8* getelementptr inbounds ([30 x i8]* @.str4, i32 0, i32 0)) nounwind, !dbg !684
  %21 = tail call i32* @__errno_location() nounwind readnone, !dbg !685
  store i32 5, i32* %21, align 4, !dbg !685
  ret i32 -1, !dbg !679

bb6:                                              ; preds = %bb4
  %22 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %23 = load i32* %22, align 4, !dbg !686
  %24 = tail call i32 (i32, ...)* @syscall(i32 194, i32 %23, i64 %length) nounwind, !dbg !686
  tail call void @llvm.dbg.value(metadata !{i32 %24}, i64 0, metadata !281), !dbg !686
  %25 = icmp eq i32 %24, -1, !dbg !687
  br i1 %25, label %bb7, label %bb9, !dbg !687

bb7:                                              ; preds = %bb6
  %26 = tail call i32* @__errno_location() nounwind readnone, !dbg !688
  %27 = tail call i32 @klee_get_errno() nounwind, !dbg !688
  store i32 %27, i32* %26, align 4, !dbg !688
  ret i32 %24, !dbg !679

bb9:                                              ; preds = %bb6
  ret i32 %24, !dbg !679
}

define i32 @fchown(i32 %fd, i32 %owner, i32 %group) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !290), !dbg !689
  tail call void @llvm.dbg.value(metadata !{i32 %owner}, i64 0, metadata !291), !dbg !689
  tail call void @llvm.dbg.value(metadata !{i32 %group}, i64 0, metadata !292), !dbg !689
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !690
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !692

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !693
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !694
  %3 = and i32 %2, 1, !dbg !694
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !694

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !293), !dbg !691
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !695
  br i1 %5, label %bb, label %bb1, !dbg !695

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !696
  store i32 9, i32* %6, align 4, !dbg !696
  ret i32 -1, !dbg !697

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !698
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !698
  br i1 %9, label %bb3, label %bb2, !dbg !698

bb2:                                              ; preds = %bb1
  tail call void @llvm.dbg.value(metadata !699, i64 0, metadata !256) nounwind, !dbg !700
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !257) nounwind, !dbg !700
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !258) nounwind, !dbg !700
  tail call void @klee_warning(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0)) nounwind, !dbg !702
  %10 = tail call i32* @__errno_location() nounwind readnone, !dbg !704
  store i32 1, i32* %10, align 4, !dbg !704
  ret i32 -1, !dbg !697

bb3:                                              ; preds = %bb1
  %11 = tail call i32 (i32, ...)* @syscall(i32 95, i32 %fd, i32 %owner, i32 %group) nounwind, !dbg !705
  tail call void @llvm.dbg.value(metadata !{i32 %11}, i64 0, metadata !295), !dbg !705
  %12 = icmp eq i32 %11, -1, !dbg !706
  br i1 %12, label %bb4, label %bb6, !dbg !706

bb4:                                              ; preds = %bb3
  %13 = tail call i32* @__errno_location() nounwind readnone, !dbg !707
  %14 = tail call i32 @klee_get_errno() nounwind, !dbg !707
  store i32 %14, i32* %13, align 4, !dbg !707
  ret i32 %11, !dbg !697

bb6:                                              ; preds = %bb3
  ret i32 %11, !dbg !697
}

define i32 @fchdir(i32 %fd) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !297), !dbg !708
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !709
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !711

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !712
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !713
  %3 = and i32 %2, 1, !dbg !713
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !713

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !298), !dbg !710
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !714
  br i1 %5, label %bb, label %bb1, !dbg !714

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !715
  store i32 9, i32* %6, align 4, !dbg !715
  ret i32 -1, !dbg !716

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !717
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !717
  br i1 %9, label %bb3, label %bb2, !dbg !717

bb2:                                              ; preds = %bb1
  tail call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str5, i32 0, i32 0)) nounwind, !dbg !718
  %10 = tail call i32* @__errno_location() nounwind readnone, !dbg !719
  store i32 2, i32* %10, align 4, !dbg !719
  ret i32 -1, !dbg !716

bb3:                                              ; preds = %bb1
  %11 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %12 = load i32* %11, align 4, !dbg !720
  %13 = tail call i32 (i32, ...)* @syscall(i32 133, i32 %12) nounwind, !dbg !720
  tail call void @llvm.dbg.value(metadata !{i32 %13}, i64 0, metadata !300), !dbg !720
  %14 = icmp eq i32 %13, -1, !dbg !721
  br i1 %14, label %bb4, label %bb6, !dbg !721

bb4:                                              ; preds = %bb3
  %15 = tail call i32* @__errno_location() nounwind readnone, !dbg !722
  %16 = tail call i32 @klee_get_errno() nounwind, !dbg !722
  store i32 %16, i32* %15, align 4, !dbg !722
  ret i32 %13, !dbg !716

bb6:                                              ; preds = %bb3
  ret i32 %13, !dbg !716
}

declare i32 @klee_get_valuel(i32)

declare void @klee_assume(i32)

define i8* @getcwd(i8* %buf, i32 %size) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !308), !dbg !723
  tail call void @llvm.dbg.value(metadata !{i32 %size}, i64 0, metadata !309), !dbg !723
  %0 = load i32* @n_calls.5273, align 4, !dbg !724
  %1 = add nsw i32 %0, 1, !dbg !724
  store i32 %1, i32* @n_calls.5273, align 4, !dbg !724
  %2 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !725
  %3 = icmp eq i32 %2, 0, !dbg !725
  br i1 %3, label %bb2, label %bb, !dbg !725

bb:                                               ; preds = %entry
  %4 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 10), align 4, !dbg !725
  %5 = load i32* %4, align 4, !dbg !725
  %6 = icmp eq i32 %5, %1, !dbg !725
  br i1 %6, label %bb1, label %bb2, !dbg !725

bb1:                                              ; preds = %bb
  %7 = add i32 %2, -1
  store i32 %7, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !726
  %8 = tail call i32* @__errno_location() nounwind readnone, !dbg !727
  store i32 34, i32* %8, align 4, !dbg !727
  ret i8* null, !dbg !728

bb2:                                              ; preds = %entry, %bb
  %9 = icmp eq i8* %buf, null, !dbg !729
  br i1 %9, label %bb3, label %bb6, !dbg !729

bb3:                                              ; preds = %bb2
  %10 = icmp eq i32 %size, 0, !dbg !730
  tail call void @llvm.dbg.value(metadata !731, i64 0, metadata !309), !dbg !732
  %size_addr.0 = select i1 %10, i32 1024, i32 %size
  %11 = tail call noalias i8* @malloc(i32 %size_addr.0) nounwind, !dbg !733
  tail call void @llvm.dbg.value(metadata !{i8* %11}, i64 0, metadata !308), !dbg !733
  br label %bb6, !dbg !733

bb6:                                              ; preds = %bb3, %bb2
  %size_addr.1 = phi i32 [ %size_addr.0, %bb3 ], [ %size, %bb2 ]
  %buf_addr.0 = phi i8* [ %11, %bb3 ], [ %buf, %bb2 ]
  tail call void @llvm.dbg.value(metadata !{i8* %buf_addr.0}, i64 0, metadata !302) nounwind, !dbg !734
  %12 = ptrtoint i8* %buf_addr.0 to i32, !dbg !736
  %13 = tail call i32 @klee_get_valuel(i32 %12) nounwind, !dbg !736
  %14 = inttoptr i32 %13 to i8*, !dbg !736
  tail call void @llvm.dbg.value(metadata !{i8* %14}, i64 0, metadata !303) nounwind, !dbg !736
  %15 = icmp eq i8* %14, %buf_addr.0, !dbg !737
  %16 = zext i1 %15 to i32, !dbg !737
  tail call void @klee_assume(i32 %16) nounwind, !dbg !737
  tail call void @llvm.dbg.value(metadata !{i8* %14}, i64 0, metadata !308), !dbg !735
  tail call void @llvm.dbg.value(metadata !{i32 %size_addr.1}, i64 0, metadata !305) nounwind, !dbg !738
  %17 = tail call i32 @klee_get_valuel(i32 %size_addr.1) nounwind, !dbg !740
  tail call void @llvm.dbg.value(metadata !{i32 %17}, i64 0, metadata !306) nounwind, !dbg !740
  %18 = icmp eq i32 %17, %size_addr.1, !dbg !741
  %19 = zext i1 %18 to i32, !dbg !741
  tail call void @klee_assume(i32 %19) nounwind, !dbg !741
  tail call void @llvm.dbg.value(metadata !{i32 %17}, i64 0, metadata !309), !dbg !739
  tail call void @klee_check_memory_access(i8* %14, i32 %17) nounwind, !dbg !742
  %20 = tail call i32 (i32, ...)* @syscall(i32 183, i8* %14, i32 %17) nounwind, !dbg !743
  tail call void @llvm.dbg.value(metadata !{i32 %20}, i64 0, metadata !310), !dbg !743
  %21 = icmp eq i32 %20, -1, !dbg !744
  br i1 %21, label %bb7, label %bb9, !dbg !744

bb7:                                              ; preds = %bb6
  %22 = tail call i32* @__errno_location() nounwind readnone, !dbg !745
  %23 = tail call i32 @klee_get_errno() nounwind, !dbg !745
  store i32 %23, i32* %22, align 4, !dbg !745
  ret i8* null, !dbg !728

bb9:                                              ; preds = %bb6
  ret i8* %14, !dbg !728
}

declare noalias i8* @malloc(i32) nounwind

declare void @klee_check_memory_access(i8*, i32)

define i32 @__fd_statfs(i8* %path, %struct.statfs* %buf) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !320), !dbg !746
  tail call void @llvm.dbg.value(metadata !{%struct.statfs* %buf}, i64 0, metadata !321), !dbg !746
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !747
  %0 = load i8* %path, align 1, !dbg !749
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !749
  %1 = icmp eq i8 %0, 0, !dbg !750
  br i1 %1, label %bb1, label %bb.i, !dbg !750

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !750
  %3 = load i8* %2, align 1, !dbg !750
  %4 = icmp eq i8 %3, 0, !dbg !750
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !750

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !751
  %6 = sext i8 %0 to i32, !dbg !752
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !752
  %9 = icmp eq i32 %6, %8, !dbg !752
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !752

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !753
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !753
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !754
  %12 = load %struct.stat64** %11, align 4, !dbg !754
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !754
  %14 = load i64* %13, align 4, !dbg !754
  %15 = icmp eq i64 %14, 0, !dbg !754
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !754

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !751
  br label %bb8.i, !dbg !751

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !751
  br i1 %18, label %bb3.i, label %bb1, !dbg !751

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !753
  %phitmp = icmp eq %struct.exe_disk_file_t* %19, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !322), !dbg !748
  br i1 %phitmp, label %bb1, label %bb, !dbg !755

bb:                                               ; preds = %__get_sym_file.exit
  tail call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str5, i32 0, i32 0)) nounwind, !dbg !756
  %20 = tail call i32* @__errno_location() nounwind readnone, !dbg !757
  store i32 2, i32* %20, align 4, !dbg !757
  ret i32 -1, !dbg !758

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !759
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !761
  %21 = ptrtoint i8* %path to i32, !dbg !763
  %22 = tail call i32 @klee_get_valuel(i32 %21) nounwind, !dbg !763
  %23 = inttoptr i32 %22 to i8*, !dbg !763
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !303) nounwind, !dbg !763
  %24 = icmp eq i8* %23, %path, !dbg !764
  %25 = zext i1 %24 to i32, !dbg !764
  tail call void @klee_assume(i32 %25) nounwind, !dbg !764
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !313) nounwind, !dbg !762
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !765
  br label %bb.i6, !dbg !765

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %23, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %26 = phi i32 [ 0, %bb1 ], [ %38, %bb6.i8 ]
  %tmp.i = add i32 %26, -1
  %27 = load i8* %sc.1.i, align 1, !dbg !766
  %28 = and i32 %tmp.i, %26, !dbg !767
  %29 = icmp eq i32 %28, 0, !dbg !767
  br i1 %29, label %bb1.i, label %bb5.i, !dbg !767

bb1.i:                                            ; preds = %bb.i6
  switch i8 %27, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %27}, i64 0, metadata !316) nounwind, !dbg !766
  store i8 0, i8* %sc.1.i, align 1, !dbg !768
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !768
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !769
  %30 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !769
  br label %bb6.i8, !dbg !769

bb5.i:                                            ; preds = %bb.i6
  %31 = sext i8 %27 to i32, !dbg !770
  %32 = tail call i32 @klee_get_valuel(i32 %31) nounwind, !dbg !770
  %33 = trunc i32 %32 to i8, !dbg !770
  %34 = icmp eq i8 %33, %27, !dbg !771
  %35 = zext i1 %34 to i32, !dbg !771
  tail call void @klee_assume(i32 %35) nounwind, !dbg !771
  store i8 %33, i8* %sc.1.i, align 1, !dbg !772
  %36 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !772
  %37 = icmp eq i8 %33, 0, !dbg !773
  br i1 %37, label %__concretize_string.exit, label %bb6.i8, !dbg !773

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %30, %bb4.i7 ], [ %36, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %38 = add i32 %26, 1, !dbg !765
  br label %bb.i6, !dbg !765

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %39 = tail call i32 (i32, ...)* @syscall(i32 99, i8* %path, %struct.statfs* %buf) nounwind, !dbg !760
  tail call void @llvm.dbg.value(metadata !{i32 %39}, i64 0, metadata !324), !dbg !760
  %40 = icmp eq i32 %39, -1, !dbg !774
  br i1 %40, label %bb2, label %bb4, !dbg !774

bb2:                                              ; preds = %__concretize_string.exit
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !775
  %42 = tail call i32 @klee_get_errno() nounwind, !dbg !775
  store i32 %42, i32* %41, align 4, !dbg !775
  ret i32 %39, !dbg !758

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %39, !dbg !758
}

define i32 @lchown(i8* %path, i32 %owner, i32 %group) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !326), !dbg !776
  tail call void @llvm.dbg.value(metadata !{i32 %owner}, i64 0, metadata !327), !dbg !776
  tail call void @llvm.dbg.value(metadata !{i32 %group}, i64 0, metadata !328), !dbg !776
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !777
  %0 = load i8* %path, align 1, !dbg !779
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !779
  %1 = icmp eq i8 %0, 0, !dbg !780
  br i1 %1, label %bb1, label %bb.i, !dbg !780

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !780
  %3 = load i8* %2, align 1, !dbg !780
  %4 = icmp eq i8 %3, 0, !dbg !780
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !780

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !781
  %6 = sext i8 %0 to i32, !dbg !782
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !782
  %9 = icmp eq i32 %6, %8, !dbg !782
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !782

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !783
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !783
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !784
  %12 = load %struct.stat64** %11, align 4, !dbg !784
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !784
  %14 = load i64* %13, align 4, !dbg !784
  %15 = icmp eq i64 %14, 0, !dbg !784
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !784

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !781
  br label %bb8.i, !dbg !781

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !781
  br i1 %18, label %bb3.i, label %bb1, !dbg !781

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !783
  %phitmp = icmp eq %struct.exe_disk_file_t* %19, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !329), !dbg !778
  br i1 %phitmp, label %bb1, label %bb, !dbg !785

bb:                                               ; preds = %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !699, i64 0, metadata !256) nounwind, !dbg !786
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !257) nounwind, !dbg !786
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !258) nounwind, !dbg !786
  tail call void @klee_warning(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0)) nounwind, !dbg !788
  %20 = tail call i32* @__errno_location() nounwind readnone, !dbg !789
  store i32 1, i32* %20, align 4, !dbg !789
  ret i32 -1, !dbg !787

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !790
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !792
  %21 = ptrtoint i8* %path to i32, !dbg !794
  %22 = tail call i32 @klee_get_valuel(i32 %21) nounwind, !dbg !794
  %23 = inttoptr i32 %22 to i8*, !dbg !794
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !303) nounwind, !dbg !794
  %24 = icmp eq i8* %23, %path, !dbg !795
  %25 = zext i1 %24 to i32, !dbg !795
  tail call void @klee_assume(i32 %25) nounwind, !dbg !795
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !313) nounwind, !dbg !793
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !796
  br label %bb.i6, !dbg !796

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %23, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %26 = phi i32 [ 0, %bb1 ], [ %38, %bb6.i8 ]
  %tmp.i = add i32 %26, -1
  %27 = load i8* %sc.1.i, align 1, !dbg !797
  %28 = and i32 %tmp.i, %26, !dbg !798
  %29 = icmp eq i32 %28, 0, !dbg !798
  br i1 %29, label %bb1.i, label %bb5.i, !dbg !798

bb1.i:                                            ; preds = %bb.i6
  switch i8 %27, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %27}, i64 0, metadata !316) nounwind, !dbg !797
  store i8 0, i8* %sc.1.i, align 1, !dbg !799
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !799
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !800
  %30 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !800
  br label %bb6.i8, !dbg !800

bb5.i:                                            ; preds = %bb.i6
  %31 = sext i8 %27 to i32, !dbg !801
  %32 = tail call i32 @klee_get_valuel(i32 %31) nounwind, !dbg !801
  %33 = trunc i32 %32 to i8, !dbg !801
  %34 = icmp eq i8 %33, %27, !dbg !802
  %35 = zext i1 %34 to i32, !dbg !802
  tail call void @klee_assume(i32 %35) nounwind, !dbg !802
  store i8 %33, i8* %sc.1.i, align 1, !dbg !803
  %36 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !803
  %37 = icmp eq i8 %33, 0, !dbg !804
  br i1 %37, label %__concretize_string.exit, label %bb6.i8, !dbg !804

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %30, %bb4.i7 ], [ %36, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %38 = add i32 %26, 1, !dbg !796
  br label %bb.i6, !dbg !796

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %39 = tail call i32 (i32, ...)* @syscall(i32 182, i8* %path, i32 %owner, i32 %group) nounwind, !dbg !791
  tail call void @llvm.dbg.value(metadata !{i32 %39}, i64 0, metadata !331), !dbg !791
  %40 = icmp eq i32 %39, -1, !dbg !805
  br i1 %40, label %bb2, label %bb4, !dbg !805

bb2:                                              ; preds = %__concretize_string.exit
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !806
  %42 = tail call i32 @klee_get_errno() nounwind, !dbg !806
  store i32 %42, i32* %41, align 4, !dbg !806
  ret i32 %39, !dbg !787

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %39, !dbg !787
}

define i32 @chown(i8* %path, i32 %owner, i32 %group) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !333), !dbg !807
  tail call void @llvm.dbg.value(metadata !{i32 %owner}, i64 0, metadata !334), !dbg !807
  tail call void @llvm.dbg.value(metadata !{i32 %group}, i64 0, metadata !335), !dbg !807
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !808
  %0 = load i8* %path, align 1, !dbg !810
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !810
  %1 = icmp eq i8 %0, 0, !dbg !811
  br i1 %1, label %bb1, label %bb.i, !dbg !811

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !811
  %3 = load i8* %2, align 1, !dbg !811
  %4 = icmp eq i8 %3, 0, !dbg !811
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !811

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !812
  %6 = sext i8 %0 to i32, !dbg !813
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !813
  %9 = icmp eq i32 %6, %8, !dbg !813
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !813

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !814
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !814
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !815
  %12 = load %struct.stat64** %11, align 4, !dbg !815
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !815
  %14 = load i64* %13, align 4, !dbg !815
  %15 = icmp eq i64 %14, 0, !dbg !815
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !815

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !812
  br label %bb8.i, !dbg !812

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !812
  br i1 %18, label %bb3.i, label %bb1, !dbg !812

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !814
  %phitmp = icmp eq %struct.exe_disk_file_t* %19, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !336), !dbg !809
  br i1 %phitmp, label %bb1, label %bb, !dbg !816

bb:                                               ; preds = %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !699, i64 0, metadata !256) nounwind, !dbg !817
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !257) nounwind, !dbg !817
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !258) nounwind, !dbg !817
  tail call void @klee_warning(i8* getelementptr inbounds ([32 x i8]* @.str2, i32 0, i32 0)) nounwind, !dbg !819
  %20 = tail call i32* @__errno_location() nounwind readnone, !dbg !820
  store i32 1, i32* %20, align 4, !dbg !820
  ret i32 -1, !dbg !818

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !821
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !823
  %21 = ptrtoint i8* %path to i32, !dbg !825
  %22 = tail call i32 @klee_get_valuel(i32 %21) nounwind, !dbg !825
  %23 = inttoptr i32 %22 to i8*, !dbg !825
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !303) nounwind, !dbg !825
  %24 = icmp eq i8* %23, %path, !dbg !826
  %25 = zext i1 %24 to i32, !dbg !826
  tail call void @klee_assume(i32 %25) nounwind, !dbg !826
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !313) nounwind, !dbg !824
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !827
  br label %bb.i6, !dbg !827

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %23, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %26 = phi i32 [ 0, %bb1 ], [ %38, %bb6.i8 ]
  %tmp.i = add i32 %26, -1
  %27 = load i8* %sc.1.i, align 1, !dbg !828
  %28 = and i32 %tmp.i, %26, !dbg !829
  %29 = icmp eq i32 %28, 0, !dbg !829
  br i1 %29, label %bb1.i, label %bb5.i, !dbg !829

bb1.i:                                            ; preds = %bb.i6
  switch i8 %27, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %27}, i64 0, metadata !316) nounwind, !dbg !828
  store i8 0, i8* %sc.1.i, align 1, !dbg !830
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !830
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !831
  %30 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !831
  br label %bb6.i8, !dbg !831

bb5.i:                                            ; preds = %bb.i6
  %31 = sext i8 %27 to i32, !dbg !832
  %32 = tail call i32 @klee_get_valuel(i32 %31) nounwind, !dbg !832
  %33 = trunc i32 %32 to i8, !dbg !832
  %34 = icmp eq i8 %33, %27, !dbg !833
  %35 = zext i1 %34 to i32, !dbg !833
  tail call void @klee_assume(i32 %35) nounwind, !dbg !833
  store i8 %33, i8* %sc.1.i, align 1, !dbg !834
  %36 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !834
  %37 = icmp eq i8 %33, 0, !dbg !835
  br i1 %37, label %__concretize_string.exit, label %bb6.i8, !dbg !835

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %30, %bb4.i7 ], [ %36, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %38 = add i32 %26, 1, !dbg !827
  br label %bb.i6, !dbg !827

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %39 = tail call i32 (i32, ...)* @syscall(i32 182, i8* %path, i32 %owner, i32 %group) nounwind, !dbg !822
  tail call void @llvm.dbg.value(metadata !{i32 %39}, i64 0, metadata !338), !dbg !822
  %40 = icmp eq i32 %39, -1, !dbg !836
  br i1 %40, label %bb2, label %bb4, !dbg !836

bb2:                                              ; preds = %__concretize_string.exit
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !837
  %42 = tail call i32 @klee_get_errno() nounwind, !dbg !837
  store i32 %42, i32* %41, align 4, !dbg !837
  ret i32 %39, !dbg !818

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %39, !dbg !818
}

define i32 @chdir(i8* %path) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !340), !dbg !838
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !839
  %0 = load i8* %path, align 1, !dbg !841
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !841
  %1 = icmp eq i8 %0, 0, !dbg !842
  br i1 %1, label %bb1, label %bb.i, !dbg !842

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !842
  %3 = load i8* %2, align 1, !dbg !842
  %4 = icmp eq i8 %3, 0, !dbg !842
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !842

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !843
  %6 = sext i8 %0 to i32, !dbg !844
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !844
  %9 = icmp eq i32 %6, %8, !dbg !844
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !844

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !845
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !845
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !846
  %12 = load %struct.stat64** %11, align 4, !dbg !846
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !846
  %14 = load i64* %13, align 4, !dbg !846
  %15 = icmp eq i64 %14, 0, !dbg !846
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !846

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !843
  br label %bb8.i, !dbg !843

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !843
  br i1 %18, label %bb3.i, label %bb1, !dbg !843

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !845
  %phitmp = icmp eq %struct.exe_disk_file_t* %19, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !341), !dbg !840
  br i1 %phitmp, label %bb1, label %bb, !dbg !847

bb:                                               ; preds = %__get_sym_file.exit
  tail call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str5, i32 0, i32 0)) nounwind, !dbg !848
  %20 = tail call i32* @__errno_location() nounwind readnone, !dbg !849
  store i32 2, i32* %20, align 4, !dbg !849
  ret i32 -1, !dbg !850

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !851
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !853
  %21 = ptrtoint i8* %path to i32, !dbg !855
  %22 = tail call i32 @klee_get_valuel(i32 %21) nounwind, !dbg !855
  %23 = inttoptr i32 %22 to i8*, !dbg !855
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !303) nounwind, !dbg !855
  %24 = icmp eq i8* %23, %path, !dbg !856
  %25 = zext i1 %24 to i32, !dbg !856
  tail call void @klee_assume(i32 %25) nounwind, !dbg !856
  tail call void @llvm.dbg.value(metadata !{i8* %23}, i64 0, metadata !313) nounwind, !dbg !854
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !857
  br label %bb.i6, !dbg !857

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %23, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %26 = phi i32 [ 0, %bb1 ], [ %38, %bb6.i8 ]
  %tmp.i = add i32 %26, -1
  %27 = load i8* %sc.1.i, align 1, !dbg !858
  %28 = and i32 %tmp.i, %26, !dbg !859
  %29 = icmp eq i32 %28, 0, !dbg !859
  br i1 %29, label %bb1.i, label %bb5.i, !dbg !859

bb1.i:                                            ; preds = %bb.i6
  switch i8 %27, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %27}, i64 0, metadata !316) nounwind, !dbg !858
  store i8 0, i8* %sc.1.i, align 1, !dbg !860
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !860
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !861
  %30 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !861
  br label %bb6.i8, !dbg !861

bb5.i:                                            ; preds = %bb.i6
  %31 = sext i8 %27 to i32, !dbg !862
  %32 = tail call i32 @klee_get_valuel(i32 %31) nounwind, !dbg !862
  %33 = trunc i32 %32 to i8, !dbg !862
  %34 = icmp eq i8 %33, %27, !dbg !863
  %35 = zext i1 %34 to i32, !dbg !863
  tail call void @klee_assume(i32 %35) nounwind, !dbg !863
  store i8 %33, i8* %sc.1.i, align 1, !dbg !864
  %36 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !864
  %37 = icmp eq i8 %33, 0, !dbg !865
  br i1 %37, label %__concretize_string.exit, label %bb6.i8, !dbg !865

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %30, %bb4.i7 ], [ %36, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %38 = add i32 %26, 1, !dbg !857
  br label %bb.i6, !dbg !857

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %39 = tail call i32 (i32, ...)* @syscall(i32 12, i8* %path) nounwind, !dbg !852
  tail call void @llvm.dbg.value(metadata !{i32 %39}, i64 0, metadata !343), !dbg !852
  %40 = icmp eq i32 %39, -1, !dbg !866
  br i1 %40, label %bb2, label %bb4, !dbg !866

bb2:                                              ; preds = %__concretize_string.exit
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !867
  %42 = tail call i32 @klee_get_errno() nounwind, !dbg !867
  store i32 %42, i32* %41, align 4, !dbg !867
  ret i32 %39, !dbg !850

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %39, !dbg !850
}

define i32 @select(i32 %nfds, %struct.fd_set* %read, %struct.fd_set* %write, %struct.fd_set* %except, %struct.timespec* nocapture %timeout) nounwind {
entry:
  %in_read = alloca %struct.fd_set, align 8
  %in_write = alloca %struct.fd_set, align 8
  %in_except = alloca %struct.fd_set, align 8
  %os_read = alloca %struct.fd_set, align 8
  %os_write = alloca %struct.fd_set, align 8
  %os_except = alloca %struct.fd_set, align 8
  %tv = alloca %struct.timespec, align 8
  call void @llvm.dbg.value(metadata !{i32 %nfds}, i64 0, metadata !351), !dbg !868
  call void @llvm.dbg.value(metadata !{%struct.fd_set* %read}, i64 0, metadata !352), !dbg !868
  call void @llvm.dbg.value(metadata !{%struct.fd_set* %write}, i64 0, metadata !353), !dbg !868
  call void @llvm.dbg.value(metadata !{%struct.fd_set* %except}, i64 0, metadata !354), !dbg !869
  call void @llvm.dbg.value(metadata !{%struct.timespec* %timeout}, i64 0, metadata !355), !dbg !869
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %in_read}, metadata !356), !dbg !870
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %in_write}, metadata !358), !dbg !870
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %in_except}, metadata !359), !dbg !870
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %os_read}, metadata !360), !dbg !870
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %os_write}, metadata !361), !dbg !870
  call void @llvm.dbg.declare(metadata !{%struct.fd_set* %os_except}, metadata !362), !dbg !870
  call void @llvm.dbg.value(metadata !562, i64 0, metadata !364), !dbg !871
  call void @llvm.dbg.value(metadata !562, i64 0, metadata !365), !dbg !871
  %0 = icmp eq %struct.fd_set* %read, null, !dbg !872
  %in_read3 = bitcast %struct.fd_set* %in_read to i8*, !dbg !873
  br i1 %0, label %bb2, label %bb, !dbg !872

bb:                                               ; preds = %entry
  %1 = bitcast %struct.fd_set* %read to i8*, !dbg !874
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* %in_read3, i8* %1, i32 128, i32 4, i1 false), !dbg !874
  call void @llvm.memset.p0i8.i32(i8* %1, i8 0, i32 128, i32 4, i1 false), !dbg !875
  br label %bb4, !dbg !875

bb2:                                              ; preds = %entry
  call void @llvm.memset.p0i8.i32(i8* %in_read3, i8 0, i32 128, i32 8, i1 false), !dbg !873
  br label %bb4, !dbg !873

bb4:                                              ; preds = %bb2, %bb
  %2 = icmp eq %struct.fd_set* %write, null, !dbg !876
  %in_write8 = bitcast %struct.fd_set* %in_write to i8*, !dbg !877
  br i1 %2, label %bb7, label %bb5, !dbg !876

bb5:                                              ; preds = %bb4
  %3 = bitcast %struct.fd_set* %write to i8*, !dbg !878
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* %in_write8, i8* %3, i32 128, i32 4, i1 false), !dbg !878
  call void @llvm.memset.p0i8.i32(i8* %3, i8 0, i32 128, i32 4, i1 false), !dbg !879
  br label %bb9, !dbg !879

bb7:                                              ; preds = %bb4
  call void @llvm.memset.p0i8.i32(i8* %in_write8, i8 0, i32 128, i32 8, i1 false), !dbg !877
  br label %bb9, !dbg !877

bb9:                                              ; preds = %bb7, %bb5
  %4 = icmp eq %struct.fd_set* %except, null, !dbg !880
  %in_except13 = bitcast %struct.fd_set* %in_except to i8*, !dbg !881
  br i1 %4, label %bb12, label %bb10, !dbg !880

bb10:                                             ; preds = %bb9
  %5 = bitcast %struct.fd_set* %except to i8*, !dbg !882
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* %in_except13, i8* %5, i32 128, i32 4, i1 false), !dbg !882
  call void @llvm.memset.p0i8.i32(i8* %5, i8 0, i32 128, i32 4, i1 false), !dbg !883
  br label %bb14, !dbg !883

bb12:                                             ; preds = %bb9
  call void @llvm.memset.p0i8.i32(i8* %in_except13, i8 0, i32 128, i32 8, i1 false), !dbg !881
  br label %bb14, !dbg !881

bb14:                                             ; preds = %bb12, %bb10
  %os_read15 = bitcast %struct.fd_set* %os_read to i8*, !dbg !884
  call void @llvm.memset.p0i8.i32(i8* %os_read15, i8 0, i32 128, i32 8, i1 false), !dbg !884
  %os_write16 = bitcast %struct.fd_set* %os_write to i8*, !dbg !885
  call void @llvm.memset.p0i8.i32(i8* %os_write16, i8 0, i32 128, i32 8, i1 false), !dbg !885
  %os_except17 = bitcast %struct.fd_set* %os_except to i8*, !dbg !886
  call void @llvm.memset.p0i8.i32(i8* %os_except17, i8 0, i32 128, i32 8, i1 false), !dbg !886
  call void @llvm.dbg.value(metadata !562, i64 0, metadata !363), !dbg !887
  br label %bb40, !dbg !887

bb18:                                             ; preds = %bb40
  %6 = sdiv i32 %78, 32, !dbg !888
  %7 = getelementptr inbounds %struct.fd_set* %in_read, i32 0, i32 0, i32 %6
  %8 = load i32* %7, align 4, !dbg !888
  %9 = srem i32 %78, 32, !dbg !888
  %10 = shl i32 1, %9, !dbg !888
  %11 = and i32 %10, %8, !dbg !888
  %12 = icmp eq i32 %11, 0, !dbg !888
  br i1 %12, label %bb19, label %bb21, !dbg !888

bb19:                                             ; preds = %bb18
  %13 = getelementptr inbounds %struct.fd_set* %in_write, i32 0, i32 0, i32 %6
  %14 = load i32* %13, align 4, !dbg !888
  %15 = and i32 %10, %14, !dbg !888
  %16 = icmp eq i32 %15, 0, !dbg !888
  br i1 %16, label %bb20, label %bb21, !dbg !888

bb20:                                             ; preds = %bb19
  %17 = getelementptr inbounds %struct.fd_set* %in_except, i32 0, i32 0, i32 %6
  %18 = load i32* %17, align 4, !dbg !888
  %19 = and i32 %10, %18, !dbg !888
  %20 = icmp eq i32 %19, 0, !dbg !888
  br i1 %20, label %bb39, label %bb21, !dbg !888

bb21:                                             ; preds = %bb20, %bb19, %bb18
  %21 = icmp ult i32 %78, 32
  br i1 %21, label %bb.i, label %bb22, !dbg !889

bb.i:                                             ; preds = %bb21
  %22 = load i32* %scevgep79, align 4, !dbg !891
  %23 = and i32 %22, 1, !dbg !891
  %toBool.i = icmp eq i32 %23, 0
  br i1 %toBool.i, label %bb22, label %bb23, !dbg !891

bb22:                                             ; preds = %bb21, %bb.i
  tail call void @llvm.dbg.value(metadata !{i32 %78}, i64 0, metadata !236), !dbg !892
  %24 = call i32* @__errno_location() nounwind readnone, !dbg !893
  store i32 9, i32* %24, align 4, !dbg !893
  ret i32 -1, !dbg !894

bb23:                                             ; preds = %bb.i
  %25 = load %struct.exe_disk_file_t** %scevgep78, align 4, !dbg !895
  %26 = icmp eq %struct.exe_disk_file_t* %25, null, !dbg !895
  %27 = icmp ne i32 %11, 0, !dbg !896
  br i1 %26, label %bb31, label %bb24, !dbg !895

bb24:                                             ; preds = %bb23
  br i1 %27, label %bb25, label %bb26, !dbg !896

bb25:                                             ; preds = %bb24
  %28 = getelementptr inbounds %struct.fd_set* %read, i32 0, i32 0, i32 %6
  %29 = load i32* %28, align 4, !dbg !896
  %30 = or i32 %10, %29, !dbg !896
  store i32 %30, i32* %28, align 4, !dbg !896
  br label %bb26, !dbg !896

bb26:                                             ; preds = %bb24, %bb25
  %31 = getelementptr inbounds %struct.fd_set* %in_write, i32 0, i32 0, i32 %6
  %32 = load i32* %31, align 4, !dbg !897
  %33 = and i32 %10, %32, !dbg !897
  %34 = icmp eq i32 %33, 0, !dbg !897
  br i1 %34, label %bb28, label %bb27, !dbg !897

bb27:                                             ; preds = %bb26
  %35 = getelementptr inbounds %struct.fd_set* %write, i32 0, i32 0, i32 %6
  %36 = load i32* %35, align 4, !dbg !897
  %37 = or i32 %10, %36, !dbg !897
  store i32 %37, i32* %35, align 4, !dbg !897
  br label %bb28, !dbg !897

bb28:                                             ; preds = %bb26, %bb27
  %38 = getelementptr inbounds %struct.fd_set* %in_except, i32 0, i32 0, i32 %6
  %39 = load i32* %38, align 4, !dbg !898
  %40 = and i32 %10, %39, !dbg !898
  %41 = icmp eq i32 %40, 0, !dbg !898
  br i1 %41, label %bb30, label %bb29, !dbg !898

bb29:                                             ; preds = %bb28
  %42 = getelementptr inbounds %struct.fd_set* %except, i32 0, i32 0, i32 %6
  %43 = load i32* %42, align 4, !dbg !898
  %44 = or i32 %10, %43, !dbg !898
  store i32 %44, i32* %42, align 4, !dbg !898
  br label %bb30, !dbg !898

bb30:                                             ; preds = %bb28, %bb29
  %45 = add nsw i32 %count.1, 1, !dbg !899
  br label %bb39, !dbg !899

bb31:                                             ; preds = %bb23
  br i1 %27, label %bb32, label %bb33, !dbg !900

bb32:                                             ; preds = %bb31
  %46 = load i32* %scevgep7677, align 4, !dbg !900
  %47 = sdiv i32 %46, 32, !dbg !900
  %48 = getelementptr inbounds %struct.fd_set* %os_read, i32 0, i32 0, i32 %47
  %49 = load i32* %48, align 4, !dbg !900
  %50 = srem i32 %46, 32, !dbg !900
  %51 = shl i32 1, %50, !dbg !900
  %52 = or i32 %51, %49, !dbg !900
  store i32 %52, i32* %48, align 4, !dbg !900
  br label %bb33, !dbg !900

bb33:                                             ; preds = %bb31, %bb32
  %53 = getelementptr inbounds %struct.fd_set* %in_write, i32 0, i32 0, i32 %6
  %54 = load i32* %53, align 4, !dbg !901
  %55 = and i32 %10, %54, !dbg !901
  %56 = icmp eq i32 %55, 0, !dbg !901
  br i1 %56, label %bb35, label %bb34, !dbg !901

bb34:                                             ; preds = %bb33
  %57 = load i32* %scevgep7677, align 4, !dbg !901
  %58 = sdiv i32 %57, 32, !dbg !901
  %59 = getelementptr inbounds %struct.fd_set* %os_write, i32 0, i32 0, i32 %58
  %60 = load i32* %59, align 4, !dbg !901
  %61 = srem i32 %57, 32, !dbg !901
  %62 = shl i32 1, %61, !dbg !901
  %63 = or i32 %62, %60, !dbg !901
  store i32 %63, i32* %59, align 4, !dbg !901
  br label %bb35, !dbg !901

bb35:                                             ; preds = %bb33, %bb34
  %64 = getelementptr inbounds %struct.fd_set* %in_except, i32 0, i32 0, i32 %6
  %65 = load i32* %64, align 4, !dbg !902
  %66 = and i32 %10, %65, !dbg !902
  %67 = icmp eq i32 %66, 0, !dbg !902
  %.pre = load i32* %scevgep7677, align 4
  br i1 %67, label %bb37, label %bb36, !dbg !902

bb36:                                             ; preds = %bb35
  %68 = sdiv i32 %.pre, 32, !dbg !902
  %69 = getelementptr inbounds %struct.fd_set* %os_except, i32 0, i32 0, i32 %68
  %70 = load i32* %69, align 4, !dbg !902
  %71 = srem i32 %.pre, 32, !dbg !902
  %72 = shl i32 1, %71, !dbg !902
  %73 = or i32 %72, %70, !dbg !902
  store i32 %73, i32* %69, align 4, !dbg !902
  br label %bb37, !dbg !902

bb37:                                             ; preds = %bb35, %bb36
  %74 = phi i32 [ %.pre, %bb36 ], [ %.pre, %bb35 ]
  %75 = add nsw i32 %.pre, 1, !dbg !903
  %76 = icmp slt i32 %.pre, %os_nfds.1, !dbg !903
  %os_nfds.1. = select i1 %76, i32 %os_nfds.1, i32 %75
  br label %bb39, !dbg !903

bb39:                                             ; preds = %bb37, %bb20, %bb30
  %count.0 = phi i32 [ %45, %bb30 ], [ %count.1, %bb20 ], [ %count.1, %bb37 ]
  %os_nfds.0 = phi i32 [ %os_nfds.1, %bb30 ], [ %os_nfds.1, %bb20 ], [ %os_nfds.1., %bb37 ]
  %77 = add nsw i32 %78, 1, !dbg !887
  br label %bb40, !dbg !887

bb40:                                             ; preds = %bb39, %bb14
  %78 = phi i32 [ 0, %bb14 ], [ %77, %bb39 ]
  %count.1 = phi i32 [ 0, %bb14 ], [ %count.0, %bb39 ]
  %os_nfds.1 = phi i32 [ 0, %bb14 ], [ %os_nfds.0, %bb39 ]
  %scevgep7677 = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %78, i32 0
  %scevgep78 = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %78, i32 3
  %scevgep79 = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %78, i32 1
  %79 = icmp slt i32 %78, %nfds, !dbg !887
  br i1 %79, label %bb18, label %bb41, !dbg !887

bb41:                                             ; preds = %bb40
  %80 = icmp sgt i32 %os_nfds.1, 0, !dbg !904
  br i1 %80, label %bb42, label %bb61, !dbg !904

bb42:                                             ; preds = %bb41
  call void @llvm.dbg.declare(metadata !{%struct.timespec* %tv}, metadata !368), !dbg !905
  %81 = getelementptr inbounds %struct.timespec* %tv, i32 0, i32 0, !dbg !905
  store i32 0, i32* %81, align 8, !dbg !905
  %82 = getelementptr inbounds %struct.timespec* %tv, i32 0, i32 1, !dbg !905
  store i32 0, i32* %82, align 4, !dbg !905
  %83 = call i32 (i32, ...)* @syscall(i32 82, i32 %os_nfds.1, %struct.fd_set* %os_read, %struct.fd_set* %os_write, %struct.fd_set* %os_except, %struct.timespec* %tv) nounwind, !dbg !906
  call void @llvm.dbg.value(metadata !{i32 %83}, i64 0, metadata !370), !dbg !906
  %84 = icmp eq i32 %83, -1, !dbg !907
  br i1 %84, label %bb43, label %bb45, !dbg !907

bb43:                                             ; preds = %bb42
  %85 = icmp eq i32 %count.1, 0, !dbg !908
  br i1 %85, label %bb44, label %bb61, !dbg !908

bb44:                                             ; preds = %bb43
  %86 = call i32* @__errno_location() nounwind readnone, !dbg !909
  %87 = call i32 @klee_get_errno() nounwind, !dbg !909
  store i32 %87, i32* %86, align 4, !dbg !909
  ret i32 -1, !dbg !894

bb45:                                             ; preds = %bb42
  %88 = add nsw i32 %83, %count.1, !dbg !910
  call void @llvm.dbg.value(metadata !{i32 %88}, i64 0, metadata !364), !dbg !910
  call void @llvm.dbg.value(metadata !562, i64 0, metadata !363), !dbg !911
  %89 = icmp sgt i32 %nfds, 0, !dbg !911
  br i1 %89, label %bb46, label %bb61, !dbg !911

bb46:                                             ; preds = %bb45, %bb58
  %90 = phi i32 [ %138, %bb58 ], [ 0, %bb45 ]
  %scevgep7173 = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %90, i32 0
  %scevgep72 = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %90, i32 3
  %91 = icmp ult i32 %90, 32
  br i1 %91, label %bb.i64, label %bb58, !dbg !912

bb.i64:                                           ; preds = %bb46
  %scevgep = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %90, i32 1
  %92 = load i32* %scevgep, align 4, !dbg !914
  %93 = and i32 %92, 1, !dbg !914
  %toBool.i63 = icmp eq i32 %93, 0
  br i1 %toBool.i63, label %bb58, label %bb48, !dbg !914

bb48:                                             ; preds = %bb.i64
  %94 = load %struct.exe_disk_file_t** %scevgep72, align 4, !dbg !915
  %95 = icmp eq %struct.exe_disk_file_t* %94, null, !dbg !915
  br i1 %95, label %bb49, label %bb58, !dbg !915

bb49:                                             ; preds = %bb48
  br i1 %0, label %bb52, label %bb50, !dbg !916

bb50:                                             ; preds = %bb49
  %96 = load i32* %scevgep7173, align 4, !dbg !916
  %97 = sdiv i32 %96, 32, !dbg !916
  %98 = getelementptr inbounds %struct.fd_set* %os_read, i32 0, i32 0, i32 %97
  %99 = load i32* %98, align 4, !dbg !916
  %100 = srem i32 %96, 32, !dbg !916
  %101 = shl i32 1, %100, !dbg !916
  %102 = and i32 %101, %99, !dbg !916
  %103 = icmp eq i32 %102, 0, !dbg !916
  br i1 %103, label %bb52, label %bb51, !dbg !916

bb51:                                             ; preds = %bb50
  %104 = sdiv i32 %90, 32, !dbg !916
  %105 = getelementptr inbounds %struct.fd_set* %read, i32 0, i32 0, i32 %104
  %106 = load i32* %105, align 4, !dbg !916
  %107 = srem i32 %90, 32, !dbg !916
  %108 = shl i32 1, %107, !dbg !916
  %109 = or i32 %108, %106, !dbg !916
  store i32 %109, i32* %105, align 4, !dbg !916
  br label %bb52, !dbg !916

bb52:                                             ; preds = %bb50, %bb49, %bb51
  br i1 %2, label %bb55, label %bb53, !dbg !917

bb53:                                             ; preds = %bb52
  %110 = load i32* %scevgep7173, align 4, !dbg !917
  %111 = sdiv i32 %110, 32, !dbg !917
  %112 = getelementptr inbounds %struct.fd_set* %os_write, i32 0, i32 0, i32 %111
  %113 = load i32* %112, align 4, !dbg !917
  %114 = srem i32 %110, 32, !dbg !917
  %115 = shl i32 1, %114, !dbg !917
  %116 = and i32 %115, %113, !dbg !917
  %117 = icmp eq i32 %116, 0, !dbg !917
  br i1 %117, label %bb55, label %bb54, !dbg !917

bb54:                                             ; preds = %bb53
  %118 = sdiv i32 %90, 32, !dbg !917
  %119 = getelementptr inbounds %struct.fd_set* %write, i32 0, i32 0, i32 %118
  %120 = load i32* %119, align 4, !dbg !917
  %121 = srem i32 %90, 32, !dbg !917
  %122 = shl i32 1, %121, !dbg !917
  %123 = or i32 %122, %120, !dbg !917
  store i32 %123, i32* %119, align 4, !dbg !917
  br label %bb55, !dbg !917

bb55:                                             ; preds = %bb53, %bb52, %bb54
  br i1 %4, label %bb58, label %bb56, !dbg !918

bb56:                                             ; preds = %bb55
  %124 = load i32* %scevgep7173, align 4, !dbg !918
  %125 = sdiv i32 %124, 32, !dbg !918
  %126 = getelementptr inbounds %struct.fd_set* %os_except, i32 0, i32 0, i32 %125
  %127 = load i32* %126, align 4, !dbg !918
  %128 = srem i32 %124, 32, !dbg !918
  %129 = shl i32 1, %128, !dbg !918
  %130 = and i32 %129, %127, !dbg !918
  %131 = icmp eq i32 %130, 0, !dbg !918
  br i1 %131, label %bb58, label %bb57, !dbg !918

bb57:                                             ; preds = %bb56
  %132 = sdiv i32 %90, 32, !dbg !918
  %133 = getelementptr inbounds %struct.fd_set* %except, i32 0, i32 0, i32 %132
  %134 = load i32* %133, align 4, !dbg !918
  %135 = srem i32 %90, 32, !dbg !918
  %136 = shl i32 1, %135, !dbg !918
  %137 = or i32 %136, %134, !dbg !918
  store i32 %137, i32* %133, align 4, !dbg !918
  br label %bb58, !dbg !918

bb58:                                             ; preds = %bb46, %bb.i64, %bb56, %bb55, %bb57, %bb48
  %138 = add nsw i32 %90, 1, !dbg !911
  %exitcond = icmp eq i32 %138, %nfds
  br i1 %exitcond, label %bb61, label %bb46, !dbg !911

bb61:                                             ; preds = %bb45, %bb58, %bb41, %bb43
  %.0 = phi i32 [ %count.1, %bb43 ], [ %count.1, %bb41 ], [ %88, %bb58 ], [ %88, %bb45 ]
  ret i32 %.0, !dbg !894
}

declare void @llvm.memcpy.p0i8.p0i8.i32(i8* nocapture, i8* nocapture, i32, i32, i1) nounwind

declare void @llvm.memset.p0i8.i32(i8* nocapture, i8, i32, i32, i1) nounwind

define i32 @close(i32 %fd) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !373), !dbg !919
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !376), !dbg !920
  %0 = load i32* @n_calls.4387, align 4, !dbg !921
  %1 = add nsw i32 %0, 1, !dbg !921
  store i32 %1, i32* @n_calls.4387, align 4, !dbg !921
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !922
  %2 = icmp ult i32 %fd, 32
  br i1 %2, label %bb.i, label %bb, !dbg !924

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !925
  %3 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %4 = load i32* %3, align 4, !dbg !926
  %5 = and i32 %4, 1, !dbg !926
  %toBool.i = icmp eq i32 %5, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !926

__get_file.exit:                                  ; preds = %bb.i
  %6 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %6}, i64 0, metadata !374), !dbg !923
  %7 = icmp eq %struct.exe_file_t* %6, null, !dbg !927
  br i1 %7, label %bb, label %bb1, !dbg !927

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %8 = tail call i32* @__errno_location() nounwind readnone, !dbg !928
  store i32 9, i32* %8, align 4, !dbg !928
  ret i32 -1, !dbg !929

bb1:                                              ; preds = %__get_file.exit
  %9 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !930
  %10 = icmp eq i32 %9, 0, !dbg !930
  br i1 %10, label %bb4, label %bb2, !dbg !930

bb2:                                              ; preds = %bb1
  %11 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 8), align 4, !dbg !930
  %12 = load i32* %11, align 4, !dbg !930
  %13 = icmp eq i32 %12, %1, !dbg !930
  br i1 %13, label %bb3, label %bb4, !dbg !930

bb3:                                              ; preds = %bb2
  %14 = add i32 %9, -1
  store i32 %14, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !931
  %15 = tail call i32* @__errno_location() nounwind readnone, !dbg !932
  store i32 5, i32* %15, align 4, !dbg !932
  ret i32 -1, !dbg !929

bb4:                                              ; preds = %bb1, %bb2
  %16 = bitcast %struct.exe_file_t* %6 to i8*, !dbg !933
  tail call void @llvm.memset.p0i8.i32(i8* %16, i8 0, i32 20, i32 4, i1 false), !dbg !933
  ret i32 0, !dbg !929
}

define i32 @dup2(i32 %oldfd, i32 %newfd) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %oldfd}, i64 0, metadata !377), !dbg !934
  tail call void @llvm.dbg.value(metadata !{i32 %newfd}, i64 0, metadata !378), !dbg !934
  tail call void @llvm.dbg.value(metadata !{i32 %oldfd}, i64 0, metadata !236), !dbg !935
  %0 = icmp ult i32 %oldfd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !937

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !938
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd, i32 1
  %2 = load i32* %1, align 4, !dbg !939
  %3 = and i32 %2, 1, !dbg !939
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !939

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !379), !dbg !936
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !940
  %6 = icmp ugt i32 %newfd, 31, !dbg !940
  %7 = or i1 %5, %6, !dbg !940
  br i1 %7, label %bb, label %bb3, !dbg !940

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %8 = tail call i32* @__errno_location() nounwind readnone, !dbg !941
  store i32 9, i32* %8, align 4, !dbg !941
  ret i32 -1, !dbg !942

bb3:                                              ; preds = %__get_file.exit
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !381), !dbg !943
  %9 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %newfd, i32 1
  %10 = load i32* %9, align 4, !dbg !944
  %11 = and i32 %10, 1, !dbg !944
  %toBool4 = icmp eq i32 %11, 0
  br i1 %toBool4, label %bb6, label %bb5, !dbg !944

bb5:                                              ; preds = %bb3
  tail call void @llvm.dbg.value(metadata !{i32 %newfd}, i64 0, metadata !373) nounwind, !dbg !945
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !376) nounwind, !dbg !946
  %12 = load i32* @n_calls.4387, align 4, !dbg !947
  %13 = add nsw i32 %12, 1, !dbg !947
  store i32 %13, i32* @n_calls.4387, align 4, !dbg !947
  tail call void @llvm.dbg.value(metadata !{i32 %newfd}, i64 0, metadata !236) nounwind, !dbg !948
  %14 = icmp ult i32 %newfd, 32
  br i1 %14, label %__get_file.exit.i, label %bb.i9, !dbg !950

__get_file.exit.i:                                ; preds = %bb5
  %15 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %newfd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %15}, i64 0, metadata !374) nounwind, !dbg !949
  %16 = icmp eq %struct.exe_file_t* %15, null, !dbg !951
  br i1 %16, label %bb.i9, label %bb1.i10, !dbg !951

bb.i9:                                            ; preds = %__get_file.exit.i, %bb5
  %17 = tail call i32* @__errno_location() nounwind readnone, !dbg !952
  store i32 9, i32* %17, align 4, !dbg !952
  br label %bb6

bb1.i10:                                          ; preds = %__get_file.exit.i
  %18 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !953
  %19 = icmp eq i32 %18, 0, !dbg !953
  br i1 %19, label %bb4.i, label %bb2.i, !dbg !953

bb2.i:                                            ; preds = %bb1.i10
  %20 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 8), align 4, !dbg !953
  %21 = load i32* %20, align 4, !dbg !953
  %22 = icmp eq i32 %21, %13, !dbg !953
  br i1 %22, label %bb3.i11, label %bb4.i, !dbg !953

bb3.i11:                                          ; preds = %bb2.i
  %23 = add i32 %18, -1
  store i32 %23, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !954
  %24 = tail call i32* @__errno_location() nounwind readnone, !dbg !955
  store i32 5, i32* %24, align 4, !dbg !955
  br label %bb6

bb4.i:                                            ; preds = %bb2.i, %bb1.i10
  %25 = bitcast %struct.exe_file_t* %15 to i8*, !dbg !956
  tail call void @llvm.memset.p0i8.i32(i8* %25, i8 0, i32 20, i32 4, i1 false) nounwind, !dbg !956
  br label %bb6

bb6:                                              ; preds = %bb.i9, %bb4.i, %bb3.i11, %bb3
  %26 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %newfd, i32 0
  %27 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd, i32 0
  %28 = load i32* %27, align 4, !dbg !957
  store i32 %28, i32* %26, align 4, !dbg !957
  %29 = load i32* %1, align 4, !dbg !957
  %30 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %newfd, i32 2
  %31 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd, i32 2
  %32 = load i64* %31, align 4, !dbg !957
  store i64 %32, i64* %30, align 4, !dbg !957
  %33 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %newfd, i32 3
  %34 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd, i32 3
  %35 = load %struct.exe_disk_file_t** %34, align 4, !dbg !957
  store %struct.exe_disk_file_t* %35, %struct.exe_disk_file_t** %33, align 4, !dbg !957
  %36 = and i32 %29, -3, !dbg !958
  store i32 %36, i32* %9, align 4, !dbg !958
  ret i32 %newfd, !dbg !942
}

define i32 @dup(i32 %oldfd) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %oldfd}, i64 0, metadata !383), !dbg !959
  tail call void @llvm.dbg.value(metadata !{i32 %oldfd}, i64 0, metadata !236), !dbg !960
  %0 = icmp ult i32 %oldfd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !962

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !963
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd, i32 1
  %2 = load i32* %1, align 4, !dbg !964
  %3 = and i32 %2, 1, !dbg !964
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !964

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %oldfd
  %phitmp = icmp eq %struct.exe_file_t* %4, null
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !384), !dbg !961
  br i1 %phitmp, label %bb, label %bb4, !dbg !965

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %5 = tail call i32* @__errno_location() nounwind readnone, !dbg !966
  store i32 9, i32* %5, align 4, !dbg !966
  ret i32 -1, !dbg !967

bb2:                                              ; preds = %bb4
  %scevgep = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %10, i32 1
  %6 = load i32* %scevgep, align 4, !dbg !968
  %7 = and i32 %6, 1, !dbg !968
  %8 = icmp eq i32 %7, 0, !dbg !968
  br i1 %8, label %bb7, label %bb3, !dbg !968

bb3:                                              ; preds = %bb2
  %9 = add nsw i32 %10, 1, !dbg !969
  br label %bb4, !dbg !969

bb4:                                              ; preds = %__get_file.exit, %bb3
  %10 = phi i32 [ %9, %bb3 ], [ 0, %__get_file.exit ]
  %11 = icmp slt i32 %10, 32
  br i1 %11, label %bb2, label %bb6, !dbg !969

bb6:                                              ; preds = %bb4
  %12 = tail call i32* @__errno_location() nounwind readnone, !dbg !970
  store i32 24, i32* %12, align 4, !dbg !970
  ret i32 -1, !dbg !967

bb7:                                              ; preds = %bb2
  %13 = tail call i32 @dup2(i32 %oldfd, i32 %10) nounwind, !dbg !971
  ret i32 %13, !dbg !967
}

define i32 @fcntl(i32 %fd, i32 %cmd, ...) nounwind {
entry:
  %ap = alloca i8*, align 4
  call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !388), !dbg !972
  call void @llvm.dbg.value(metadata !{i32 %cmd}, i64 0, metadata !389), !dbg !972
  call void @llvm.dbg.declare(metadata !{i8** %ap}, metadata !392), !dbg !973
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !974
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !976

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !977
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !978
  %3 = and i32 %2, 1, !dbg !978
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !978

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !390), !dbg !975
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !979
  br i1 %5, label %bb, label %bb1, !dbg !979

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = call i32* @__errno_location() nounwind readnone, !dbg !980
  store i32 9, i32* %6, align 4, !dbg !980
  ret i32 -1, !dbg !981

bb1:                                              ; preds = %__get_file.exit
  switch i32 %cmd, label %bb4 [
    i32 1, label %bb16
    i32 3, label %bb16
  ]

bb4:                                              ; preds = %bb1
  %7 = icmp eq i32 %cmd, 9, !dbg !982
  %8 = icmp eq i32 %cmd, 11, !dbg !982
  %9 = or i1 %7, %8, !dbg !982
  %cmd.off = add i32 %cmd, -1025
  %10 = icmp ult i32 %cmd.off, 2
  %or.cond = or i1 %9, %10
  br i1 %or.cond, label %bb16, label %bb13, !dbg !982

bb13:                                             ; preds = %bb4
  %ap14 = bitcast i8** %ap to i8*, !dbg !983
  call void @llvm.va_start(i8* %ap14), !dbg !983
  %11 = load i8** %ap, align 4, !dbg !984
  %12 = getelementptr inbounds i8* %11, i32 4, !dbg !984
  store i8* %12, i8** %ap, align 4, !dbg !984
  %13 = bitcast i8* %11 to i32*, !dbg !984
  %14 = load i32* %13, align 4, !dbg !984
  call void @llvm.dbg.value(metadata !{i32 %14}, i64 0, metadata !395), !dbg !984
  call void @llvm.va_end(i8* %ap14), !dbg !985
  br label %bb16, !dbg !985

bb16:                                             ; preds = %bb4, %bb1, %bb1, %bb13
  %arg.0 = phi i32 [ %14, %bb13 ], [ 0, %bb1 ], [ 0, %bb1 ], [ 0, %bb4 ]
  %15 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %16 = load %struct.exe_disk_file_t** %15, align 4, !dbg !986
  %17 = icmp eq %struct.exe_disk_file_t* %16, null, !dbg !986
  br i1 %17, label %bb27, label %bb17, !dbg !986

bb17:                                             ; preds = %bb16
  switch i32 %cmd, label %bb26 [
    i32 1, label %bb18
    i32 2, label %bb21
    i32 3, label %bb30
  ], !dbg !987

bb18:                                             ; preds = %bb17
  call void @llvm.dbg.value(metadata !562, i64 0, metadata !396), !dbg !988
  %18 = load i32* %1, align 4, !dbg !989
  call void @llvm.dbg.value(metadata !990, i64 0, metadata !396), !dbg !991
  %19 = lshr i32 %18, 1
  %.lobit = and i32 %19, 1
  ret i32 %.lobit, !dbg !981

bb21:                                             ; preds = %bb17
  %20 = load i32* %1, align 4, !dbg !992
  %21 = and i32 %20, -3, !dbg !992
  store i32 %21, i32* %1, align 4, !dbg !992
  %22 = and i32 %arg.0, 1, !dbg !993
  %toBool22 = icmp eq i32 %22, 0
  br i1 %toBool22, label %bb30, label %bb23, !dbg !993

bb23:                                             ; preds = %bb21
  %23 = or i32 %20, 2, !dbg !994
  store i32 %23, i32* %1, align 4, !dbg !994
  ret i32 0, !dbg !981

bb26:                                             ; preds = %bb17
  call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str6, i32 0, i32 0)) nounwind, !dbg !995
  %24 = call i32* @__errno_location() nounwind readnone, !dbg !996
  store i32 22, i32* %24, align 4, !dbg !996
  ret i32 -1, !dbg !981

bb27:                                             ; preds = %bb16
  %25 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %26 = load i32* %25, align 4, !dbg !997
  %27 = call i32 (i32, ...)* @syscall(i32 55, i32 %26, i32 %cmd, i32 %arg.0) nounwind, !dbg !997
  call void @llvm.dbg.value(metadata !{i32 %27}, i64 0, metadata !398), !dbg !997
  %28 = icmp eq i32 %27, -1, !dbg !998
  br i1 %28, label %bb28, label %bb30, !dbg !998

bb28:                                             ; preds = %bb27
  %29 = call i32* @__errno_location() nounwind readnone, !dbg !999
  %30 = call i32 @klee_get_errno() nounwind, !dbg !999
  store i32 %30, i32* %29, align 4, !dbg !999
  ret i32 %27, !dbg !981

bb30:                                             ; preds = %bb27, %bb17, %bb21
  %.0 = phi i32 [ 0, %bb21 ], [ 0, %bb17 ], [ %27, %bb27 ]
  ret i32 %.0, !dbg !981
}

declare void @llvm.va_start(i8*) nounwind

declare void @llvm.va_end(i8*) nounwind

define i32 @ioctl(i32 %fd, i32 %request, ...) nounwind {
entry:
  %ap = alloca i8*, align 4
  call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !400), !dbg !1000
  call void @llvm.dbg.value(metadata !{i32 %request}, i64 0, metadata !401), !dbg !1000
  call void @llvm.dbg.declare(metadata !{i8** %ap}, metadata !404), !dbg !1001
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1002
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !1004

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1005
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !1006
  %3 = and i32 %2, 1, !dbg !1006
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1006

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !402), !dbg !1003
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !1007
  br i1 %5, label %bb, label %bb1, !dbg !1007

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = call i32* @__errno_location() nounwind readnone, !dbg !1008
  store i32 9, i32* %6, align 4, !dbg !1008
  ret i32 -1, !dbg !1009

bb1:                                              ; preds = %__get_file.exit
  %ap2 = bitcast i8** %ap to i8*, !dbg !1010
  call void @llvm.va_start(i8* %ap2), !dbg !1010
  %7 = load i8** %ap, align 4, !dbg !1011
  %8 = getelementptr inbounds i8* %7, i32 4, !dbg !1011
  store i8* %8, i8** %ap, align 4, !dbg !1011
  %9 = bitcast i8* %7 to i8**, !dbg !1011
  %10 = load i8** %9, align 4, !dbg !1011
  call void @llvm.dbg.value(metadata !{i8* %10}, i64 0, metadata !405), !dbg !1011
  call void @llvm.va_end(i8* %ap2), !dbg !1012
  %11 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %12 = load %struct.exe_disk_file_t** %11, align 4, !dbg !1013
  %13 = icmp eq %struct.exe_disk_file_t* %12, null, !dbg !1013
  br i1 %13, label %bb31, label %bb4, !dbg !1013

bb4:                                              ; preds = %bb1
  %14 = getelementptr inbounds %struct.exe_disk_file_t* %12, i32 0, i32 2, !dbg !1014
  %15 = load %struct.stat64** %14, align 4, !dbg !1014
  call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !406), !dbg !1014
  switch i32 %request, label %bb30 [
    i32 21505, label %bb5
    i32 21506, label %bb8
    i32 21507, label %bb11
    i32 21508, label %bb14
    i32 21523, label %bb17
    i32 21524, label %bb20
    i32 21531, label %bb23
    i32 -2145620734, label %bb29
  ], !dbg !1015

bb5:                                              ; preds = %bb4
  call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !430), !dbg !1016
  call void @klee_warning_once(i8* getelementptr inbounds ([41 x i8]* @.str7, i32 0, i32 0)) nounwind, !dbg !1017
  %16 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %17 = load i32* %16, align 4, !dbg !1018
  %18 = and i32 %17, 61440, !dbg !1018
  %19 = icmp eq i32 %18, 8192, !dbg !1018
  br i1 %19, label %bb6, label %bb7, !dbg !1018

bb6:                                              ; preds = %bb5
  %20 = bitcast i8* %10 to i32*
  store i32 27906, i32* %20, align 4, !dbg !1019
  %21 = getelementptr inbounds i8* %10, i32 4
  %22 = bitcast i8* %21 to i32*
  store i32 5, i32* %22, align 4, !dbg !1020
  %23 = getelementptr inbounds i8* %10, i32 8
  %24 = bitcast i8* %23 to i32*
  store i32 1215, i32* %24, align 4, !dbg !1021
  %25 = getelementptr inbounds i8* %10, i32 12
  %26 = bitcast i8* %25 to i32*
  store i32 35287, i32* %26, align 4, !dbg !1022
  %27 = getelementptr inbounds i8* %10, i32 16
  store i8 0, i8* %27, align 4, !dbg !1023
  %28 = getelementptr inbounds i8* %10, i32 17
  store i8 3, i8* %28, align 1, !dbg !1024
  %29 = getelementptr inbounds i8* %10, i32 18
  store i8 28, i8* %29, align 1, !dbg !1025
  %30 = getelementptr inbounds i8* %10, i32 19
  store i8 127, i8* %30, align 1, !dbg !1026
  %31 = getelementptr inbounds i8* %10, i32 20
  store i8 21, i8* %31, align 1, !dbg !1027
  %32 = getelementptr inbounds i8* %10, i32 21
  store i8 4, i8* %32, align 1, !dbg !1028
  %33 = getelementptr inbounds i8* %10, i32 22
  store i8 0, i8* %33, align 1, !dbg !1029
  %34 = getelementptr inbounds i8* %10, i32 23
  store i8 1, i8* %34, align 1, !dbg !1030
  %35 = getelementptr inbounds i8* %10, i32 24
  store i8 -1, i8* %35, align 1, !dbg !1031
  %36 = getelementptr inbounds i8* %10, i32 25
  store i8 17, i8* %36, align 1, !dbg !1032
  %37 = getelementptr inbounds i8* %10, i32 26
  store i8 19, i8* %37, align 1, !dbg !1033
  %38 = getelementptr inbounds i8* %10, i32 27
  store i8 26, i8* %38, align 1, !dbg !1034
  %39 = getelementptr inbounds i8* %10, i32 28
  store i8 -1, i8* %39, align 1, !dbg !1035
  %40 = getelementptr inbounds i8* %10, i32 29
  store i8 18, i8* %40, align 1, !dbg !1036
  %41 = getelementptr inbounds i8* %10, i32 30
  store i8 15, i8* %41, align 1, !dbg !1037
  %42 = getelementptr inbounds i8* %10, i32 31
  store i8 23, i8* %42, align 1, !dbg !1038
  %43 = getelementptr inbounds i8* %10, i32 32
  store i8 22, i8* %43, align 1, !dbg !1039
  %44 = getelementptr inbounds i8* %10, i32 33
  store i8 -1, i8* %44, align 1, !dbg !1040
  %45 = getelementptr inbounds i8* %10, i32 34
  store i8 0, i8* %45, align 1, !dbg !1041
  %46 = getelementptr inbounds i8* %10, i32 35
  store i8 0, i8* %46, align 1, !dbg !1042
  ret i32 0, !dbg !1009

bb7:                                              ; preds = %bb5
  %47 = call i32* @__errno_location() nounwind readnone, !dbg !1043
  store i32 25, i32* %47, align 4, !dbg !1043
  ret i32 -1, !dbg !1009

bb8:                                              ; preds = %bb4
  call void @klee_warning_once(i8* getelementptr inbounds ([42 x i8]* @.str8, i32 0, i32 0)) nounwind, !dbg !1044
  %48 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %49 = load i32* %48, align 4, !dbg !1045
  %50 = and i32 %49, 61440, !dbg !1045
  %51 = icmp eq i32 %50, 8192, !dbg !1045
  br i1 %51, label %bb34, label %bb10, !dbg !1045

bb10:                                             ; preds = %bb8
  %52 = call i32* @__errno_location() nounwind readnone, !dbg !1046
  store i32 25, i32* %52, align 4, !dbg !1046
  ret i32 -1, !dbg !1009

bb11:                                             ; preds = %bb4
  call void @klee_warning_once(i8* getelementptr inbounds ([43 x i8]* @.str9, i32 0, i32 0)) nounwind, !dbg !1047
  %53 = icmp eq i32 %fd, 0, !dbg !1048
  br i1 %53, label %bb34, label %bb13, !dbg !1048

bb13:                                             ; preds = %bb11
  %54 = call i32* @__errno_location() nounwind readnone, !dbg !1049
  store i32 25, i32* %54, align 4, !dbg !1049
  ret i32 -1, !dbg !1009

bb14:                                             ; preds = %bb4
  call void @klee_warning_once(i8* getelementptr inbounds ([43 x i8]* @.str10, i32 0, i32 0)) nounwind, !dbg !1050
  %55 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %56 = load i32* %55, align 4, !dbg !1051
  %57 = and i32 %56, 61440, !dbg !1051
  %58 = icmp eq i32 %57, 8192, !dbg !1051
  br i1 %58, label %bb34, label %bb16, !dbg !1051

bb16:                                             ; preds = %bb14
  %59 = call i32* @__errno_location() nounwind readnone, !dbg !1052
  store i32 25, i32* %59, align 4, !dbg !1052
  ret i32 -1, !dbg !1009

bb17:                                             ; preds = %bb4
  call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !448), !dbg !1053
  %60 = bitcast i8* %10 to i16*
  store i16 24, i16* %60, align 2, !dbg !1054
  %61 = getelementptr inbounds i8* %10, i32 2
  %62 = bitcast i8* %61 to i16*
  store i16 80, i16* %62, align 2, !dbg !1055
  call void @klee_warning_once(i8* getelementptr inbounds ([45 x i8]* @.str11, i32 0, i32 0)) nounwind, !dbg !1056
  %63 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %64 = load i32* %63, align 4, !dbg !1057
  %65 = and i32 %64, 61440, !dbg !1057
  %66 = icmp eq i32 %65, 8192, !dbg !1057
  br i1 %66, label %bb34, label %bb19, !dbg !1057

bb19:                                             ; preds = %bb17
  %67 = call i32* @__errno_location() nounwind readnone, !dbg !1058
  store i32 25, i32* %67, align 4, !dbg !1058
  ret i32 -1, !dbg !1009

bb20:                                             ; preds = %bb4
  call void @klee_warning_once(i8* getelementptr inbounds ([46 x i8]* @.str12, i32 0, i32 0)) nounwind, !dbg !1059
  %68 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %69 = load i32* %68, align 4, !dbg !1060
  %70 = and i32 %69, 61440, !dbg !1060
  %71 = icmp eq i32 %70, 8192, !dbg !1060
  %72 = call i32* @__errno_location() nounwind readnone, !dbg !1061
  br i1 %71, label %bb21, label %bb22, !dbg !1060

bb21:                                             ; preds = %bb20
  store i32 22, i32* %72, align 4, !dbg !1061
  ret i32 -1, !dbg !1009

bb22:                                             ; preds = %bb20
  store i32 25, i32* %72, align 4, !dbg !1062
  ret i32 -1, !dbg !1009

bb23:                                             ; preds = %bb4
  %73 = bitcast i8* %10 to i32*, !dbg !1063
  call void @llvm.dbg.value(metadata !{i32* %73}, i64 0, metadata !458), !dbg !1063
  call void @klee_warning_once(i8* getelementptr inbounds ([43 x i8]* @.str13, i32 0, i32 0)) nounwind, !dbg !1064
  %74 = getelementptr inbounds %struct.stat64* %15, i32 0, i32 3
  %75 = load i32* %74, align 4, !dbg !1065
  %76 = and i32 %75, 61440, !dbg !1065
  %77 = icmp eq i32 %76, 8192, !dbg !1065
  br i1 %77, label %bb24, label %bb28, !dbg !1065

bb24:                                             ; preds = %bb23
  %78 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %79 = load i64* %78, align 4, !dbg !1066
  %80 = load %struct.exe_disk_file_t** %11, align 4, !dbg !1066
  %81 = getelementptr inbounds %struct.exe_disk_file_t* %80, i32 0, i32 0, !dbg !1066
  %82 = load i32* %81, align 4, !dbg !1066
  %83 = zext i32 %82 to i64, !dbg !1066
  %84 = icmp slt i64 %79, %83, !dbg !1066
  br i1 %84, label %bb25, label %bb27, !dbg !1066

bb25:                                             ; preds = %bb24
  %85 = trunc i64 %79 to i32, !dbg !1067
  %86 = sub i32 %82, %85, !dbg !1067
  br label %bb27, !dbg !1067

bb27:                                             ; preds = %bb24, %bb25
  %storemerge = phi i32 [ %86, %bb25 ], [ 0, %bb24 ]
  store i32 %storemerge, i32* %73, align 4
  ret i32 0, !dbg !1009

bb28:                                             ; preds = %bb23
  %87 = call i32* @__errno_location() nounwind readnone, !dbg !1068
  store i32 25, i32* %87, align 4, !dbg !1068
  ret i32 -1, !dbg !1009

bb29:                                             ; preds = %bb4
  call void @klee_warning(i8* getelementptr inbounds ([44 x i8]* @.str14, i32 0, i32 0)) nounwind, !dbg !1069
  %88 = call i32* @__errno_location() nounwind readnone, !dbg !1070
  store i32 22, i32* %88, align 4, !dbg !1070
  ret i32 -1, !dbg !1009

bb30:                                             ; preds = %bb4
  call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str6, i32 0, i32 0)) nounwind, !dbg !1071
  %89 = call i32* @__errno_location() nounwind readnone, !dbg !1072
  store i32 22, i32* %89, align 4, !dbg !1072
  ret i32 -1, !dbg !1009

bb31:                                             ; preds = %bb1
  %90 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %91 = load i32* %90, align 4, !dbg !1073
  %92 = call i32 (i32, ...)* @syscall(i32 54, i32 %91, i32 %request, i8* %10) nounwind, !dbg !1073
  call void @llvm.dbg.value(metadata !{i32 %92}, i64 0, metadata !461), !dbg !1073
  %93 = icmp eq i32 %92, -1, !dbg !1074
  br i1 %93, label %bb32, label %bb34, !dbg !1074

bb32:                                             ; preds = %bb31
  %94 = call i32* @__errno_location() nounwind readnone, !dbg !1075
  %95 = call i32 @klee_get_errno() nounwind, !dbg !1075
  store i32 %95, i32* %94, align 4, !dbg !1075
  ret i32 %92, !dbg !1009

bb34:                                             ; preds = %bb31, %bb17, %bb14, %bb11, %bb8
  %.0 = phi i32 [ 0, %bb8 ], [ 0, %bb11 ], [ 0, %bb14 ], [ 0, %bb17 ], [ %92, %bb31 ]
  ret i32 %.0, !dbg !1009
}

declare void @klee_warning_once(i8*)

define i32 @__fd_getdents(i32 %fd, %struct.dirent64* %dirp, i32 %count) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !463), !dbg !1076
  tail call void @llvm.dbg.value(metadata !{%struct.dirent64* %dirp}, i64 0, metadata !464), !dbg !1076
  tail call void @llvm.dbg.value(metadata !{i32 %count}, i64 0, metadata !465), !dbg !1076
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1077
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !1079

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1080
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !1081
  %3 = and i32 %2, 1, !dbg !1081
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1081

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !466), !dbg !1078
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !1082
  br i1 %5, label %bb, label %bb1, !dbg !1082

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !1083
  store i32 9, i32* %6, align 4, !dbg !1083
  ret i32 -1, !dbg !1084

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !1085
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !1085
  br i1 %9, label %bb3, label %bb2, !dbg !1085

bb2:                                              ; preds = %bb1
  tail call void @klee_warning(i8* getelementptr inbounds ([33 x i8]* @.str6, i32 0, i32 0)) nounwind, !dbg !1086
  %10 = tail call i32* @__errno_location() nounwind readnone, !dbg !1087
  store i32 22, i32* %10, align 4, !dbg !1087
  ret i32 -1, !dbg !1084

bb3:                                              ; preds = %bb1
  %11 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %12 = load i64* %11, align 4, !dbg !1088
  %13 = trunc i64 %12 to i32, !dbg !1088
  %14 = icmp ult i32 %13, 4096
  br i1 %14, label %bb4, label %bb10, !dbg !1088

bb4:                                              ; preds = %bb3
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !471), !dbg !1089
  %15 = sdiv i64 %12, 276, !dbg !1090
  %16 = trunc i64 %15 to i32, !dbg !1090
  tail call void @llvm.dbg.value(metadata !{i32 %16}, i64 0, metadata !468), !dbg !1090
  %17 = mul i32 %16, 276, !dbg !1091
  %18 = zext i32 %17 to i64, !dbg !1091
  %19 = icmp eq i64 %18, %12, !dbg !1091
  br i1 %19, label %bb5, label %bb6, !dbg !1091

bb5:                                              ; preds = %bb4
  %20 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1091
  %21 = icmp ult i32 %20, %16, !dbg !1091
  br i1 %21, label %bb6, label %bb8.preheader, !dbg !1091

bb8.preheader:                                    ; preds = %bb5
  %22 = icmp ugt i32 %20, %16, !dbg !1092
  br i1 %22, label %bb.nph26, label %bb9, !dbg !1092

bb6:                                              ; preds = %bb4, %bb5
  %23 = tail call i32* @__errno_location() nounwind readnone, !dbg !1093
  store i32 22, i32* %23, align 4, !dbg !1093
  ret i32 -1, !dbg !1084

bb.nph26:                                         ; preds = %bb8.preheader
  %tmp38 = add i32 %16, 65
  %tmp48 = add i32 %17, 276
  %tmp50 = add i32 %16, 1
  br label %bb7

bb7:                                              ; preds = %bb.nph26, %bb7
  %indvar = phi i32 [ 0, %bb.nph26 ], [ %tmp33, %bb7 ]
  %bytes.024 = phi i32 [ 0, %bb.nph26 ], [ %35, %bb7 ]
  %scevgep29 = getelementptr inbounds %struct.dirent64* %dirp, i32 %indvar, i32 0
  %scevgep30 = getelementptr %struct.dirent64* %dirp, i32 %indvar, i32 2
  %scevgep31 = getelementptr %struct.dirent64* %dirp, i32 %indvar, i32 3
  %scevgep32 = getelementptr %struct.dirent64* %dirp, i32 %indvar, i32 1
  %tmp33 = add i32 %indvar, 1
  %scevgep35 = getelementptr %struct.dirent64* %dirp, i32 %indvar, i32 4, i32 0
  %scevgep36 = getelementptr %struct.dirent64* %dirp, i32 %indvar, i32 4, i32 1
  %tmp40 = add i32 %tmp38, %indvar
  %tmp41 = trunc i32 %tmp40 to i8
  %tmp43 = add i32 %16, %indvar
  %tmp46 = mul i32 %indvar, 276
  %tmp49 = add i32 %tmp48, %tmp46
  %24 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !1094
  %scevgep44 = getelementptr %struct.exe_disk_file_t* %24, i32 %tmp43, i32 2
  %25 = load %struct.stat64** %scevgep44, align 4, !dbg !1095
  %26 = getelementptr inbounds %struct.stat64* %25, i32 0, i32 15, !dbg !1095
  %27 = load i64* %26, align 4, !dbg !1095
  store i64 %27, i64* %scevgep29, align 4, !dbg !1095
  store i16 276, i16* %scevgep30, align 4, !dbg !1096
  %28 = load %struct.stat64** %scevgep44, align 4, !dbg !1097
  %29 = getelementptr inbounds %struct.stat64* %28, i32 0, i32 3, !dbg !1097
  %30 = load i32* %29, align 4, !dbg !1097
  %31 = lshr i32 %30, 12
  %32 = and i32 %31, 15
  %33 = trunc i32 %32 to i8, !dbg !1097
  store i8 %33, i8* %scevgep31, align 2, !dbg !1097
  store i8 %tmp41, i8* %scevgep35, align 1, !dbg !1098
  store i8 0, i8* %scevgep36, align 1, !dbg !1099
  %34 = zext i32 %tmp49 to i64, !dbg !1100
  store i64 %34, i64* %scevgep32, align 4, !dbg !1100
  %35 = add i32 %bytes.024, 276, !dbg !1101
  %tmp51 = add i32 %tmp50, %indvar
  %36 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1092
  %37 = icmp ugt i32 %36, %tmp51, !dbg !1092
  br i1 %37, label %bb7, label %bb8.bb9_crit_edge, !dbg !1092

bb8.bb9_crit_edge:                                ; preds = %bb7
  %scevgep34 = getelementptr %struct.dirent64* %dirp, i32 %tmp33
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !472), !dbg !1094
  tail call void @llvm.dbg.value(metadata !{i32 %35}, i64 0, metadata !471), !dbg !1101
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !464), !dbg !1102
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !468), !dbg !1092
  br label %bb9

bb9:                                              ; preds = %bb8.bb9_crit_edge, %bb8.preheader
  %dirp_addr.0.lcssa = phi %struct.dirent64* [ %scevgep34, %bb8.bb9_crit_edge ], [ %dirp, %bb8.preheader ]
  %bytes.0.lcssa = phi i32 [ %35, %bb8.bb9_crit_edge ], [ 0, %bb8.preheader ]
  %38 = icmp ugt i32 %count, 4096
  %min = select i1 %38, i32 4096, i32 %count, !dbg !1103
  tail call void @llvm.dbg.value(metadata !{i32 %min}, i64 0, metadata !470), !dbg !1103
  %39 = getelementptr inbounds %struct.dirent64* %dirp_addr.0.lcssa, i32 0, i32 0, !dbg !1104
  store i64 0, i64* %39, align 4, !dbg !1104
  %40 = trunc i32 %min to i16, !dbg !1105
  %41 = trunc i32 %bytes.0.lcssa to i16, !dbg !1105
  %42 = sub i16 %40, %41, !dbg !1105
  %43 = getelementptr inbounds %struct.dirent64* %dirp_addr.0.lcssa, i32 0, i32 2, !dbg !1105
  store i16 %42, i16* %43, align 4, !dbg !1105
  %44 = getelementptr inbounds %struct.dirent64* %dirp_addr.0.lcssa, i32 0, i32 3, !dbg !1106
  store i8 0, i8* %44, align 2, !dbg !1106
  %45 = getelementptr inbounds %struct.dirent64* %dirp_addr.0.lcssa, i32 0, i32 4, i32 0
  store i8 0, i8* %45, align 1, !dbg !1107
  %46 = getelementptr inbounds %struct.dirent64* %dirp_addr.0.lcssa, i32 0, i32 1, !dbg !1108
  store i64 4096, i64* %46, align 4, !dbg !1108
  %47 = zext i16 %42 to i32, !dbg !1109
  %48 = add i32 %47, %bytes.0.lcssa, !dbg !1109
  tail call void @llvm.dbg.value(metadata !{i32 %48}, i64 0, metadata !471), !dbg !1109
  %49 = zext i32 %min to i64, !dbg !1110
  store i64 %49, i64* %11, align 4, !dbg !1110
  ret i32 %48, !dbg !1084

bb10:                                             ; preds = %bb3
  %50 = add i32 %13, -4096
  tail call void @llvm.dbg.value(metadata !{i32 %50}, i64 0, metadata !474), !dbg !1111
  %51 = bitcast %struct.dirent64* %dirp to i8*, !dbg !1112
  tail call void @llvm.memset.p0i8.i32(i8* %51, i8 0, i32 %count, i32 4, i1 false), !dbg !1112
  %52 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %53 = load i32* %52, align 4, !dbg !1113
  %54 = tail call i32 (i32, ...)* @syscall(i32 19, i32 %53, i32 %50, i32 0) nounwind, !dbg !1113
  tail call void @llvm.dbg.value(metadata !{i32 %54}, i64 0, metadata !477), !dbg !1113
  %55 = icmp eq i32 %54, -1, !dbg !1114
  br i1 %55, label %bb11, label %bb12, !dbg !1114

bb11:                                             ; preds = %bb10
  tail call void @__assert_fail(i8* getelementptr inbounds ([18 x i8]* @.str15, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str16, i32 0, i32 0), i32 735, i8* getelementptr inbounds ([14 x i8]* @__PRETTY_FUNCTION__.4792, i32 0, i32 0)) noreturn nounwind, !dbg !1114
  unreachable, !dbg !1114

bb12:                                             ; preds = %bb10
  %56 = load i32* %52, align 4, !dbg !1115
  %57 = tail call i32 (i32, ...)* @syscall(i32 220, i32 %56, %struct.dirent64* %dirp, i32 %count) nounwind, !dbg !1115
  tail call void @llvm.dbg.value(metadata !{i32 %57}, i64 0, metadata !476), !dbg !1115
  %58 = icmp eq i32 %57, -1, !dbg !1116
  br i1 %58, label %bb13, label %bb14, !dbg !1116

bb13:                                             ; preds = %bb12
  %59 = tail call i32* @__errno_location() nounwind readnone, !dbg !1117
  %60 = tail call i32 @klee_get_errno() nounwind, !dbg !1117
  store i32 %60, i32* %59, align 4, !dbg !1117
  ret i32 %57, !dbg !1084

bb14:                                             ; preds = %bb12
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !478), !dbg !1118
  %61 = load i32* %52, align 4, !dbg !1119
  %62 = tail call i32 (i32, ...)* @syscall(i32 19, i32 %61, i32 0, i32 1) nounwind, !dbg !1119
  %63 = add nsw i32 %62, 4096, !dbg !1119
  %64 = sext i32 %63 to i64, !dbg !1119
  store i64 %64, i64* %11, align 4, !dbg !1119
  %65 = icmp sgt i32 %57, 0, !dbg !1120
  br i1 %65, label %bb15, label %bb18, !dbg !1120

bb15:                                             ; preds = %bb14, %bb15
  %pos.022 = phi i32 [ %74, %bb15 ], [ 0, %bb14 ]
  %.sum = add i32 %pos.022, 8
  %66 = getelementptr inbounds i8* %51, i32 %.sum
  %67 = bitcast i8* %66 to i64*
  %68 = load i64* %67, align 4, !dbg !1121
  %69 = add nsw i64 %68, 4096, !dbg !1121
  store i64 %69, i64* %67, align 4, !dbg !1121
  %.sum21 = add i32 %pos.022, 16
  %70 = getelementptr inbounds i8* %51, i32 %.sum21
  %71 = bitcast i8* %70 to i16*
  %72 = load i16* %71, align 4, !dbg !1122
  %73 = zext i16 %72 to i32, !dbg !1122
  %74 = add nsw i32 %73, %pos.022, !dbg !1122
  %75 = icmp slt i32 %74, %57, !dbg !1120
  br i1 %75, label %bb15, label %bb18, !dbg !1120

bb18:                                             ; preds = %bb15, %bb14
  ret i32 %57, !dbg !1084
}

declare void @__assert_fail(i8*, i8*, i32, i8*) noreturn nounwind

define i64 @__fd_lseek(i32 %fd, i64 %offset, i32 %whence) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !482), !dbg !1123
  tail call void @llvm.dbg.value(metadata !{i64 %offset}, i64 0, metadata !483), !dbg !1123
  tail call void @llvm.dbg.value(metadata !{i32 %whence}, i64 0, metadata !484), !dbg !1123
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1124
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !1126

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1127
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !1128
  %3 = and i32 %2, 1, !dbg !1128
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1128

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !487), !dbg !1125
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !1129
  br i1 %5, label %bb, label %bb1, !dbg !1129

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !1130
  store i32 9, i32* %6, align 4, !dbg !1130
  ret i64 -1, !dbg !1131

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !1132
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !1132
  br i1 %9, label %bb2, label %bb11, !dbg !1132

bb2:                                              ; preds = %bb1
  %10 = icmp eq i32 %whence, 0, !dbg !1133
  br i1 %10, label %bb3, label %bb4, !dbg !1133

bb3:                                              ; preds = %bb2
  %11 = trunc i64 %offset to i32, !dbg !1134
  %12 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %13 = load i32* %12, align 4, !dbg !1134
  %14 = tail call i32 (i32, ...)* @syscall(i32 19, i32 %13, i32 %11, i32 0) nounwind, !dbg !1134
  %15 = sext i32 %14 to i64, !dbg !1134
  tail call void @llvm.dbg.value(metadata !{i64 %15}, i64 0, metadata !485), !dbg !1134
  br label %bb8, !dbg !1134

bb4:                                              ; preds = %bb2
  %16 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %17 = load i64* %16, align 4, !dbg !1135
  %18 = trunc i64 %17 to i32, !dbg !1135
  %19 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %20 = load i32* %19, align 4, !dbg !1135
  %21 = tail call i32 (i32, ...)* @syscall(i32 19, i32 %20, i32 %18, i32 0) nounwind, !dbg !1135
  %22 = sext i32 %21 to i64, !dbg !1135
  tail call void @llvm.dbg.value(metadata !{i64 %22}, i64 0, metadata !485), !dbg !1135
  %23 = icmp eq i32 %21, -1
  br i1 %23, label %bb8, label %bb5, !dbg !1136

bb5:                                              ; preds = %bb4
  %24 = load i64* %16, align 4, !dbg !1137
  %25 = icmp eq i64 %24, %22, !dbg !1137
  br i1 %25, label %bb7, label %bb6, !dbg !1137

bb6:                                              ; preds = %bb5
  tail call void @__assert_fail(i8* getelementptr inbounds ([18 x i8]* @.str17, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str16, i32 0, i32 0), i32 397, i8* getelementptr inbounds ([11 x i8]* @__PRETTY_FUNCTION__.4536, i32 0, i32 0)) noreturn nounwind, !dbg !1137
  unreachable, !dbg !1137

bb7:                                              ; preds = %bb5
  %26 = trunc i64 %offset to i32, !dbg !1138
  %27 = load i32* %19, align 4, !dbg !1138
  %28 = tail call i32 (i32, ...)* @syscall(i32 19, i32 %27, i32 %26, i32 %whence) nounwind, !dbg !1138
  %29 = sext i32 %28 to i64, !dbg !1138
  tail call void @llvm.dbg.value(metadata !{i64 %29}, i64 0, metadata !485), !dbg !1138
  br label %bb8, !dbg !1138

bb8:                                              ; preds = %bb4, %bb7, %bb3
  %new_off.0 = phi i64 [ %15, %bb3 ], [ %29, %bb7 ], [ %22, %bb4 ]
  %30 = icmp eq i64 %new_off.0, -1, !dbg !1139
  br i1 %30, label %bb9, label %bb10, !dbg !1139

bb9:                                              ; preds = %bb8
  %31 = tail call i32* @__errno_location() nounwind readnone, !dbg !1140
  %32 = tail call i32 @klee_get_errno() nounwind, !dbg !1140
  store i32 %32, i32* %31, align 4, !dbg !1140
  ret i64 -1, !dbg !1131

bb10:                                             ; preds = %bb8
  %33 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  store i64 %new_off.0, i64* %33, align 4, !dbg !1141
  ret i64 %new_off.0, !dbg !1131

bb11:                                             ; preds = %bb1
  switch i32 %whence, label %bb15 [
    i32 0, label %bb16
    i32 1, label %bb13
    i32 2, label %bb14
  ], !dbg !1142

bb13:                                             ; preds = %bb11
  %34 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %35 = load i64* %34, align 4, !dbg !1143
  %36 = add nsw i64 %35, %offset, !dbg !1143
  tail call void @llvm.dbg.value(metadata !{i64 %36}, i64 0, metadata !485), !dbg !1143
  br label %bb16, !dbg !1143

bb14:                                             ; preds = %bb11
  %37 = getelementptr inbounds %struct.exe_disk_file_t* %8, i32 0, i32 0, !dbg !1144
  %38 = load i32* %37, align 4, !dbg !1144
  %39 = zext i32 %38 to i64, !dbg !1144
  %40 = add nsw i64 %39, %offset, !dbg !1144
  tail call void @llvm.dbg.value(metadata !{i64 %40}, i64 0, metadata !485), !dbg !1144
  br label %bb16, !dbg !1144

bb15:                                             ; preds = %bb11
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !1145
  store i32 22, i32* %41, align 4, !dbg !1145
  ret i64 -1, !dbg !1131

bb16:                                             ; preds = %bb11, %bb14, %bb13
  %new_off.1 = phi i64 [ %40, %bb14 ], [ %36, %bb13 ], [ %offset, %bb11 ]
  %42 = icmp slt i64 %new_off.1, 0, !dbg !1146
  br i1 %42, label %bb17, label %bb18, !dbg !1146

bb17:                                             ; preds = %bb16
  %43 = tail call i32* @__errno_location() nounwind readnone, !dbg !1147
  store i32 22, i32* %43, align 4, !dbg !1147
  ret i64 -1, !dbg !1131

bb18:                                             ; preds = %bb16
  %44 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  store i64 %new_off.1, i64* %44, align 4, !dbg !1148
  ret i64 %new_off.1, !dbg !1131
}

define i32 @__fd_fstat(i32 %fd, %struct.stat64* %buf) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !488), !dbg !1149
  tail call void @llvm.dbg.value(metadata !{%struct.stat64* %buf}, i64 0, metadata !489), !dbg !1149
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1150
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !1152

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1153
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !1154
  %3 = and i32 %2, 1, !dbg !1154
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1154

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !490), !dbg !1151
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !1155
  br i1 %5, label %bb, label %bb1, !dbg !1155

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !1156
  store i32 9, i32* %6, align 4, !dbg !1156
  ret i32 -1, !dbg !1157

bb1:                                              ; preds = %__get_file.exit
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %8 = load %struct.exe_disk_file_t** %7, align 4, !dbg !1158
  %9 = icmp eq %struct.exe_disk_file_t* %8, null, !dbg !1158
  br i1 %9, label %bb2, label %bb5, !dbg !1158

bb2:                                              ; preds = %bb1
  %10 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %11 = load i32* %10, align 4, !dbg !1159
  %12 = tail call i32 (i32, ...)* @syscall(i32 197, i32 %11, %struct.stat64* %buf) nounwind, !dbg !1159
  tail call void @llvm.dbg.value(metadata !{i32 %12}, i64 0, metadata !492), !dbg !1159
  %13 = icmp eq i32 %12, -1, !dbg !1160
  br i1 %13, label %bb3, label %bb6, !dbg !1160

bb3:                                              ; preds = %bb2
  %14 = tail call i32* @__errno_location() nounwind readnone, !dbg !1161
  %15 = tail call i32 @klee_get_errno() nounwind, !dbg !1161
  store i32 %15, i32* %14, align 4, !dbg !1161
  ret i32 %12, !dbg !1157

bb5:                                              ; preds = %bb1
  %16 = getelementptr inbounds %struct.exe_disk_file_t* %8, i32 0, i32 2, !dbg !1162
  %17 = load %struct.stat64** %16, align 4, !dbg !1162
  %18 = bitcast %struct.stat64* %buf to i8*, !dbg !1162
  %19 = bitcast %struct.stat64* %17 to i8*, !dbg !1162
  tail call void @llvm.memcpy.p0i8.p0i8.i32(i8* %18, i8* %19, i32 96, i32 4, i1 false), !dbg !1162
  ret i32 0, !dbg !1157

bb6:                                              ; preds = %bb2
  ret i32 %12, !dbg !1157
}

define i32 @__fd_lstat(i8* %path, %struct.stat64* %buf) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !494), !dbg !1163
  tail call void @llvm.dbg.value(metadata !{%struct.stat64* %buf}, i64 0, metadata !495), !dbg !1163
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !1164
  %0 = load i8* %path, align 1, !dbg !1166
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !1166
  %1 = icmp eq i8 %0, 0, !dbg !1167
  br i1 %1, label %bb1, label %bb.i, !dbg !1167

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !1167
  %3 = load i8* %2, align 1, !dbg !1167
  %4 = icmp eq i8 %3, 0, !dbg !1167
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !1167

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1168
  %6 = sext i8 %0 to i32, !dbg !1169
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !1169
  %9 = icmp eq i32 %6, %8, !dbg !1169
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !1169

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !1170
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !1170
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !1171
  %12 = load %struct.stat64** %11, align 4, !dbg !1171
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !1171
  %14 = load i64* %13, align 4, !dbg !1171
  %15 = icmp eq i64 %14, 0, !dbg !1171
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !1171

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !1168
  br label %bb8.i, !dbg !1168

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !1168
  br i1 %18, label %bb3.i, label %bb1, !dbg !1168

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !1170
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %19}, i64 0, metadata !496), !dbg !1165
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !1172
  br i1 %20, label %bb1, label %bb, !dbg !1172

bb:                                               ; preds = %__get_sym_file.exit
  %21 = bitcast %struct.stat64* %buf to i8*, !dbg !1173
  %22 = bitcast %struct.stat64* %12 to i8*, !dbg !1173
  tail call void @llvm.memcpy.p0i8.p0i8.i32(i8* %21, i8* %22, i32 96, i32 4, i1 false), !dbg !1173
  ret i32 0, !dbg !1174

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !1175
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !1177
  %23 = ptrtoint i8* %path to i32, !dbg !1179
  %24 = tail call i32 @klee_get_valuel(i32 %23) nounwind, !dbg !1179
  %25 = inttoptr i32 %24 to i8*, !dbg !1179
  tail call void @llvm.dbg.value(metadata !{i8* %25}, i64 0, metadata !303) nounwind, !dbg !1179
  %26 = icmp eq i8* %25, %path, !dbg !1180
  %27 = zext i1 %26 to i32, !dbg !1180
  tail call void @klee_assume(i32 %27) nounwind, !dbg !1180
  tail call void @llvm.dbg.value(metadata !{i8* %25}, i64 0, metadata !313) nounwind, !dbg !1178
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !1181
  br label %bb.i6, !dbg !1181

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %25, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %28 = phi i32 [ 0, %bb1 ], [ %40, %bb6.i8 ]
  %tmp.i = add i32 %28, -1
  %29 = load i8* %sc.1.i, align 1, !dbg !1182
  %30 = and i32 %tmp.i, %28, !dbg !1183
  %31 = icmp eq i32 %30, 0, !dbg !1183
  br i1 %31, label %bb1.i, label %bb5.i, !dbg !1183

bb1.i:                                            ; preds = %bb.i6
  switch i8 %29, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %29}, i64 0, metadata !316) nounwind, !dbg !1182
  store i8 0, i8* %sc.1.i, align 1, !dbg !1184
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !1184
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !1185
  %32 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1185
  br label %bb6.i8, !dbg !1185

bb5.i:                                            ; preds = %bb.i6
  %33 = sext i8 %29 to i32, !dbg !1186
  %34 = tail call i32 @klee_get_valuel(i32 %33) nounwind, !dbg !1186
  %35 = trunc i32 %34 to i8, !dbg !1186
  %36 = icmp eq i8 %35, %29, !dbg !1187
  %37 = zext i1 %36 to i32, !dbg !1187
  tail call void @klee_assume(i32 %37) nounwind, !dbg !1187
  store i8 %35, i8* %sc.1.i, align 1, !dbg !1188
  %38 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1188
  %39 = icmp eq i8 %35, 0, !dbg !1189
  br i1 %39, label %__concretize_string.exit, label %bb6.i8, !dbg !1189

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %32, %bb4.i7 ], [ %38, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %40 = add i32 %28, 1, !dbg !1181
  br label %bb.i6, !dbg !1181

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %41 = tail call i32 (i32, ...)* @syscall(i32 196, i8* %path, %struct.stat64* %buf) nounwind, !dbg !1176
  tail call void @llvm.dbg.value(metadata !{i32 %41}, i64 0, metadata !498), !dbg !1176
  %42 = icmp eq i32 %41, -1, !dbg !1190
  br i1 %42, label %bb2, label %bb4, !dbg !1190

bb2:                                              ; preds = %__concretize_string.exit
  %43 = tail call i32* @__errno_location() nounwind readnone, !dbg !1191
  %44 = tail call i32 @klee_get_errno() nounwind, !dbg !1191
  store i32 %44, i32* %43, align 4, !dbg !1191
  ret i32 %41, !dbg !1174

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %41, !dbg !1174
}

define i32 @__fd_stat(i8* %path, %struct.stat64* %buf) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !500), !dbg !1192
  tail call void @llvm.dbg.value(metadata !{%struct.stat64* %buf}, i64 0, metadata !501), !dbg !1192
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !1193
  %0 = load i8* %path, align 1, !dbg !1195
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !1195
  %1 = icmp eq i8 %0, 0, !dbg !1196
  br i1 %1, label %bb1, label %bb.i, !dbg !1196

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !1196
  %3 = load i8* %2, align 1, !dbg !1196
  %4 = icmp eq i8 %3, 0, !dbg !1196
  br i1 %4, label %bb8.preheader.i, label %bb1, !dbg !1196

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1197
  %6 = sext i8 %0 to i32, !dbg !1198
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %17, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !1198
  %9 = icmp eq i32 %6, %8, !dbg !1198
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !1198

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !1199
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !1199
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, i32 2, !dbg !1200
  %12 = load %struct.stat64** %11, align 4, !dbg !1200
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !1200
  %14 = load i64* %13, align 4, !dbg !1200
  %15 = icmp eq i64 %14, 0, !dbg !1200
  br i1 %15, label %bb1, label %__get_sym_file.exit, !dbg !1200

bb7.i:                                            ; preds = %bb3.i
  %16 = add i32 %17, 1, !dbg !1197
  br label %bb8.i, !dbg !1197

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %17 = phi i32 [ %16, %bb7.i ], [ 0, %bb8.preheader.i ]
  %18 = icmp ugt i32 %5, %17, !dbg !1197
  br i1 %18, label %bb3.i, label %bb1, !dbg !1197

__get_sym_file.exit:                              ; preds = %bb4.i
  %19 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %17, !dbg !1199
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %19}, i64 0, metadata !502), !dbg !1194
  %20 = icmp eq %struct.exe_disk_file_t* %19, null, !dbg !1201
  br i1 %20, label %bb1, label %bb, !dbg !1201

bb:                                               ; preds = %__get_sym_file.exit
  %21 = bitcast %struct.stat64* %buf to i8*, !dbg !1202
  %22 = bitcast %struct.stat64* %12 to i8*, !dbg !1202
  tail call void @llvm.memcpy.p0i8.p0i8.i32(i8* %21, i8* %22, i32 96, i32 4, i1 false), !dbg !1202
  ret i32 0, !dbg !1203

bb1:                                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !1204
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !1206
  %23 = ptrtoint i8* %path to i32, !dbg !1208
  %24 = tail call i32 @klee_get_valuel(i32 %23) nounwind, !dbg !1208
  %25 = inttoptr i32 %24 to i8*, !dbg !1208
  tail call void @llvm.dbg.value(metadata !{i8* %25}, i64 0, metadata !303) nounwind, !dbg !1208
  %26 = icmp eq i8* %25, %path, !dbg !1209
  %27 = zext i1 %26 to i32, !dbg !1209
  tail call void @klee_assume(i32 %27) nounwind, !dbg !1209
  tail call void @llvm.dbg.value(metadata !{i8* %25}, i64 0, metadata !313) nounwind, !dbg !1207
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !1210
  br label %bb.i6, !dbg !1210

bb.i6:                                            ; preds = %bb6.i8, %bb1
  %sc.1.i = phi i8* [ %25, %bb1 ], [ %sc.0.i, %bb6.i8 ]
  %28 = phi i32 [ 0, %bb1 ], [ %40, %bb6.i8 ]
  %tmp.i = add i32 %28, -1
  %29 = load i8* %sc.1.i, align 1, !dbg !1211
  %30 = and i32 %tmp.i, %28, !dbg !1212
  %31 = icmp eq i32 %30, 0, !dbg !1212
  br i1 %31, label %bb1.i, label %bb5.i, !dbg !1212

bb1.i:                                            ; preds = %bb.i6
  switch i8 %29, label %bb6.i8 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i7
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %29}, i64 0, metadata !316) nounwind, !dbg !1211
  store i8 0, i8* %sc.1.i, align 1, !dbg !1213
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !1213
  br label %__concretize_string.exit

bb4.i7:                                           ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !1214
  %32 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1214
  br label %bb6.i8, !dbg !1214

bb5.i:                                            ; preds = %bb.i6
  %33 = sext i8 %29 to i32, !dbg !1215
  %34 = tail call i32 @klee_get_valuel(i32 %33) nounwind, !dbg !1215
  %35 = trunc i32 %34 to i8, !dbg !1215
  %36 = icmp eq i8 %35, %29, !dbg !1216
  %37 = zext i1 %36 to i32, !dbg !1216
  tail call void @klee_assume(i32 %37) nounwind, !dbg !1216
  store i8 %35, i8* %sc.1.i, align 1, !dbg !1217
  %38 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1217
  %39 = icmp eq i8 %35, 0, !dbg !1218
  br i1 %39, label %__concretize_string.exit, label %bb6.i8, !dbg !1218

bb6.i8:                                           ; preds = %bb5.i, %bb4.i7, %bb1.i
  %sc.0.i = phi i8* [ %32, %bb4.i7 ], [ %38, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %40 = add i32 %28, 1, !dbg !1210
  br label %bb.i6, !dbg !1210

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %41 = tail call i32 (i32, ...)* @syscall(i32 195, i8* %path, %struct.stat64* %buf) nounwind, !dbg !1205
  tail call void @llvm.dbg.value(metadata !{i32 %41}, i64 0, metadata !504), !dbg !1205
  %42 = icmp eq i32 %41, -1, !dbg !1219
  br i1 %42, label %bb2, label %bb4, !dbg !1219

bb2:                                              ; preds = %__concretize_string.exit
  %43 = tail call i32* @__errno_location() nounwind readnone, !dbg !1220
  %44 = tail call i32 @klee_get_errno() nounwind, !dbg !1220
  store i32 %44, i32* %43, align 4, !dbg !1220
  ret i32 %41, !dbg !1203

bb4:                                              ; preds = %__concretize_string.exit
  ret i32 %41, !dbg !1203
}

define i32 @read(i32 %fd, i8* %buf, i32 %count) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !506), !dbg !1221
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !507), !dbg !1221
  tail call void @llvm.dbg.value(metadata !{i32 %count}, i64 0, metadata !508), !dbg !1221
  %0 = load i32* @n_calls.4407, align 4, !dbg !1222
  %1 = add nsw i32 %0, 1, !dbg !1222
  store i32 %1, i32* @n_calls.4407, align 4, !dbg !1222
  %2 = icmp eq i32 %count, 0, !dbg !1223
  br i1 %2, label %bb24, label %bb1, !dbg !1223

bb1:                                              ; preds = %entry
  %3 = icmp eq i8* %buf, null, !dbg !1224
  br i1 %3, label %bb2, label %bb3, !dbg !1224

bb2:                                              ; preds = %bb1
  %4 = tail call i32* @__errno_location() nounwind readnone, !dbg !1225
  store i32 14, i32* %4, align 4, !dbg !1225
  ret i32 -1, !dbg !1226

bb3:                                              ; preds = %bb1
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1227
  %5 = icmp ult i32 %fd, 32
  br i1 %5, label %bb.i, label %bb4, !dbg !1229

bb.i:                                             ; preds = %bb3
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1230
  %6 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %7 = load i32* %6, align 4, !dbg !1231
  %8 = and i32 %7, 1, !dbg !1231
  %toBool.i = icmp eq i32 %8, 0
  br i1 %toBool.i, label %bb4, label %__get_file.exit, !dbg !1231

__get_file.exit:                                  ; preds = %bb.i
  %9 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %9}, i64 0, metadata !509), !dbg !1228
  %10 = icmp eq %struct.exe_file_t* %9, null, !dbg !1232
  br i1 %10, label %bb4, label %bb5, !dbg !1232

bb4:                                              ; preds = %bb3, %bb.i, %__get_file.exit
  %11 = tail call i32* @__errno_location() nounwind readnone, !dbg !1233
  store i32 9, i32* %11, align 4, !dbg !1233
  ret i32 -1, !dbg !1226

bb5:                                              ; preds = %__get_file.exit
  %12 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1234
  %13 = icmp eq i32 %12, 0, !dbg !1234
  br i1 %13, label %bb8, label %bb6, !dbg !1234

bb6:                                              ; preds = %bb5
  %14 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 6), align 4, !dbg !1234
  %15 = load i32* %14, align 4, !dbg !1234
  %16 = icmp eq i32 %15, %1, !dbg !1234
  br i1 %16, label %bb7, label %bb8, !dbg !1234

bb7:                                              ; preds = %bb6
  %17 = add i32 %12, -1
  store i32 %17, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1235
  %18 = tail call i32* @__errno_location() nounwind readnone, !dbg !1236
  store i32 5, i32* %18, align 4, !dbg !1236
  ret i32 -1, !dbg !1226

bb8:                                              ; preds = %bb5, %bb6
  %19 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %20 = load %struct.exe_disk_file_t** %19, align 4, !dbg !1237
  %21 = icmp eq %struct.exe_disk_file_t* %20, null, !dbg !1237
  br i1 %21, label %bb9, label %bb17, !dbg !1237

bb9:                                              ; preds = %bb8
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !302) nounwind, !dbg !1238
  %22 = ptrtoint i8* %buf to i32, !dbg !1240
  %23 = tail call i32 @klee_get_valuel(i32 %22) nounwind, !dbg !1240
  %24 = inttoptr i32 %23 to i8*, !dbg !1240
  tail call void @llvm.dbg.value(metadata !{i8* %24}, i64 0, metadata !303) nounwind, !dbg !1240
  %25 = icmp eq i8* %24, %buf, !dbg !1241
  %26 = zext i1 %25 to i32, !dbg !1241
  tail call void @klee_assume(i32 %26) nounwind, !dbg !1241
  tail call void @llvm.dbg.value(metadata !{i8* %24}, i64 0, metadata !507), !dbg !1239
  tail call void @llvm.dbg.value(metadata !{i32 %count}, i64 0, metadata !305) nounwind, !dbg !1242
  %27 = tail call i32 @klee_get_valuel(i32 %count) nounwind, !dbg !1244
  tail call void @llvm.dbg.value(metadata !{i32 %27}, i64 0, metadata !306) nounwind, !dbg !1244
  %28 = icmp eq i32 %27, %count, !dbg !1245
  %29 = zext i1 %28 to i32, !dbg !1245
  tail call void @klee_assume(i32 %29) nounwind, !dbg !1245
  tail call void @llvm.dbg.value(metadata !{i32 %27}, i64 0, metadata !508), !dbg !1243
  tail call void @klee_check_memory_access(i8* %24, i32 %27) nounwind, !dbg !1246
  %30 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %31 = load i32* %30, align 4, !dbg !1247
  %32 = icmp eq i32 %31, 0, !dbg !1247
  br i1 %32, label %bb10, label %bb11, !dbg !1247

bb10:                                             ; preds = %bb9
  %33 = tail call i32 (i32, ...)* @syscall(i32 3, i32 %31, i8* %24, i32 %27) nounwind, !dbg !1248
  tail call void @llvm.dbg.value(metadata !{i32 %33}, i64 0, metadata !511), !dbg !1248
  br label %bb12, !dbg !1248

bb11:                                             ; preds = %bb9
  %34 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %35 = load i64* %34, align 4, !dbg !1249
  %36 = tail call i32 (i32, ...)* @syscall(i32 180, i32 %31, i8* %24, i32 %27, i64 %35) nounwind, !dbg !1249
  tail call void @llvm.dbg.value(metadata !{i32 %36}, i64 0, metadata !511), !dbg !1249
  br label %bb12, !dbg !1249

bb12:                                             ; preds = %bb11, %bb10
  %r.0 = phi i32 [ %33, %bb10 ], [ %36, %bb11 ]
  %37 = icmp eq i32 %r.0, -1, !dbg !1250
  br i1 %37, label %bb13, label %bb14, !dbg !1250

bb13:                                             ; preds = %bb12
  %38 = tail call i32* @__errno_location() nounwind readnone, !dbg !1251
  %39 = tail call i32 @klee_get_errno() nounwind, !dbg !1251
  store i32 %39, i32* %38, align 4, !dbg !1251
  ret i32 -1, !dbg !1226

bb14:                                             ; preds = %bb12
  %40 = load i32* %30, align 4, !dbg !1252
  %41 = icmp eq i32 %40, 0, !dbg !1252
  br i1 %41, label %bb24, label %bb15, !dbg !1252

bb15:                                             ; preds = %bb14
  %42 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %43 = load i64* %42, align 4, !dbg !1253
  %44 = sext i32 %r.0 to i64, !dbg !1253
  %45 = add nsw i64 %43, %44, !dbg !1253
  store i64 %45, i64* %42, align 4, !dbg !1253
  ret i32 %r.0, !dbg !1226

bb17:                                             ; preds = %bb8
  %46 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %47 = load i64* %46, align 4, !dbg !1254
  %48 = icmp slt i64 %47, 0, !dbg !1254
  br i1 %48, label %bb18, label %bb19, !dbg !1254

bb18:                                             ; preds = %bb17
  tail call void @__assert_fail(i8* getelementptr inbounds ([12 x i8]* @.str18, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str16, i32 0, i32 0), i32 284, i8* getelementptr inbounds ([5 x i8]* @__PRETTY_FUNCTION__.4410, i32 0, i32 0)) noreturn nounwind, !dbg !1254
  unreachable, !dbg !1254

bb19:                                             ; preds = %bb17
  %49 = getelementptr inbounds %struct.exe_disk_file_t* %20, i32 0, i32 0, !dbg !1255
  %50 = load i32* %49, align 4, !dbg !1255
  %51 = zext i32 %50 to i64, !dbg !1255
  %52 = icmp slt i64 %51, %47, !dbg !1255
  br i1 %52, label %bb24, label %bb21, !dbg !1255

bb21:                                             ; preds = %bb19
  %53 = zext i32 %count to i64, !dbg !1256
  %54 = add nsw i64 %47, %53, !dbg !1256
  %55 = icmp sgt i64 %54, %51, !dbg !1256
  br i1 %55, label %bb22, label %bb23, !dbg !1256

bb22:                                             ; preds = %bb21
  %56 = trunc i64 %47 to i32, !dbg !1257
  %57 = sub i32 %50, %56, !dbg !1257
  tail call void @llvm.dbg.value(metadata !{i32 %57}, i64 0, metadata !508), !dbg !1257
  br label %bb23, !dbg !1257

bb23:                                             ; preds = %bb21, %bb22
  %count_addr.0 = phi i32 [ %57, %bb22 ], [ %count, %bb21 ]
  %58 = getelementptr inbounds %struct.exe_disk_file_t* %20, i32 0, i32 1, !dbg !1258
  %59 = load i8** %58, align 4, !dbg !1258
  %60 = trunc i64 %47 to i32, !dbg !1258
  %61 = getelementptr inbounds i8* %59, i32 %60, !dbg !1258
  tail call void @llvm.memcpy.p0i8.p0i8.i32(i8* %buf, i8* %61, i32 %count_addr.0, i32 1, i1 false), !dbg !1258
  %62 = load i64* %46, align 4, !dbg !1259
  %63 = zext i32 %count_addr.0 to i64, !dbg !1259
  %64 = add nsw i64 %62, %63, !dbg !1259
  store i64 %64, i64* %46, align 4, !dbg !1259
  ret i32 %count_addr.0, !dbg !1226

bb24:                                             ; preds = %bb19, %bb14, %entry
  %.0 = phi i32 [ 0, %entry ], [ %r.0, %bb14 ], [ 0, %bb19 ]
  ret i32 %.0, !dbg !1226
}

declare i32 @geteuid() nounwind

declare i32 @getgid() nounwind

define i32 @fchmod(i32 %fd, i32 %mode) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !515), !dbg !1260
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !516), !dbg !1260
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1261
  %0 = icmp ult i32 %fd, 32
  br i1 %0, label %bb.i, label %bb, !dbg !1263

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1264
  %1 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %2 = load i32* %1, align 4, !dbg !1265
  %3 = and i32 %2, 1, !dbg !1265
  %toBool.i = icmp eq i32 %3, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1265

__get_file.exit:                                  ; preds = %bb.i
  %4 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %4}, i64 0, metadata !517), !dbg !1262
  %5 = icmp eq %struct.exe_file_t* %4, null, !dbg !1266
  br i1 %5, label %bb, label %bb1, !dbg !1266

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !1267
  store i32 9, i32* %6, align 4, !dbg !1267
  ret i32 -1, !dbg !1268

bb1:                                              ; preds = %__get_file.exit
  %7 = load i32* @n_calls.4662, align 4, !dbg !1269
  %8 = add nsw i32 %7, 1, !dbg !1269
  store i32 %8, i32* @n_calls.4662, align 4, !dbg !1269
  %9 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1270
  %10 = icmp eq i32 %9, 0, !dbg !1270
  br i1 %10, label %bb4, label %bb2, !dbg !1270

bb2:                                              ; preds = %bb1
  %11 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 12), align 4, !dbg !1270
  %12 = load i32* %11, align 4, !dbg !1270
  %13 = icmp eq i32 %12, %8, !dbg !1270
  br i1 %13, label %bb3, label %bb4, !dbg !1270

bb3:                                              ; preds = %bb2
  %14 = add i32 %9, -1
  store i32 %14, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1271
  %15 = tail call i32* @__errno_location() nounwind readnone, !dbg !1272
  store i32 5, i32* %15, align 4, !dbg !1272
  ret i32 -1, !dbg !1268

bb4:                                              ; preds = %bb1, %bb2
  %16 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %17 = load %struct.exe_disk_file_t** %16, align 4, !dbg !1273
  %18 = icmp eq %struct.exe_disk_file_t* %17, null, !dbg !1273
  br i1 %18, label %bb6, label %bb5, !dbg !1273

bb5:                                              ; preds = %bb4
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %17}, i64 0, metadata !513) nounwind, !dbg !1274
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !514) nounwind, !dbg !1274
  %19 = tail call i32 @geteuid() nounwind, !dbg !1276
  %20 = getelementptr inbounds %struct.exe_disk_file_t* %17, i32 0, i32 2, !dbg !1276
  %21 = load %struct.stat64** %20, align 4, !dbg !1276
  %22 = getelementptr inbounds %struct.stat64* %21, i32 0, i32 5, !dbg !1276
  %23 = load i32* %22, align 4, !dbg !1276
  %24 = icmp eq i32 %19, %23, !dbg !1276
  br i1 %24, label %bb.i11, label %bb3.i12, !dbg !1276

bb.i11:                                           ; preds = %bb5
  %25 = tail call i32 @getgid() nounwind, !dbg !1278
  %26 = load %struct.stat64** %20, align 4, !dbg !1278
  %27 = getelementptr inbounds %struct.stat64* %26, i32 0, i32 6, !dbg !1278
  %28 = load i32* %27, align 4, !dbg !1278
  %29 = and i32 %mode, 3071, !dbg !1279
  %30 = icmp eq i32 %25, %28, !dbg !1278
  %mode..i = select i1 %30, i32 %mode, i32 %29
  tail call void @llvm.dbg.value(metadata !{i32 %29}, i64 0, metadata !514) nounwind, !dbg !1279
  %31 = getelementptr inbounds %struct.stat64* %26, i32 0, i32 3, !dbg !1280
  %32 = load i32* %31, align 4, !dbg !1280
  %33 = and i32 %32, -4096, !dbg !1280
  %34 = and i32 %mode..i, 4095, !dbg !1280
  %35 = or i32 %34, %33, !dbg !1280
  store i32 %35, i32* %31, align 4, !dbg !1280
  ret i32 0, !dbg !1268

bb3.i12:                                          ; preds = %bb5
  %36 = tail call i32* @__errno_location() nounwind readnone, !dbg !1281
  store i32 1, i32* %36, align 4, !dbg !1281
  ret i32 -1, !dbg !1268

bb6:                                              ; preds = %bb4
  %37 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %38 = load i32* %37, align 4, !dbg !1282
  %39 = tail call i32 (i32, ...)* @syscall(i32 94, i32 %38, i32 %mode) nounwind, !dbg !1282
  tail call void @llvm.dbg.value(metadata !{i32 %39}, i64 0, metadata !519), !dbg !1282
  %40 = icmp eq i32 %39, -1, !dbg !1283
  br i1 %40, label %bb7, label %bb9, !dbg !1283

bb7:                                              ; preds = %bb6
  %41 = tail call i32* @__errno_location() nounwind readnone, !dbg !1284
  %42 = tail call i32 @klee_get_errno() nounwind, !dbg !1284
  store i32 %42, i32* %41, align 4, !dbg !1284
  ret i32 %39, !dbg !1268

bb9:                                              ; preds = %bb6
  ret i32 %39, !dbg !1268
}

define i32 @chmod(i8* %path, i32 %mode) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !521), !dbg !1285
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !522), !dbg !1285
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !230), !dbg !1286
  %0 = load i8* %path, align 1, !dbg !1288
  tail call void @llvm.dbg.value(metadata !{i8 %0}, i64 0, metadata !231), !dbg !1288
  %1 = icmp eq i8 %0, 0, !dbg !1289
  br i1 %1, label %__get_sym_file.exit, label %bb.i, !dbg !1289

bb.i:                                             ; preds = %entry
  %2 = getelementptr inbounds i8* %path, i32 1, !dbg !1289
  %3 = load i8* %2, align 1, !dbg !1289
  %4 = icmp eq i8 %3, 0, !dbg !1289
  br i1 %4, label %bb8.preheader.i, label %__get_sym_file.exit, !dbg !1289

bb8.preheader.i:                                  ; preds = %bb.i
  %5 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1290
  %6 = sext i8 %0 to i32, !dbg !1291
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %18, 24
  %7 = ashr i32 %sext.i, 24
  %8 = add nsw i32 %7, 65, !dbg !1291
  %9 = icmp eq i32 %6, %8, !dbg !1291
  br i1 %9, label %bb4.i, label %bb7.i, !dbg !1291

bb4.i:                                            ; preds = %bb3.i
  %10 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !1292
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !1292
  %11 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %18, i32 2, !dbg !1293
  %12 = load %struct.stat64** %11, align 4, !dbg !1293
  %13 = getelementptr inbounds %struct.stat64* %12, i32 0, i32 15, !dbg !1293
  %14 = load i64* %13, align 4, !dbg !1293
  %15 = icmp eq i64 %14, 0, !dbg !1293
  br i1 %15, label %__get_sym_file.exit, label %bb6.i, !dbg !1293

bb6.i:                                            ; preds = %bb4.i
  %16 = getelementptr inbounds %struct.exe_disk_file_t* %10, i32 %18, !dbg !1292
  br label %__get_sym_file.exit

bb7.i:                                            ; preds = %bb3.i
  %17 = add i32 %18, 1, !dbg !1290
  br label %bb8.i, !dbg !1290

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %18 = phi i32 [ %17, %bb7.i ], [ 0, %bb8.preheader.i ]
  %19 = icmp ugt i32 %5, %18, !dbg !1290
  br i1 %19, label %bb3.i, label %__get_sym_file.exit, !dbg !1290

__get_sym_file.exit:                              ; preds = %bb8.i, %entry, %bb.i, %bb4.i, %bb6.i
  %20 = phi %struct.exe_disk_file_t* [ %16, %bb6.i ], [ null, %bb4.i ], [ null, %bb.i ], [ null, %entry ], [ null, %bb8.i ]
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %20}, i64 0, metadata !523), !dbg !1287
  %21 = load i32* @n_calls.4639, align 4, !dbg !1294
  %22 = add nsw i32 %21, 1, !dbg !1294
  store i32 %22, i32* @n_calls.4639, align 4, !dbg !1294
  %23 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1295
  %24 = icmp eq i32 %23, 0, !dbg !1295
  br i1 %24, label %bb2, label %bb, !dbg !1295

bb:                                               ; preds = %__get_sym_file.exit
  %25 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 11), align 4, !dbg !1295
  %26 = load i32* %25, align 4, !dbg !1295
  %27 = icmp eq i32 %26, %22, !dbg !1295
  br i1 %27, label %bb1, label %bb2, !dbg !1295

bb1:                                              ; preds = %bb
  %28 = add i32 %23, -1
  store i32 %28, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1296
  %29 = tail call i32* @__errno_location() nounwind readnone, !dbg !1297
  store i32 5, i32* %29, align 4, !dbg !1297
  ret i32 -1, !dbg !1298

bb2:                                              ; preds = %__get_sym_file.exit, %bb
  %30 = icmp eq %struct.exe_disk_file_t* %20, null, !dbg !1299
  br i1 %30, label %bb4, label %bb3, !dbg !1299

bb3:                                              ; preds = %bb2
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %20}, i64 0, metadata !513) nounwind, !dbg !1300
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !514) nounwind, !dbg !1300
  %31 = tail call i32 @geteuid() nounwind, !dbg !1302
  %32 = getelementptr inbounds %struct.exe_disk_file_t* %20, i32 0, i32 2, !dbg !1302
  %33 = load %struct.stat64** %32, align 4, !dbg !1302
  %34 = getelementptr inbounds %struct.stat64* %33, i32 0, i32 5, !dbg !1302
  %35 = load i32* %34, align 4, !dbg !1302
  %36 = icmp eq i32 %31, %35, !dbg !1302
  br i1 %36, label %bb.i13, label %bb3.i14, !dbg !1302

bb.i13:                                           ; preds = %bb3
  %37 = tail call i32 @getgid() nounwind, !dbg !1303
  %38 = load %struct.stat64** %32, align 4, !dbg !1303
  %39 = getelementptr inbounds %struct.stat64* %38, i32 0, i32 6, !dbg !1303
  %40 = load i32* %39, align 4, !dbg !1303
  %41 = and i32 %mode, 3071, !dbg !1304
  %42 = icmp eq i32 %37, %40, !dbg !1303
  %mode..i = select i1 %42, i32 %mode, i32 %41
  tail call void @llvm.dbg.value(metadata !{i32 %41}, i64 0, metadata !514) nounwind, !dbg !1304
  %43 = getelementptr inbounds %struct.stat64* %38, i32 0, i32 3, !dbg !1305
  %44 = load i32* %43, align 4, !dbg !1305
  %45 = and i32 %44, -4096, !dbg !1305
  %46 = and i32 %mode..i, 4095, !dbg !1305
  %47 = or i32 %46, %45, !dbg !1305
  store i32 %47, i32* %43, align 4, !dbg !1305
  ret i32 0, !dbg !1298

bb3.i14:                                          ; preds = %bb3
  %48 = tail call i32* @__errno_location() nounwind readnone, !dbg !1306
  store i32 1, i32* %48, align 4, !dbg !1306
  ret i32 -1, !dbg !1298

bb4:                                              ; preds = %bb2
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !312) nounwind, !dbg !1307
  tail call void @llvm.dbg.value(metadata !{i8* %path}, i64 0, metadata !302) nounwind, !dbg !1309
  %49 = ptrtoint i8* %path to i32, !dbg !1311
  %50 = tail call i32 @klee_get_valuel(i32 %49) nounwind, !dbg !1311
  %51 = inttoptr i32 %50 to i8*, !dbg !1311
  tail call void @llvm.dbg.value(metadata !{i8* %51}, i64 0, metadata !303) nounwind, !dbg !1311
  %52 = icmp eq i8* %51, %path, !dbg !1312
  %53 = zext i1 %52 to i32, !dbg !1312
  tail call void @klee_assume(i32 %53) nounwind, !dbg !1312
  tail call void @llvm.dbg.value(metadata !{i8* %51}, i64 0, metadata !313) nounwind, !dbg !1310
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !1313
  br label %bb.i9, !dbg !1313

bb.i9:                                            ; preds = %bb6.i11, %bb4
  %sc.1.i = phi i8* [ %51, %bb4 ], [ %sc.0.i, %bb6.i11 ]
  %54 = phi i32 [ 0, %bb4 ], [ %66, %bb6.i11 ]
  %tmp.i = add i32 %54, -1
  %55 = load i8* %sc.1.i, align 1, !dbg !1314
  %56 = and i32 %tmp.i, %54, !dbg !1315
  %57 = icmp eq i32 %56, 0, !dbg !1315
  br i1 %57, label %bb1.i, label %bb5.i, !dbg !1315

bb1.i:                                            ; preds = %bb.i9
  switch i8 %55, label %bb6.i11 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i10
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %55}, i64 0, metadata !316) nounwind, !dbg !1314
  store i8 0, i8* %sc.1.i, align 1, !dbg !1316
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !1316
  br label %__concretize_string.exit

bb4.i10:                                          ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !1317
  %58 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1317
  br label %bb6.i11, !dbg !1317

bb5.i:                                            ; preds = %bb.i9
  %59 = sext i8 %55 to i32, !dbg !1318
  %60 = tail call i32 @klee_get_valuel(i32 %59) nounwind, !dbg !1318
  %61 = trunc i32 %60 to i8, !dbg !1318
  %62 = icmp eq i8 %61, %55, !dbg !1319
  %63 = zext i1 %62 to i32, !dbg !1319
  tail call void @klee_assume(i32 %63) nounwind, !dbg !1319
  store i8 %61, i8* %sc.1.i, align 1, !dbg !1320
  %64 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1320
  %65 = icmp eq i8 %61, 0, !dbg !1321
  br i1 %65, label %__concretize_string.exit, label %bb6.i11, !dbg !1321

bb6.i11:                                          ; preds = %bb5.i, %bb4.i10, %bb1.i
  %sc.0.i = phi i8* [ %58, %bb4.i10 ], [ %64, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %66 = add i32 %54, 1, !dbg !1313
  br label %bb.i9, !dbg !1313

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %67 = tail call i32 (i32, ...)* @syscall(i32 15, i8* %path, i32 %mode) nounwind, !dbg !1308
  tail call void @llvm.dbg.value(metadata !{i32 %67}, i64 0, metadata !525), !dbg !1308
  %68 = icmp eq i32 %67, -1, !dbg !1322
  br i1 %68, label %bb5, label %bb7, !dbg !1322

bb5:                                              ; preds = %__concretize_string.exit
  %69 = tail call i32* @__errno_location() nounwind readnone, !dbg !1323
  %70 = tail call i32 @klee_get_errno() nounwind, !dbg !1323
  store i32 %70, i32* %69, align 4, !dbg !1323
  ret i32 %67, !dbg !1298

bb7:                                              ; preds = %__concretize_string.exit
  ret i32 %67, !dbg !1298
}

define i32 @write(i32 %fd, i8* %buf, i32 %count) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !527), !dbg !1324
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !528), !dbg !1324
  tail call void @llvm.dbg.value(metadata !{i32 %count}, i64 0, metadata !529), !dbg !1324
  %0 = load i32* @n_calls.4466, align 4, !dbg !1325
  %1 = add nsw i32 %0, 1, !dbg !1325
  store i32 %1, i32* @n_calls.4466, align 4, !dbg !1325
  tail call void @llvm.dbg.value(metadata !{i32 %fd}, i64 0, metadata !236), !dbg !1326
  %2 = icmp ult i32 %fd, 32
  br i1 %2, label %bb.i, label %bb, !dbg !1328

bb.i:                                             ; preds = %entry
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !237), !dbg !1329
  %3 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 1
  %4 = load i32* %3, align 4, !dbg !1330
  %5 = and i32 %4, 1, !dbg !1330
  %toBool.i = icmp eq i32 %5, 0
  br i1 %toBool.i, label %bb, label %__get_file.exit, !dbg !1330

__get_file.exit:                                  ; preds = %bb.i
  %6 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %6}, i64 0, metadata !530), !dbg !1327
  %7 = icmp eq %struct.exe_file_t* %6, null, !dbg !1331
  br i1 %7, label %bb, label %bb1, !dbg !1331

bb:                                               ; preds = %entry, %bb.i, %__get_file.exit
  %8 = tail call i32* @__errno_location() nounwind readnone, !dbg !1332
  store i32 9, i32* %8, align 4, !dbg !1332
  ret i32 -1, !dbg !1333

bb1:                                              ; preds = %__get_file.exit
  %9 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1334
  %10 = icmp eq i32 %9, 0, !dbg !1334
  br i1 %10, label %bb4, label %bb2, !dbg !1334

bb2:                                              ; preds = %bb1
  %11 = load i32** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 7), align 4, !dbg !1334
  %12 = load i32* %11, align 4, !dbg !1334
  %13 = icmp eq i32 %12, %1, !dbg !1334
  br i1 %13, label %bb3, label %bb4, !dbg !1334

bb3:                                              ; preds = %bb2
  %14 = add i32 %9, -1
  store i32 %14, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 5), align 4, !dbg !1335
  %15 = tail call i32* @__errno_location() nounwind readnone, !dbg !1336
  store i32 5, i32* %15, align 4, !dbg !1336
  ret i32 -1, !dbg !1333

bb4:                                              ; preds = %bb1, %bb2
  %16 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 3
  %17 = load %struct.exe_disk_file_t** %16, align 4, !dbg !1337
  %18 = icmp eq %struct.exe_disk_file_t* %17, null, !dbg !1337
  br i1 %18, label %bb5, label %bb15, !dbg !1337

bb5:                                              ; preds = %bb4
  tail call void @llvm.dbg.value(metadata !{i8* %buf}, i64 0, metadata !302) nounwind, !dbg !1338
  %19 = ptrtoint i8* %buf to i32, !dbg !1340
  %20 = tail call i32 @klee_get_valuel(i32 %19) nounwind, !dbg !1340
  %21 = inttoptr i32 %20 to i8*, !dbg !1340
  tail call void @llvm.dbg.value(metadata !{i8* %21}, i64 0, metadata !303) nounwind, !dbg !1340
  %22 = icmp eq i8* %21, %buf, !dbg !1341
  %23 = zext i1 %22 to i32, !dbg !1341
  tail call void @klee_assume(i32 %23) nounwind, !dbg !1341
  tail call void @llvm.dbg.value(metadata !{i8* %21}, i64 0, metadata !528), !dbg !1339
  tail call void @llvm.dbg.value(metadata !{i32 %count}, i64 0, metadata !305) nounwind, !dbg !1342
  %24 = tail call i32 @klee_get_valuel(i32 %count) nounwind, !dbg !1344
  tail call void @llvm.dbg.value(metadata !{i32 %24}, i64 0, metadata !306) nounwind, !dbg !1344
  %25 = icmp eq i32 %24, %count, !dbg !1345
  %26 = zext i1 %25 to i32, !dbg !1345
  tail call void @klee_assume(i32 %26) nounwind, !dbg !1345
  tail call void @llvm.dbg.value(metadata !{i32 %24}, i64 0, metadata !529), !dbg !1343
  tail call void @klee_check_memory_access(i8* %21, i32 %24) nounwind, !dbg !1346
  %27 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 0
  %28 = load i32* %27, align 4, !dbg !1347
  %29 = add i32 %28, -1
  %30 = icmp ult i32 %29, 2
  br i1 %30, label %bb6, label %bb7, !dbg !1347

bb6:                                              ; preds = %bb5
  %31 = tail call i32 (i32, ...)* @syscall(i32 4, i32 %28, i8* %21, i32 %24) nounwind, !dbg !1348
  tail call void @llvm.dbg.value(metadata !{i32 %31}, i64 0, metadata !532), !dbg !1348
  br label %bb8, !dbg !1348

bb7:                                              ; preds = %bb5
  %32 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %33 = load i64* %32, align 4, !dbg !1349
  %34 = tail call i32 (i32, ...)* @syscall(i32 181, i32 %28, i8* %21, i32 %24, i64 %33) nounwind, !dbg !1349
  tail call void @llvm.dbg.value(metadata !{i32 %34}, i64 0, metadata !532), !dbg !1349
  br label %bb8, !dbg !1349

bb8:                                              ; preds = %bb7, %bb6
  %r.0 = phi i32 [ %31, %bb6 ], [ %34, %bb7 ]
  %35 = icmp eq i32 %r.0, -1, !dbg !1350
  br i1 %35, label %bb9, label %bb10, !dbg !1350

bb9:                                              ; preds = %bb8
  %36 = tail call i32* @__errno_location() nounwind readnone, !dbg !1351
  %37 = tail call i32 @klee_get_errno() nounwind, !dbg !1351
  store i32 %37, i32* %36, align 4, !dbg !1351
  ret i32 -1, !dbg !1333

bb10:                                             ; preds = %bb8
  %38 = icmp slt i32 %r.0, 0, !dbg !1352
  br i1 %38, label %bb11, label %bb12, !dbg !1352

bb11:                                             ; preds = %bb10
  tail call void @__assert_fail(i8* getelementptr inbounds ([7 x i8]* @.str19, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str16, i32 0, i32 0), i32 338, i8* getelementptr inbounds ([6 x i8]* @__PRETTY_FUNCTION__.4469, i32 0, i32 0)) noreturn nounwind, !dbg !1352
  unreachable, !dbg !1352

bb12:                                             ; preds = %bb10
  %39 = load i32* %27, align 4, !dbg !1353
  %40 = add i32 %39, -1
  %41 = icmp ugt i32 %40, 1, !dbg !1353
  br i1 %41, label %bb13, label %bb28, !dbg !1353

bb13:                                             ; preds = %bb12
  %42 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %43 = load i64* %42, align 4, !dbg !1354
  %44 = sext i32 %r.0 to i64, !dbg !1354
  %45 = add nsw i64 %43, %44, !dbg !1354
  store i64 %45, i64* %42, align 4, !dbg !1354
  ret i32 %r.0, !dbg !1333

bb15:                                             ; preds = %bb4
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !534), !dbg !1355
  %46 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %fd, i32 2
  %47 = load i64* %46, align 4, !dbg !1356
  %48 = zext i32 %count to i64, !dbg !1356
  %49 = add nsw i64 %47, %48, !dbg !1356
  %50 = getelementptr inbounds %struct.exe_disk_file_t* %17, i32 0, i32 0, !dbg !1356
  %51 = load i32* %50, align 4, !dbg !1356
  %52 = zext i32 %51 to i64, !dbg !1356
  %53 = icmp sgt i64 %49, %52, !dbg !1356
  br i1 %53, label %bb17, label %bb21, !dbg !1356

bb17:                                             ; preds = %bb15
  %54 = load i32* getelementptr inbounds (%struct.exe_sym_env_t* @__exe_env, i32 0, i32 3), align 4, !dbg !1357
  %55 = icmp eq i32 %54, 0, !dbg !1357
  br i1 %55, label %bb19, label %bb18, !dbg !1357

bb18:                                             ; preds = %bb17
  tail call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str20, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8]* @.str16, i32 0, i32 0), i32 351, i8* getelementptr inbounds ([6 x i8]* @__PRETTY_FUNCTION__.4469, i32 0, i32 0)) noreturn nounwind, !dbg !1358
  unreachable, !dbg !1358

bb19:                                             ; preds = %bb17
  %56 = icmp slt i64 %47, %52, !dbg !1359
  br i1 %56, label %bb20, label %bb23, !dbg !1359

bb20:                                             ; preds = %bb19
  %57 = trunc i64 %47 to i32, !dbg !1360
  %58 = sub i32 %51, %57, !dbg !1360
  tail call void @llvm.dbg.value(metadata !{i32 %58}, i64 0, metadata !534), !dbg !1360
  br label %bb21, !dbg !1360

bb21:                                             ; preds = %bb15, %bb20
  %actual_count.0 = phi i32 [ %58, %bb20 ], [ %count, %bb15 ]
  %59 = icmp eq i32 %actual_count.0, 0, !dbg !1361
  br i1 %59, label %bb23, label %bb22, !dbg !1361

bb22:                                             ; preds = %bb21
  %60 = getelementptr inbounds %struct.exe_disk_file_t* %17, i32 0, i32 1, !dbg !1362
  %61 = load i8** %60, align 4, !dbg !1362
  %62 = trunc i64 %47 to i32, !dbg !1362
  %63 = getelementptr inbounds i8* %61, i32 %62, !dbg !1362
  tail call void @llvm.memcpy.p0i8.p0i8.i32(i8* %63, i8* %buf, i32 %actual_count.0, i32 1, i1 false), !dbg !1362
  br label %bb23, !dbg !1362

bb23:                                             ; preds = %bb19, %bb21, %bb22
  %actual_count.030 = phi i32 [ 0, %bb21 ], [ %actual_count.0, %bb22 ], [ 0, %bb19 ]
  %64 = icmp eq i32 %actual_count.030, %count, !dbg !1363
  br i1 %64, label %bb25, label %bb24, !dbg !1363

bb24:                                             ; preds = %bb23
  %65 = load %struct._IO_FILE** @stderr, align 4, !dbg !1364
  %66 = bitcast %struct._IO_FILE* %65 to i8*, !dbg !1364
  %67 = tail call i32 @fwrite(i8* getelementptr inbounds ([33 x i8]* @.str21, i32 0, i32 0), i32 1, i32 32, i8* %66) nounwind, !dbg !1364
  br label %bb25, !dbg !1364

bb25:                                             ; preds = %bb23, %bb24
  %68 = load %struct.exe_disk_file_t** %16, align 4, !dbg !1365
  %69 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 2), align 4, !dbg !1365
  %70 = icmp eq %struct.exe_disk_file_t* %68, %69, !dbg !1365
  br i1 %70, label %bb26, label %bb27, !dbg !1365

bb26:                                             ; preds = %bb25
  %71 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 3), align 4, !dbg !1366
  %72 = add i32 %71, %actual_count.030, !dbg !1366
  store i32 %72, i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 3), align 4, !dbg !1366
  br label %bb27, !dbg !1366

bb27:                                             ; preds = %bb25, %bb26
  %73 = load i64* %46, align 4, !dbg !1367
  %74 = add nsw i64 %73, %48, !dbg !1367
  store i64 %74, i64* %46, align 4, !dbg !1367
  ret i32 %count, !dbg !1333

bb28:                                             ; preds = %bb12
  ret i32 %r.0, !dbg !1333
}

declare i32 @fwrite(i8* nocapture, i32, i32, i8* nocapture) nounwind

define i32 @__fd_open(i8* %pathname, i32 %flags, i32 %mode) nounwind {
entry:
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !536), !dbg !1368
  tail call void @llvm.dbg.value(metadata !{i32 %flags}, i64 0, metadata !537), !dbg !1368
  tail call void @llvm.dbg.value(metadata !{i32 %mode}, i64 0, metadata !538), !dbg !1368
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !542), !dbg !1369
  br label %bb2, !dbg !1369

bb:                                               ; preds = %bb2
  %scevgep = getelementptr %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %4, i32 1
  %0 = load i32* %scevgep, align 4, !dbg !1370
  %1 = and i32 %0, 1, !dbg !1370
  %2 = icmp eq i32 %1, 0, !dbg !1370
  br i1 %2, label %bb5, label %bb1, !dbg !1370

bb1:                                              ; preds = %bb
  %3 = add nsw i32 %4, 1, !dbg !1369
  br label %bb2, !dbg !1369

bb2:                                              ; preds = %bb1, %entry
  %4 = phi i32 [ 0, %entry ], [ %3, %bb1 ]
  %5 = icmp slt i32 %4, 32
  br i1 %5, label %bb, label %bb4, !dbg !1369

bb4:                                              ; preds = %bb2
  %6 = tail call i32* @__errno_location() nounwind readnone, !dbg !1371
  store i32 24, i32* %6, align 4, !dbg !1371
  ret i32 -1, !dbg !1372

bb5:                                              ; preds = %bb
  %7 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %4
  tail call void @llvm.dbg.value(metadata !{%struct.exe_file_t* %7}, i64 0, metadata !541), !dbg !1373
  %8 = bitcast %struct.exe_file_t* %7 to i8*, !dbg !1374
  tail call void @llvm.memset.p0i8.i32(i8* %8, i8 0, i32 20, i32 4, i1 false), !dbg !1374
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !230), !dbg !1375
  %9 = load i8* %pathname, align 1, !dbg !1377
  tail call void @llvm.dbg.value(metadata !{i8 %9}, i64 0, metadata !231), !dbg !1377
  %10 = icmp eq i8 %9, 0, !dbg !1378
  br i1 %10, label %bb16, label %bb.i, !dbg !1378

bb.i:                                             ; preds = %bb5
  %11 = getelementptr inbounds i8* %pathname, i32 1, !dbg !1378
  %12 = load i8* %11, align 1, !dbg !1378
  %13 = icmp eq i8 %12, 0, !dbg !1378
  br i1 %13, label %bb8.preheader.i, label %bb16, !dbg !1378

bb8.preheader.i:                                  ; preds = %bb.i
  %14 = load i32* getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 0), align 4, !dbg !1379
  %15 = sext i8 %9 to i32, !dbg !1380
  br label %bb8.i

bb3.i:                                            ; preds = %bb8.i
  %sext.i = shl i32 %26, 24
  %16 = ashr i32 %sext.i, 24
  %17 = add nsw i32 %16, 65, !dbg !1380
  %18 = icmp eq i32 %15, %17, !dbg !1380
  br i1 %18, label %bb4.i, label %bb7.i, !dbg !1380

bb4.i:                                            ; preds = %bb3.i
  %19 = load %struct.exe_disk_file_t** getelementptr inbounds (%struct.exe_file_system_t* @__exe_fs, i32 0, i32 4), align 4, !dbg !1381
  tail call void @llvm.dbg.value(metadata !553, i64 0, metadata !234), !dbg !1381
  %20 = getelementptr inbounds %struct.exe_disk_file_t* %19, i32 %26, i32 2, !dbg !1382
  %21 = load %struct.stat64** %20, align 4, !dbg !1382
  %22 = getelementptr inbounds %struct.stat64* %21, i32 0, i32 15, !dbg !1382
  %23 = load i64* %22, align 4, !dbg !1382
  %24 = icmp eq i64 %23, 0, !dbg !1382
  br i1 %24, label %bb16, label %__get_sym_file.exit, !dbg !1382

bb7.i:                                            ; preds = %bb3.i
  %25 = add i32 %26, 1, !dbg !1379
  br label %bb8.i, !dbg !1379

bb8.i:                                            ; preds = %bb7.i, %bb8.preheader.i
  %26 = phi i32 [ %25, %bb7.i ], [ 0, %bb8.preheader.i ]
  %27 = icmp ugt i32 %14, %26, !dbg !1379
  br i1 %27, label %bb3.i, label %bb16, !dbg !1379

__get_sym_file.exit:                              ; preds = %bb4.i
  %28 = getelementptr inbounds %struct.exe_disk_file_t* %19, i32 %26, !dbg !1381
  tail call void @llvm.dbg.value(metadata !{%struct.exe_disk_file_t* %28}, i64 0, metadata !539), !dbg !1376
  %29 = icmp eq %struct.exe_disk_file_t* %28, null, !dbg !1383
  br i1 %29, label %bb16, label %bb6, !dbg !1383

bb6:                                              ; preds = %__get_sym_file.exit
  %30 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %4, i32 3
  store %struct.exe_disk_file_t* %28, %struct.exe_disk_file_t** %30, align 4, !dbg !1384
  %31 = and i32 %flags, 192
  %32 = icmp eq i32 %31, 192
  br i1 %32, label %bb8, label %bb9, !dbg !1385

bb8:                                              ; preds = %bb6
  %33 = tail call i32* @__errno_location() nounwind readnone, !dbg !1386
  store i32 17, i32* %33, align 4, !dbg !1386
  ret i32 -1, !dbg !1372

bb9:                                              ; preds = %bb6
  %34 = and i32 %flags, 128, !dbg !1387
  %.not = icmp ne i32 %34, 0
  %35 = and i32 %flags, 64, !dbg !1387
  %36 = icmp eq i32 %35, 0, !dbg !1387
  %or.cond29 = and i1 %.not, %36
  br i1 %or.cond29, label %bb11, label %bb12, !dbg !1387

bb11:                                             ; preds = %bb9
  %37 = load %struct._IO_FILE** @stderr, align 4, !dbg !1388
  %38 = bitcast %struct._IO_FILE* %37 to i8*, !dbg !1388
  %39 = tail call i32 @fwrite(i8* getelementptr inbounds ([47 x i8]* @.str22, i32 0, i32 0), i32 1, i32 46, i8* %38) nounwind, !dbg !1388
  %40 = tail call i32* @__errno_location() nounwind readnone, !dbg !1389
  store i32 13, i32* %40, align 4, !dbg !1389
  ret i32 -1, !dbg !1372

bb12:                                             ; preds = %bb9
  %41 = load %struct.stat64** %20, align 4, !dbg !1390
  tail call void @llvm.dbg.value(metadata !{i32 %flags}, i64 0, metadata !243), !dbg !1391
  tail call void @llvm.dbg.value(metadata !{%struct.stat64* %41}, i64 0, metadata !244), !dbg !1391
  %42 = getelementptr inbounds %struct.stat64* %41, i32 0, i32 3, !dbg !1392
  %43 = load i32* %42, align 4, !dbg !1392
  tail call void @llvm.dbg.value(metadata !{i32 %43}, i64 0, metadata !248), !dbg !1392
  %44 = and i32 %flags, 2, !dbg !1393
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !247), !dbg !1394
  %45 = and i32 %flags, 3
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !245), !dbg !1395
  %not..i = icmp eq i32 %45, 0
  %46 = icmp eq i32 %44, 0
  br i1 %46, label %bb9.i, label %bb7.i34, !dbg !1396

bb7.i34:                                          ; preds = %bb12
  %47 = and i32 %43, 292, !dbg !1396
  %48 = icmp eq i32 %47, 0, !dbg !1396
  br i1 %48, label %bb9.i, label %bb13, !dbg !1396

bb9.i:                                            ; preds = %bb7.i34, %bb12
  br i1 %not..i, label %bb14, label %has_permission.exit, !dbg !1397

has_permission.exit:                              ; preds = %bb9.i
  %49 = and i32 %43, 146, !dbg !1397
  %phitmp = icmp eq i32 %49, 0
  br i1 %phitmp, label %bb13, label %bb14, !dbg !1390

bb13:                                             ; preds = %bb7.i34, %has_permission.exit
  %50 = tail call i32* @__errno_location() nounwind readnone, !dbg !1398
  store i32 13, i32* %50, align 4, !dbg !1398
  ret i32 -1, !dbg !1372

bb14:                                             ; preds = %bb9.i, %has_permission.exit
  %51 = and i32 %43, -512, !dbg !1399
  %52 = load i32* getelementptr inbounds (%struct.exe_sym_env_t* @__exe_env, i32 0, i32 1), align 4, !dbg !1399
  %not = xor i32 %52, -1, !dbg !1399
  %53 = and i32 %not, %mode, !dbg !1399
  %54 = or i32 %53, %51, !dbg !1399
  store i32 %54, i32* %42, align 4, !dbg !1399
  br label %bb19, !dbg !1399

bb16:                                             ; preds = %bb8.i, %bb5, %bb.i, %bb4.i, %__get_sym_file.exit
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !312) nounwind, !dbg !1400
  tail call void @llvm.dbg.value(metadata !{i8* %pathname}, i64 0, metadata !302) nounwind, !dbg !1402
  %55 = ptrtoint i8* %pathname to i32, !dbg !1404
  %56 = tail call i32 @klee_get_valuel(i32 %55) nounwind, !dbg !1404
  %57 = inttoptr i32 %56 to i8*, !dbg !1404
  tail call void @llvm.dbg.value(metadata !{i8* %57}, i64 0, metadata !303) nounwind, !dbg !1404
  %58 = icmp eq i8* %57, %pathname, !dbg !1405
  %59 = zext i1 %58 to i32, !dbg !1405
  tail call void @klee_assume(i32 %59) nounwind, !dbg !1405
  tail call void @llvm.dbg.value(metadata !{i8* %57}, i64 0, metadata !313) nounwind, !dbg !1403
  tail call void @llvm.dbg.value(metadata !562, i64 0, metadata !315) nounwind, !dbg !1406
  br label %bb.i30, !dbg !1406

bb.i30:                                           ; preds = %bb6.i32, %bb16
  %sc.1.i = phi i8* [ %57, %bb16 ], [ %sc.0.i, %bb6.i32 ]
  %60 = phi i32 [ 0, %bb16 ], [ %72, %bb6.i32 ]
  %tmp.i = add i32 %60, -1
  %61 = load i8* %sc.1.i, align 1, !dbg !1407
  %62 = and i32 %tmp.i, %60, !dbg !1408
  %63 = icmp eq i32 %62, 0, !dbg !1408
  br i1 %63, label %bb1.i, label %bb5.i, !dbg !1408

bb1.i:                                            ; preds = %bb.i30
  switch i8 %61, label %bb6.i32 [
    i8 0, label %bb2.i
    i8 47, label %bb4.i31
  ]

bb2.i:                                            ; preds = %bb1.i
  tail call void @llvm.dbg.value(metadata !{i8 %61}, i64 0, metadata !316) nounwind, !dbg !1407
  store i8 0, i8* %sc.1.i, align 1, !dbg !1409
  tail call void @llvm.dbg.value(metadata !{null}, i64 0, metadata !313) nounwind, !dbg !1409
  br label %__concretize_string.exit

bb4.i31:                                          ; preds = %bb1.i
  store i8 47, i8* %sc.1.i, align 1, !dbg !1410
  %64 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1410
  br label %bb6.i32, !dbg !1410

bb5.i:                                            ; preds = %bb.i30
  %65 = sext i8 %61 to i32, !dbg !1411
  %66 = tail call i32 @klee_get_valuel(i32 %65) nounwind, !dbg !1411
  %67 = trunc i32 %66 to i8, !dbg !1411
  %68 = icmp eq i8 %67, %61, !dbg !1412
  %69 = zext i1 %68 to i32, !dbg !1412
  tail call void @klee_assume(i32 %69) nounwind, !dbg !1412
  store i8 %67, i8* %sc.1.i, align 1, !dbg !1413
  %70 = getelementptr inbounds i8* %sc.1.i, i32 1, !dbg !1413
  %71 = icmp eq i8 %67, 0, !dbg !1414
  br i1 %71, label %__concretize_string.exit, label %bb6.i32, !dbg !1414

bb6.i32:                                          ; preds = %bb5.i, %bb4.i31, %bb1.i
  %sc.0.i = phi i8* [ %64, %bb4.i31 ], [ %70, %bb5.i ], [ %sc.1.i, %bb1.i ]
  %72 = add i32 %60, 1, !dbg !1406
  br label %bb.i30, !dbg !1406

__concretize_string.exit:                         ; preds = %bb5.i, %bb2.i
  %73 = tail call i32 (i32, ...)* @syscall(i32 5, i8* %pathname, i32 %flags, i32 %mode) nounwind, !dbg !1401
  tail call void @llvm.dbg.value(metadata !{i32 %73}, i64 0, metadata !543), !dbg !1401
  %74 = icmp eq i32 %73, -1, !dbg !1415
  br i1 %74, label %bb17, label %bb18, !dbg !1415

bb17:                                             ; preds = %__concretize_string.exit
  %75 = tail call i32* @__errno_location() nounwind readnone, !dbg !1416
  %76 = tail call i32 @klee_get_errno() nounwind, !dbg !1416
  store i32 %76, i32* %75, align 4, !dbg !1416
  ret i32 -1, !dbg !1372

bb18:                                             ; preds = %__concretize_string.exit
  %77 = getelementptr inbounds %struct.exe_sym_env_t* @__exe_env, i32 0, i32 0, i32 %4, i32 0
  store i32 %73, i32* %77, align 4, !dbg !1417
  br label %bb19, !dbg !1417

bb19:                                             ; preds = %bb18, %bb14
  store i32 1, i32* %scevgep, align 4, !dbg !1418
  %78 = and i32 %flags, 3, !dbg !1419
  switch i32 %78, label %bb23 [
    i32 0, label %bb20
    i32 1, label %bb22
  ]

bb20:                                             ; preds = %bb19
  store i32 5, i32* %scevgep, align 4, !dbg !1420
  ret i32 %4, !dbg !1372

bb22:                                             ; preds = %bb19
  store i32 9, i32* %scevgep, align 4, !dbg !1421
  ret i32 %4, !dbg !1372

bb23:                                             ; preds = %bb19
  store i32 13, i32* %scevgep, align 4, !dbg !1422
  ret i32 %4, !dbg !1372
}

!llvm.dbg.sp = !{!0, !59, !73, !77, !80, !83, !84, !85, !90, !95, !98, !129, !132, !135, !136, !140, !143, !146, !149, !152, !155, !156, !157, !160, !180, !181, !184, !185, !186, !189, !206, !209, !210, !213, !214, !217, !220, !223, !226, !227}
!llvm.dbg.lv.__get_sym_file = !{!230, !231, !233, !234}
!llvm.dbg.lv.__get_file = !{!236, !237}
!llvm.dbg.lv.umask = !{!240, !241}
!llvm.dbg.lv.has_permission = !{!243, !244, !245, !247, !248}
!llvm.dbg.lv.chroot = !{!249}
!llvm.dbg.lv.unlink = !{!250, !251}
!llvm.dbg.lv.rmdir = !{!253, !254}
!llvm.dbg.lv.__df_chown = !{!256, !257, !258}
!llvm.dbg.lv.readlink = !{!259, !260, !261, !262, !264}
!llvm.dbg.lv.fsync = !{!266, !267, !269}
!llvm.dbg.lv.fstatfs = !{!271, !272, !273, !275}
!llvm.dbg.lv.__fd_ftruncate = !{!277, !278, !279, !281}
!llvm.dbg.gv = !{!283, !284, !285, !286, !287, !288, !289}
!llvm.dbg.lv.fchown = !{!290, !291, !292, !293, !295}
!llvm.dbg.lv.fchdir = !{!297, !298, !300}
!llvm.dbg.lv.__concretize_ptr = !{!302, !303}
!llvm.dbg.lv.__concretize_size = !{!305, !306}
!llvm.dbg.lv.getcwd = !{!308, !309, !310}
!llvm.dbg.lv.__concretize_string = !{!312, !313, !315, !316, !318}
!llvm.dbg.lv.__fd_statfs = !{!320, !321, !322, !324}
!llvm.dbg.lv.lchown = !{!326, !327, !328, !329, !331}
!llvm.dbg.lv.chown = !{!333, !334, !335, !336, !338}
!llvm.dbg.lv.chdir = !{!340, !341, !343}
!llvm.dbg.lv.access = !{!345, !346, !347, !349}
!llvm.dbg.lv.select = !{!351, !352, !353, !354, !355, !356, !358, !359, !360, !361, !362, !363, !364, !365, !366, !368, !370, !371}
!llvm.dbg.lv.close = !{!373, !374, !376}
!llvm.dbg.lv.dup2 = !{!377, !378, !379, !381}
!llvm.dbg.lv.dup = !{!383, !384, !386}
!llvm.dbg.lv.fcntl = !{!388, !389, !390, !392, !395, !396, !398}
!llvm.dbg.lv.ioctl = !{!400, !401, !402, !404, !405, !406, !430, !448, !458, !461}
!llvm.dbg.lv.__fd_getdents = !{!463, !464, !465, !466, !468, !470, !471, !472, !474, !476, !477, !478, !480}
!llvm.dbg.lv.__fd_lseek = !{!482, !483, !484, !485, !487}
!llvm.dbg.lv.__fd_fstat = !{!488, !489, !490, !492}
!llvm.dbg.lv.__fd_lstat = !{!494, !495, !496, !498}
!llvm.dbg.lv.__fd_stat = !{!500, !501, !502, !504}
!llvm.dbg.lv.read = !{!506, !507, !508, !509, !511}
!llvm.dbg.lv.__df_chmod = !{!513, !514}
!llvm.dbg.lv.fchmod = !{!515, !516, !517, !519}
!llvm.dbg.lv.chmod = !{!521, !522, !523, !525}
!llvm.dbg.lv.write = !{!527, !528, !529, !530, !532, !534}
!llvm.dbg.lv.__fd_open = !{!536, !537, !538, !539, !541, !542, !543}

!0 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__get_sym_file", metadata !"__get_sym_file", metadata !"", metadata !1, i32 39, metadata !3, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 524329, metadata !"fd.c", metadata !"/home/you/Work/klee/runtime/POSIX/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 524305, i32 0, i32 1, metadata !"fd.c", metadata !"/home/you/Work/klee/runtime/POSIX/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.8)", i1 true, i1 true, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5, metadata !57}
!5 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !6} ; [ DW_TAG_pointer_type ]
!6 = metadata !{i32 524310, metadata !7, metadata !"exe_disk_file_t", metadata !7, i32 26, i64 0, i64 0, i64 0, i32 0, metadata !8} ; [ DW_TAG_typedef ]
!7 = metadata !{i32 524329, metadata !"fd.h", metadata !"/home/you/Work/klee/runtime/POSIX/", metadata !2} ; [ DW_TAG_file_type ]
!8 = metadata !{i32 524307, metadata !1, metadata !"", metadata !7, i32 20, i64 96, i64 32, i64 0, i32 0, null, metadata !9, i32 0, null} ; [ DW_TAG_structure_type ]
!9 = metadata !{metadata !10, metadata !12, metadata !15}
!10 = metadata !{i32 524301, metadata !8, metadata !"size", metadata !7, i32 21, i64 32, i64 32, i64 0, i32 0, metadata !11} ; [ DW_TAG_member ]
!11 = metadata !{i32 524324, metadata !1, metadata !"unsigned int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!12 = metadata !{i32 524301, metadata !8, metadata !"contents", metadata !7, i32 22, i64 32, i64 32, i64 32, i32 0, metadata !13} ; [ DW_TAG_member ]
!13 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !14} ; [ DW_TAG_pointer_type ]
!14 = metadata !{i32 524324, metadata !1, metadata !"char", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ]
!15 = metadata !{i32 524301, metadata !8, metadata !"stat", metadata !7, i32 23, i64 32, i64 32, i64 64, i32 0, metadata !16} ; [ DW_TAG_member ]
!16 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !17} ; [ DW_TAG_pointer_type ]
!17 = metadata !{i32 524307, metadata !1, metadata !"stat64", metadata !7, i32 23, i64 768, i64 32, i64 0, i32 0, null, metadata !18, i32 0, null} ; [ DW_TAG_structure_type ]
!18 = metadata !{metadata !19, metadata !24, metadata !25, metadata !28, metadata !30, metadata !32, metadata !34, metadata !36, metadata !37, metadata !38, metadata !41, metadata !44, metadata !46, metadata !53, metadata !54, metadata !55}
!19 = metadata !{i32 524301, metadata !17, metadata !"st_dev", metadata !20, i32 98, i64 64, i64 64, i64 0, i32 0, metadata !21} ; [ DW_TAG_member ]
!20 = metadata !{i32 524329, metadata !"stat.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!21 = metadata !{i32 524310, metadata !22, metadata !"__dev_t", metadata !22, i32 135, i64 0, i64 0, i64 0, i32 0, metadata !23} ; [ DW_TAG_typedef ]
!22 = metadata !{i32 524329, metadata !"types.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!23 = metadata !{i32 524324, metadata !1, metadata !"long long unsigned int", metadata !1, i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!24 = metadata !{i32 524301, metadata !17, metadata !"__pad1", metadata !20, i32 99, i64 32, i64 32, i64 64, i32 0, metadata !11} ; [ DW_TAG_member ]
!25 = metadata !{i32 524301, metadata !17, metadata !"__st_ino", metadata !20, i32 101, i64 32, i64 32, i64 96, i32 0, metadata !26} ; [ DW_TAG_member ]
!26 = metadata !{i32 524310, metadata !22, metadata !"__ino_t", metadata !22, i32 138, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_typedef ]
!27 = metadata !{i32 524324, metadata !1, metadata !"long unsigned int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!28 = metadata !{i32 524301, metadata !17, metadata !"st_mode", metadata !20, i32 102, i64 32, i64 32, i64 128, i32 0, metadata !29} ; [ DW_TAG_member ]
!29 = metadata !{i32 524310, metadata !22, metadata !"__mode_t", metadata !22, i32 140, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!30 = metadata !{i32 524301, metadata !17, metadata !"st_nlink", metadata !20, i32 103, i64 32, i64 32, i64 160, i32 0, metadata !31} ; [ DW_TAG_member ]
!31 = metadata !{i32 524310, metadata !22, metadata !"__nlink_t", metadata !22, i32 141, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!32 = metadata !{i32 524301, metadata !17, metadata !"st_uid", metadata !20, i32 104, i64 32, i64 32, i64 192, i32 0, metadata !33} ; [ DW_TAG_member ]
!33 = metadata !{i32 524310, metadata !22, metadata !"__uid_t", metadata !22, i32 136, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!34 = metadata !{i32 524301, metadata !17, metadata !"st_gid", metadata !20, i32 105, i64 32, i64 32, i64 224, i32 0, metadata !35} ; [ DW_TAG_member ]
!35 = metadata !{i32 524310, metadata !22, metadata !"__gid_t", metadata !22, i32 137, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!36 = metadata !{i32 524301, metadata !17, metadata !"st_rdev", metadata !20, i32 106, i64 64, i64 64, i64 256, i32 0, metadata !21} ; [ DW_TAG_member ]
!37 = metadata !{i32 524301, metadata !17, metadata !"__pad2", metadata !20, i32 107, i64 32, i64 32, i64 320, i32 0, metadata !11} ; [ DW_TAG_member ]
!38 = metadata !{i32 524301, metadata !17, metadata !"st_size", metadata !20, i32 108, i64 64, i64 64, i64 352, i32 0, metadata !39} ; [ DW_TAG_member ]
!39 = metadata !{i32 524310, metadata !22, metadata !"__off64_t", metadata !22, i32 143, i64 0, i64 0, i64 0, i32 0, metadata !40} ; [ DW_TAG_typedef ]
!40 = metadata !{i32 524324, metadata !1, metadata !"long long int", metadata !1, i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!41 = metadata !{i32 524301, metadata !17, metadata !"st_blksize", metadata !20, i32 109, i64 32, i64 32, i64 416, i32 0, metadata !42} ; [ DW_TAG_member ]
!42 = metadata !{i32 524310, metadata !22, metadata !"__blksize_t", metadata !22, i32 169, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!43 = metadata !{i32 524324, metadata !1, metadata !"long int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!44 = metadata !{i32 524301, metadata !17, metadata !"st_blocks", metadata !20, i32 111, i64 64, i64 64, i64 448, i32 0, metadata !45} ; [ DW_TAG_member ]
!45 = metadata !{i32 524310, metadata !22, metadata !"__blkcnt64_t", metadata !22, i32 173, i64 0, i64 0, i64 0, i32 0, metadata !40} ; [ DW_TAG_typedef ]
!46 = metadata !{i32 524301, metadata !17, metadata !"st_atim", metadata !20, i32 119, i64 64, i64 32, i64 512, i32 0, metadata !47} ; [ DW_TAG_member ]
!47 = metadata !{i32 524307, metadata !1, metadata !"timespec", metadata !48, i32 121, i64 64, i64 32, i64 0, i32 0, null, metadata !49, i32 0, null} ; [ DW_TAG_structure_type ]
!48 = metadata !{i32 524329, metadata !"time.h", metadata !"/usr/include", metadata !2} ; [ DW_TAG_file_type ]
!49 = metadata !{metadata !50, metadata !52}
!50 = metadata !{i32 524301, metadata !47, metadata !"tv_sec", metadata !48, i32 122, i64 32, i64 32, i64 0, i32 0, metadata !51} ; [ DW_TAG_member ]
!51 = metadata !{i32 524310, metadata !22, metadata !"__time_t", metadata !22, i32 150, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!52 = metadata !{i32 524301, metadata !47, metadata !"tv_nsec", metadata !48, i32 123, i64 32, i64 32, i64 32, i32 0, metadata !43} ; [ DW_TAG_member ]
!53 = metadata !{i32 524301, metadata !17, metadata !"st_mtim", metadata !20, i32 120, i64 64, i64 32, i64 576, i32 0, metadata !47} ; [ DW_TAG_member ]
!54 = metadata !{i32 524301, metadata !17, metadata !"st_ctim", metadata !20, i32 121, i64 64, i64 32, i64 640, i32 0, metadata !47} ; [ DW_TAG_member ]
!55 = metadata !{i32 524301, metadata !17, metadata !"st_ino", metadata !20, i32 130, i64 64, i64 64, i64 704, i32 0, metadata !56} ; [ DW_TAG_member ]
!56 = metadata !{i32 524310, metadata !22, metadata !"__ino64_t", metadata !22, i32 139, i64 0, i64 0, i64 0, i32 0, metadata !23} ; [ DW_TAG_typedef ]
!57 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !58} ; [ DW_TAG_pointer_type ]
!58 = metadata !{i32 524326, metadata !1, metadata !"", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, metadata !14} ; [ DW_TAG_const_type ]
!59 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__get_file", metadata !"__get_file", metadata !"", metadata !1, i32 63, metadata !60, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!60 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !61, i32 0, null} ; [ DW_TAG_subroutine_type ]
!61 = metadata !{metadata !62, metadata !67}
!62 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !63} ; [ DW_TAG_pointer_type ]
!63 = metadata !{i32 524310, metadata !7, metadata !"exe_file_t", metadata !7, i32 42, i64 0, i64 0, i64 0, i32 0, metadata !64} ; [ DW_TAG_typedef ]
!64 = metadata !{i32 524307, metadata !1, metadata !"", metadata !7, i32 33, i64 160, i64 32, i64 0, i32 0, null, metadata !65, i32 0, null} ; [ DW_TAG_structure_type ]
!65 = metadata !{metadata !66, metadata !68, metadata !69, metadata !72}
!66 = metadata !{i32 524301, metadata !64, metadata !"fd", metadata !7, i32 34, i64 32, i64 32, i64 0, i32 0, metadata !67} ; [ DW_TAG_member ]
!67 = metadata !{i32 524324, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!68 = metadata !{i32 524301, metadata !64, metadata !"flags", metadata !7, i32 35, i64 32, i64 32, i64 32, i32 0, metadata !11} ; [ DW_TAG_member ]
!69 = metadata !{i32 524301, metadata !64, metadata !"off", metadata !7, i32 38, i64 64, i64 64, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ]
!70 = metadata !{i32 524310, metadata !71, metadata !"off64_t", metadata !71, i32 99, i64 0, i64 0, i64 0, i32 0, metadata !40} ; [ DW_TAG_typedef ]
!71 = metadata !{i32 524329, metadata !"types.h", metadata !"/usr/include/i386-linux-gnu/sys", metadata !2} ; [ DW_TAG_file_type ]
!72 = metadata !{i32 524301, metadata !64, metadata !"dfile", metadata !7, i32 39, i64 32, i64 32, i64 128, i32 0, metadata !5} ; [ DW_TAG_member ]
!73 = metadata !{i32 524334, i32 0, metadata !1, metadata !"umask", metadata !"umask", metadata !"umask", metadata !1, i32 88, metadata !74, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32)* @umask} ; [ DW_TAG_subprogram ]
!74 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !75, i32 0, null} ; [ DW_TAG_subroutine_type ]
!75 = metadata !{metadata !76, metadata !76}
!76 = metadata !{i32 524310, metadata !71, metadata !"mode_t", metadata !71, i32 76, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!77 = metadata !{i32 524334, i32 0, metadata !1, metadata !"has_permission", metadata !"has_permission", metadata !"", metadata !1, i32 97, metadata !78, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!78 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !79, i32 0, null} ; [ DW_TAG_subroutine_type ]
!79 = metadata !{metadata !67, metadata !67, metadata !16}
!80 = metadata !{i32 524334, i32 0, metadata !1, metadata !"chroot", metadata !"chroot", metadata !"chroot", metadata !1, i32 1294, metadata !81, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*)* @chroot} ; [ DW_TAG_subprogram ]
!81 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !82, i32 0, null} ; [ DW_TAG_subroutine_type ]
!82 = metadata !{metadata !67, metadata !57}
!83 = metadata !{i32 524334, i32 0, metadata !1, metadata !"unlink", metadata !"unlink", metadata !"unlink", metadata !1, i32 1078, metadata !81, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*)* @unlink} ; [ DW_TAG_subprogram ]
!84 = metadata !{i32 524334, i32 0, metadata !1, metadata !"rmdir", metadata !"rmdir", metadata !"rmdir", metadata !1, i32 1060, metadata !81, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*)* @rmdir} ; [ DW_TAG_subprogram ]
!85 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__df_chown", metadata !"__df_chown", metadata !"", metadata !1, i32 569, metadata !86, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!86 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !87, i32 0, null} ; [ DW_TAG_subroutine_type ]
!87 = metadata !{metadata !67, metadata !5, metadata !88, metadata !89}
!88 = metadata !{i32 524310, metadata !71, metadata !"uid_t", metadata !71, i32 87, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!89 = metadata !{i32 524310, metadata !71, metadata !"gid_t", metadata !71, i32 71, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!90 = metadata !{i32 524334, i32 0, metadata !1, metadata !"readlink", metadata !"readlink", metadata !"readlink", metadata !1, i32 1099, metadata !91, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i8*, i32)* @readlink} ; [ DW_TAG_subprogram ]
!91 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !92, i32 0, null} ; [ DW_TAG_subroutine_type ]
!92 = metadata !{metadata !93, metadata !57, metadata !13, metadata !94}
!93 = metadata !{i32 524310, metadata !71, metadata !"ssize_t", metadata !71, i32 116, i64 0, i64 0, i64 0, i32 0, metadata !67} ; [ DW_TAG_typedef ]
!94 = metadata !{i32 524310, metadata !71, metadata !"size_t", metadata !71, i32 151, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!95 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fsync", metadata !"fsync", metadata !"fsync", metadata !1, i32 1000, metadata !96, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32)* @fsync} ; [ DW_TAG_subprogram ]
!96 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !97, i32 0, null} ; [ DW_TAG_subroutine_type ]
!97 = metadata !{metadata !67, metadata !67}
!98 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fstatfs", metadata !"fstatfs", metadata !"fstatfs", metadata !1, i32 980, metadata !99, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, %struct.statfs*)* @fstatfs} ; [ DW_TAG_subprogram ]
!99 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !100, i32 0, null} ; [ DW_TAG_subroutine_type ]
!100 = metadata !{metadata !67, metadata !67, metadata !101}
!101 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !102} ; [ DW_TAG_pointer_type ]
!102 = metadata !{i32 524307, metadata !1, metadata !"statfs", metadata !103, i32 26, i64 512, i64 32, i64 0, i32 0, null, metadata !104, i32 0, null} ; [ DW_TAG_structure_type ]
!103 = metadata !{i32 524329, metadata !"statfs.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!104 = metadata !{metadata !105, metadata !106, metadata !107, metadata !109, metadata !110, metadata !111, metadata !113, metadata !114, metadata !122, metadata !123, metadata !124, metadata !125}
!105 = metadata !{i32 524301, metadata !102, metadata !"f_type", metadata !103, i32 27, i64 32, i64 32, i64 0, i32 0, metadata !67} ; [ DW_TAG_member ]
!106 = metadata !{i32 524301, metadata !102, metadata !"f_bsize", metadata !103, i32 28, i64 32, i64 32, i64 32, i32 0, metadata !67} ; [ DW_TAG_member ]
!107 = metadata !{i32 524301, metadata !102, metadata !"f_blocks", metadata !103, i32 30, i64 32, i64 32, i64 64, i32 0, metadata !108} ; [ DW_TAG_member ]
!108 = metadata !{i32 524310, metadata !22, metadata !"__fsblkcnt_t", metadata !22, i32 174, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_typedef ]
!109 = metadata !{i32 524301, metadata !102, metadata !"f_bfree", metadata !103, i32 31, i64 32, i64 32, i64 96, i32 0, metadata !108} ; [ DW_TAG_member ]
!110 = metadata !{i32 524301, metadata !102, metadata !"f_bavail", metadata !103, i32 32, i64 32, i64 32, i64 128, i32 0, metadata !108} ; [ DW_TAG_member ]
!111 = metadata !{i32 524301, metadata !102, metadata !"f_files", metadata !103, i32 33, i64 32, i64 32, i64 160, i32 0, metadata !112} ; [ DW_TAG_member ]
!112 = metadata !{i32 524310, metadata !22, metadata !"__fsfilcnt_t", metadata !22, i32 178, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_typedef ]
!113 = metadata !{i32 524301, metadata !102, metadata !"f_ffree", metadata !103, i32 34, i64 32, i64 32, i64 192, i32 0, metadata !112} ; [ DW_TAG_member ]
!114 = metadata !{i32 524301, metadata !102, metadata !"f_fsid", metadata !103, i32 42, i64 64, i64 32, i64 224, i32 0, metadata !115} ; [ DW_TAG_member ]
!115 = metadata !{i32 524310, metadata !22, metadata !"__fsid_t", metadata !22, i32 145, i64 0, i64 0, i64 0, i32 0, metadata !116} ; [ DW_TAG_typedef ]
!116 = metadata !{i32 524307, metadata !1, metadata !"", metadata !22, i32 144, i64 64, i64 32, i64 0, i32 0, null, metadata !117, i32 0, null} ; [ DW_TAG_structure_type ]
!117 = metadata !{metadata !118}
!118 = metadata !{i32 524301, metadata !116, metadata !"__val", metadata !22, i32 144, i64 64, i64 32, i64 0, i32 0, metadata !119} ; [ DW_TAG_member ]
!119 = metadata !{i32 524289, metadata !1, metadata !"", metadata !1, i32 0, i64 64, i64 32, i64 0, i32 0, metadata !67, metadata !120, i32 0, null} ; [ DW_TAG_array_type ]
!120 = metadata !{metadata !121}
!121 = metadata !{i32 524321, i64 0, i64 1}       ; [ DW_TAG_subrange_type ]
!122 = metadata !{i32 524301, metadata !102, metadata !"f_namelen", metadata !103, i32 43, i64 32, i64 32, i64 288, i32 0, metadata !67} ; [ DW_TAG_member ]
!123 = metadata !{i32 524301, metadata !102, metadata !"f_frsize", metadata !103, i32 44, i64 32, i64 32, i64 320, i32 0, metadata !67} ; [ DW_TAG_member ]
!124 = metadata !{i32 524301, metadata !102, metadata !"f_flags", metadata !103, i32 45, i64 32, i64 32, i64 352, i32 0, metadata !67} ; [ DW_TAG_member ]
!125 = metadata !{i32 524301, metadata !102, metadata !"f_spare", metadata !103, i32 46, i64 128, i64 32, i64 384, i32 0, metadata !126} ; [ DW_TAG_member ]
!126 = metadata !{i32 524289, metadata !1, metadata !"", metadata !1, i32 0, i64 128, i64 32, i64 0, i32 0, metadata !67, metadata !127, i32 0, null} ; [ DW_TAG_array_type ]
!127 = metadata !{metadata !128}
!128 = metadata !{i32 524321, i64 0, i64 3}       ; [ DW_TAG_subrange_type ]
!129 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_ftruncate", metadata !"__fd_ftruncate", metadata !"__fd_ftruncate", metadata !1, i32 643, metadata !130, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i64)* @__fd_ftruncate} ; [ DW_TAG_subprogram ]
!130 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !131, i32 0, null} ; [ DW_TAG_subroutine_type ]
!131 = metadata !{metadata !67, metadata !67, metadata !70}
!132 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fchown", metadata !"fchown", metadata !"fchown", metadata !1, i32 588, metadata !133, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i32, i32)* @fchown} ; [ DW_TAG_subprogram ]
!133 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !134, i32 0, null} ; [ DW_TAG_subroutine_type ]
!134 = metadata !{metadata !67, metadata !67, metadata !88, metadata !89}
!135 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fchdir", metadata !"fchdir", metadata !"fchdir", metadata !1, i32 486, metadata !96, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32)* @fchdir} ; [ DW_TAG_subprogram ]
!136 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__concretize_ptr", metadata !"__concretize_ptr", metadata !"", metadata !1, i32 1252, metadata !137, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!137 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !138, i32 0, null} ; [ DW_TAG_subroutine_type ]
!138 = metadata !{metadata !139, metadata !139}
!139 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ]
!140 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__concretize_size", metadata !"__concretize_size", metadata !"", metadata !1, i32 1259, metadata !141, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!141 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !142, i32 0, null} ; [ DW_TAG_subroutine_type ]
!142 = metadata !{metadata !94, metadata !94}
!143 = metadata !{i32 524334, i32 0, metadata !1, metadata !"getcwd", metadata !"getcwd", metadata !"getcwd", metadata !1, i32 1217, metadata !144, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i8* (i8*, i32)* @getcwd} ; [ DW_TAG_subprogram ]
!144 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !145, i32 0, null} ; [ DW_TAG_subroutine_type ]
!145 = metadata !{metadata !13, metadata !13, metadata !94}
!146 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__concretize_string", metadata !"__concretize_string", metadata !"", metadata !1, i32 1265, metadata !147, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!147 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !148, i32 0, null} ; [ DW_TAG_subroutine_type ]
!148 = metadata !{metadata !57, metadata !57}
!149 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_statfs", metadata !"__fd_statfs", metadata !"__fd_statfs", metadata !1, i32 963, metadata !150, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, %struct.statfs*)* @__fd_statfs} ; [ DW_TAG_subprogram ]
!150 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !151, i32 0, null} ; [ DW_TAG_subroutine_type ]
!151 = metadata !{metadata !67, metadata !57, metadata !101}
!152 = metadata !{i32 524334, i32 0, metadata !1, metadata !"lchown", metadata !"lchown", metadata !"lchown", metadata !1, i32 606, metadata !153, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i32, i32)* @lchown} ; [ DW_TAG_subprogram ]
!153 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !154, i32 0, null} ; [ DW_TAG_subroutine_type ]
!154 = metadata !{metadata !67, metadata !57, metadata !88, metadata !89}
!155 = metadata !{i32 524334, i32 0, metadata !1, metadata !"chown", metadata !"chown", metadata !"chown", metadata !1, i32 575, metadata !153, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i32, i32)* @chown} ; [ DW_TAG_subprogram ]
!156 = metadata !{i32 524334, i32 0, metadata !1, metadata !"chdir", metadata !"chdir", metadata !"chdir", metadata !1, i32 468, metadata !81, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*)* @chdir} ; [ DW_TAG_subprogram ]
!157 = metadata !{i32 524334, i32 0, metadata !1, metadata !"access", metadata !"access", metadata !"access", metadata !1, i32 73, metadata !158, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i32)* @access} ; [ DW_TAG_subprogram ]
!158 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !159, i32 0, null} ; [ DW_TAG_subroutine_type ]
!159 = metadata !{metadata !67, metadata !57, metadata !67}
!160 = metadata !{i32 524334, i32 0, metadata !1, metadata !"select", metadata !"select", metadata !"select", metadata !1, i32 1132, metadata !161, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, %struct.fd_set*, %struct.fd_set*, %struct.fd_set*, %struct.timespec*)* @select} ; [ DW_TAG_subprogram ]
!161 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !162, i32 0, null} ; [ DW_TAG_subroutine_type ]
!162 = metadata !{metadata !67, metadata !67, metadata !163, metadata !163, metadata !163, metadata !173}
!163 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !164} ; [ DW_TAG_pointer_type ]
!164 = metadata !{i32 524310, metadata !165, metadata !"fd_set", metadata !165, i32 85, i64 0, i64 0, i64 0, i32 0, metadata !166} ; [ DW_TAG_typedef ]
!165 = metadata !{i32 524329, metadata !"select.h", metadata !"/usr/include/i386-linux-gnu/sys", metadata !2} ; [ DW_TAG_file_type ]
!166 = metadata !{i32 524307, metadata !1, metadata !"", metadata !165, i32 68, i64 1024, i64 32, i64 0, i32 0, null, metadata !167, i32 0, null} ; [ DW_TAG_structure_type ]
!167 = metadata !{metadata !168}
!168 = metadata !{i32 524301, metadata !166, metadata !"fds_bits", metadata !165, i32 72, i64 1024, i64 32, i64 0, i32 0, metadata !169} ; [ DW_TAG_member ]
!169 = metadata !{i32 524289, metadata !1, metadata !"", metadata !1, i32 0, i64 1024, i64 32, i64 0, i32 0, metadata !170, metadata !171, i32 0, null} ; [ DW_TAG_array_type ]
!170 = metadata !{i32 524310, metadata !165, metadata !"__fd_mask", metadata !165, i32 68, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!171 = metadata !{metadata !172}
!172 = metadata !{i32 524321, i64 0, i64 31}      ; [ DW_TAG_subrange_type ]
!173 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !174} ; [ DW_TAG_pointer_type ]
!174 = metadata !{i32 524307, metadata !1, metadata !"timeval", metadata !175, i32 76, i64 64, i64 32, i64 0, i32 0, null, metadata !176, i32 0, null} ; [ DW_TAG_structure_type ]
!175 = metadata !{i32 524329, metadata !"time.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!176 = metadata !{metadata !177, metadata !178}
!177 = metadata !{i32 524301, metadata !174, metadata !"tv_sec", metadata !175, i32 77, i64 32, i64 32, i64 0, i32 0, metadata !51} ; [ DW_TAG_member ]
!178 = metadata !{i32 524301, metadata !174, metadata !"tv_usec", metadata !175, i32 78, i64 32, i64 32, i64 32, i32 0, metadata !179} ; [ DW_TAG_member ]
!179 = metadata !{i32 524310, metadata !22, metadata !"__suseconds_t", metadata !22, i32 153, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!180 = metadata !{i32 524334, i32 0, metadata !1, metadata !"close", metadata !"close", metadata !"close", metadata !1, i32 201, metadata !96, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32)* @close} ; [ DW_TAG_subprogram ]
!181 = metadata !{i32 524334, i32 0, metadata !1, metadata !"dup2", metadata !"dup2", metadata !"dup2", metadata !1, i32 1016, metadata !182, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i32)* @dup2} ; [ DW_TAG_subprogram ]
!182 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !183, i32 0, null} ; [ DW_TAG_subroutine_type ]
!183 = metadata !{metadata !67, metadata !67, metadata !67}
!184 = metadata !{i32 524334, i32 0, metadata !1, metadata !"dup", metadata !"dup", metadata !"dup", metadata !1, i32 1041, metadata !96, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32)* @dup} ; [ DW_TAG_subprogram ]
!185 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fcntl", metadata !"fcntl", metadata !"fcntl", metadata !1, i32 908, metadata !182, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i32, ...)* @fcntl} ; [ DW_TAG_subprogram ]
!186 = metadata !{i32 524334, i32 0, metadata !1, metadata !"ioctl", metadata !"ioctl", metadata !"ioctl", metadata !1, i32 760, metadata !187, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i32, ...)* @ioctl} ; [ DW_TAG_subprogram ]
!187 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !188, i32 0, null} ; [ DW_TAG_subroutine_type ]
!188 = metadata !{metadata !67, metadata !67, metadata !27}
!189 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_getdents", metadata !"__fd_getdents", metadata !"__fd_getdents", metadata !1, i32 676, metadata !190, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, %struct.dirent64*, i32)* @__fd_getdents} ; [ DW_TAG_subprogram ]
!190 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !191, i32 0, null} ; [ DW_TAG_subroutine_type ]
!191 = metadata !{metadata !67, metadata !11, metadata !192, metadata !11}
!192 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !193} ; [ DW_TAG_pointer_type ]
!193 = metadata !{i32 524307, metadata !1, metadata !"dirent64", metadata !194, i32 39, i64 2208, i64 32, i64 0, i32 0, null, metadata !195, i32 0, null} ; [ DW_TAG_structure_type ]
!194 = metadata !{i32 524329, metadata !"dirent.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!195 = metadata !{metadata !196, metadata !197, metadata !198, metadata !200, metadata !202}
!196 = metadata !{i32 524301, metadata !193, metadata !"d_ino", metadata !194, i32 40, i64 64, i64 64, i64 0, i32 0, metadata !56} ; [ DW_TAG_member ]
!197 = metadata !{i32 524301, metadata !193, metadata !"d_off", metadata !194, i32 41, i64 64, i64 64, i64 64, i32 0, metadata !39} ; [ DW_TAG_member ]
!198 = metadata !{i32 524301, metadata !193, metadata !"d_reclen", metadata !194, i32 42, i64 16, i64 16, i64 128, i32 0, metadata !199} ; [ DW_TAG_member ]
!199 = metadata !{i32 524324, metadata !1, metadata !"short unsigned int", metadata !1, i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!200 = metadata !{i32 524301, metadata !193, metadata !"d_type", metadata !194, i32 43, i64 8, i64 8, i64 144, i32 0, metadata !201} ; [ DW_TAG_member ]
!201 = metadata !{i32 524324, metadata !1, metadata !"unsigned char", metadata !1, i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ]
!202 = metadata !{i32 524301, metadata !193, metadata !"d_name", metadata !194, i32 44, i64 2048, i64 8, i64 152, i32 0, metadata !203} ; [ DW_TAG_member ]
!203 = metadata !{i32 524289, metadata !1, metadata !"", metadata !1, i32 0, i64 2048, i64 8, i64 0, i32 0, metadata !14, metadata !204, i32 0, null} ; [ DW_TAG_array_type ]
!204 = metadata !{metadata !205}
!205 = metadata !{i32 524321, i64 0, i64 255}     ; [ DW_TAG_subrange_type ]
!206 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_lseek", metadata !"__fd_lseek", metadata !"__fd_lseek", metadata !1, i32 373, metadata !207, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i64 (i32, i64, i32)* @__fd_lseek} ; [ DW_TAG_subprogram ]
!207 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !208, i32 0, null} ; [ DW_TAG_subroutine_type ]
!208 = metadata !{metadata !70, metadata !67, metadata !70, metadata !67}
!209 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_fstat", metadata !"__fd_fstat", metadata !"__fd_fstat", metadata !1, i32 620, metadata !78, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, %struct.stat64*)* @__fd_fstat} ; [ DW_TAG_subprogram ]
!210 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_lstat", metadata !"__fd_lstat", metadata !"__fd_lstat", metadata !1, i32 449, metadata !211, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, %struct.stat64*)* @__fd_lstat} ; [ DW_TAG_subprogram ]
!211 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !212, i32 0, null} ; [ DW_TAG_subroutine_type ]
!212 = metadata !{metadata !67, metadata !57, metadata !16}
!213 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_stat", metadata !"__fd_stat", metadata !"__fd_stat", metadata !1, i32 430, metadata !211, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, %struct.stat64*)* @__fd_stat} ; [ DW_TAG_subprogram ]
!214 = metadata !{i32 524334, i32 0, metadata !1, metadata !"read", metadata !"read", metadata !"read", metadata !1, i32 233, metadata !215, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i8*, i32)* @read} ; [ DW_TAG_subprogram ]
!215 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !216, i32 0, null} ; [ DW_TAG_subroutine_type ]
!216 = metadata !{metadata !93, metadata !67, metadata !139, metadata !94}
!217 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__df_chmod", metadata !"__df_chmod", metadata !"", metadata !1, i32 507, metadata !218, i1 true, i1 true, i32 0, i32 0, null, i1 false, i1 true, null} ; [ DW_TAG_subprogram ]
!218 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !219, i32 0, null} ; [ DW_TAG_subroutine_type ]
!219 = metadata !{metadata !67, metadata !5, metadata !76}
!220 = metadata !{i32 524334, i32 0, metadata !1, metadata !"fchmod", metadata !"fchmod", metadata !"fchmod", metadata !1, i32 542, metadata !221, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i32)* @fchmod} ; [ DW_TAG_subprogram ]
!221 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !222, i32 0, null} ; [ DW_TAG_subroutine_type ]
!222 = metadata !{metadata !67, metadata !67, metadata !76}
!223 = metadata !{i32 524334, i32 0, metadata !1, metadata !"chmod", metadata !"chmod", metadata !"chmod", metadata !1, i32 520, metadata !224, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i32)* @chmod} ; [ DW_TAG_subprogram ]
!224 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !225, i32 0, null} ; [ DW_TAG_subroutine_type ]
!225 = metadata !{metadata !67, metadata !57, metadata !76}
!226 = metadata !{i32 524334, i32 0, metadata !1, metadata !"write", metadata !"write", metadata !"write", metadata !1, i32 301, metadata !215, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i32, i8*, i32)* @write} ; [ DW_TAG_subprogram ]
!227 = metadata !{i32 524334, i32 0, metadata !1, metadata !"__fd_open", metadata !"__fd_open", metadata !"__fd_open", metadata !1, i32 128, metadata !228, i1 false, i1 true, i32 0, i32 0, null, i1 false, i1 true, i32 (i8*, i32, i32)* @__fd_open} ; [ DW_TAG_subprogram ]
!228 = metadata !{i32 524309, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !229, i32 0, null} ; [ DW_TAG_subroutine_type ]
!229 = metadata !{metadata !67, metadata !57, metadata !67, metadata !76}
!230 = metadata !{i32 524545, metadata !0, metadata !"pathname", metadata !1, i32 39, metadata !57} ; [ DW_TAG_arg_variable ]
!231 = metadata !{i32 524544, metadata !232, metadata !"c", metadata !1, i32 40, metadata !14} ; [ DW_TAG_auto_variable ]
!232 = metadata !{i32 524299, metadata !0, i32 39, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!233 = metadata !{i32 524544, metadata !232, metadata !"i", metadata !1, i32 41, metadata !11} ; [ DW_TAG_auto_variable ]
!234 = metadata !{i32 524544, metadata !235, metadata !"df", metadata !1, i32 48, metadata !5} ; [ DW_TAG_auto_variable ]
!235 = metadata !{i32 524299, metadata !232, i32 48, i32 0, metadata !1, i32 1} ; [ DW_TAG_lexical_block ]
!236 = metadata !{i32 524545, metadata !59, metadata !"fd", metadata !1, i32 63, metadata !67} ; [ DW_TAG_arg_variable ]
!237 = metadata !{i32 524544, metadata !238, metadata !"f", metadata !1, i32 65, metadata !62} ; [ DW_TAG_auto_variable ]
!238 = metadata !{i32 524299, metadata !239, i32 63, i32 0, metadata !1, i32 3} ; [ DW_TAG_lexical_block ]
!239 = metadata !{i32 524299, metadata !59, i32 63, i32 0, metadata !1, i32 2} ; [ DW_TAG_lexical_block ]
!240 = metadata !{i32 524545, metadata !73, metadata !"mask", metadata !1, i32 88, metadata !76} ; [ DW_TAG_arg_variable ]
!241 = metadata !{i32 524544, metadata !242, metadata !"r", metadata !1, i32 89, metadata !76} ; [ DW_TAG_auto_variable ]
!242 = metadata !{i32 524299, metadata !73, i32 88, i32 0, metadata !1, i32 4} ; [ DW_TAG_lexical_block ]
!243 = metadata !{i32 524545, metadata !77, metadata !"flags", metadata !1, i32 97, metadata !67} ; [ DW_TAG_arg_variable ]
!244 = metadata !{i32 524545, metadata !77, metadata !"s", metadata !1, i32 97, metadata !16} ; [ DW_TAG_arg_variable ]
!245 = metadata !{i32 524544, metadata !246, metadata !"write_access", metadata !1, i32 98, metadata !67} ; [ DW_TAG_auto_variable ]
!246 = metadata !{i32 524299, metadata !77, i32 97, i32 0, metadata !1, i32 5} ; [ DW_TAG_lexical_block ]
!247 = metadata !{i32 524544, metadata !246, metadata !"read_access", metadata !1, i32 98, metadata !67} ; [ DW_TAG_auto_variable ]
!248 = metadata !{i32 524544, metadata !246, metadata !"mode", metadata !1, i32 99, metadata !76} ; [ DW_TAG_auto_variable ]
!249 = metadata !{i32 524545, metadata !80, metadata !"path", metadata !1, i32 1294, metadata !57} ; [ DW_TAG_arg_variable ]
!250 = metadata !{i32 524545, metadata !83, metadata !"pathname", metadata !1, i32 1078, metadata !57} ; [ DW_TAG_arg_variable ]
!251 = metadata !{i32 524544, metadata !252, metadata !"dfile", metadata !1, i32 1079, metadata !5} ; [ DW_TAG_auto_variable ]
!252 = metadata !{i32 524299, metadata !83, i32 1078, i32 0, metadata !1, i32 7} ; [ DW_TAG_lexical_block ]
!253 = metadata !{i32 524545, metadata !84, metadata !"pathname", metadata !1, i32 1060, metadata !57} ; [ DW_TAG_arg_variable ]
!254 = metadata !{i32 524544, metadata !255, metadata !"dfile", metadata !1, i32 1061, metadata !5} ; [ DW_TAG_auto_variable ]
!255 = metadata !{i32 524299, metadata !84, i32 1060, i32 0, metadata !1, i32 8} ; [ DW_TAG_lexical_block ]
!256 = metadata !{i32 524545, metadata !85, metadata !"df", metadata !1, i32 569, metadata !5} ; [ DW_TAG_arg_variable ]
!257 = metadata !{i32 524545, metadata !85, metadata !"owner", metadata !1, i32 569, metadata !88} ; [ DW_TAG_arg_variable ]
!258 = metadata !{i32 524545, metadata !85, metadata !"group", metadata !1, i32 569, metadata !89} ; [ DW_TAG_arg_variable ]
!259 = metadata !{i32 524545, metadata !90, metadata !"path", metadata !1, i32 1099, metadata !57} ; [ DW_TAG_arg_variable ]
!260 = metadata !{i32 524545, metadata !90, metadata !"buf", metadata !1, i32 1099, metadata !13} ; [ DW_TAG_arg_variable ]
!261 = metadata !{i32 524545, metadata !90, metadata !"bufsize", metadata !1, i32 1099, metadata !94} ; [ DW_TAG_arg_variable ]
!262 = metadata !{i32 524544, metadata !263, metadata !"dfile", metadata !1, i32 1100, metadata !5} ; [ DW_TAG_auto_variable ]
!263 = metadata !{i32 524299, metadata !90, i32 1099, i32 0, metadata !1, i32 10} ; [ DW_TAG_lexical_block ]
!264 = metadata !{i32 524544, metadata !265, metadata !"r", metadata !1, i32 1116, metadata !67} ; [ DW_TAG_auto_variable ]
!265 = metadata !{i32 524299, metadata !263, i32 1116, i32 0, metadata !1, i32 11} ; [ DW_TAG_lexical_block ]
!266 = metadata !{i32 524545, metadata !95, metadata !"fd", metadata !1, i32 1000, metadata !67} ; [ DW_TAG_arg_variable ]
!267 = metadata !{i32 524544, metadata !268, metadata !"f", metadata !1, i32 1001, metadata !62} ; [ DW_TAG_auto_variable ]
!268 = metadata !{i32 524299, metadata !95, i32 1000, i32 0, metadata !1, i32 12} ; [ DW_TAG_lexical_block ]
!269 = metadata !{i32 524544, metadata !270, metadata !"r", metadata !1, i32 1009, metadata !67} ; [ DW_TAG_auto_variable ]
!270 = metadata !{i32 524299, metadata !268, i32 1009, i32 0, metadata !1, i32 13} ; [ DW_TAG_lexical_block ]
!271 = metadata !{i32 524545, metadata !98, metadata !"fd", metadata !1, i32 980, metadata !67} ; [ DW_TAG_arg_variable ]
!272 = metadata !{i32 524545, metadata !98, metadata !"buf", metadata !1, i32 980, metadata !101} ; [ DW_TAG_arg_variable ]
!273 = metadata !{i32 524544, metadata !274, metadata !"f", metadata !1, i32 981, metadata !62} ; [ DW_TAG_auto_variable ]
!274 = metadata !{i32 524299, metadata !98, i32 980, i32 0, metadata !1, i32 14} ; [ DW_TAG_lexical_block ]
!275 = metadata !{i32 524544, metadata !276, metadata !"r", metadata !1, i32 993, metadata !67} ; [ DW_TAG_auto_variable ]
!276 = metadata !{i32 524299, metadata !274, i32 993, i32 0, metadata !1, i32 15} ; [ DW_TAG_lexical_block ]
!277 = metadata !{i32 524545, metadata !129, metadata !"fd", metadata !1, i32 643, metadata !67} ; [ DW_TAG_arg_variable ]
!278 = metadata !{i32 524545, metadata !129, metadata !"length", metadata !1, i32 643, metadata !70} ; [ DW_TAG_arg_variable ]
!279 = metadata !{i32 524544, metadata !280, metadata !"f", metadata !1, i32 645, metadata !62} ; [ DW_TAG_auto_variable ]
!280 = metadata !{i32 524299, metadata !129, i32 643, i32 0, metadata !1, i32 16} ; [ DW_TAG_lexical_block ]
!281 = metadata !{i32 524544, metadata !282, metadata !"r", metadata !1, i32 668, metadata !67} ; [ DW_TAG_auto_variable ]
!282 = metadata !{i32 524299, metadata !280, i32 668, i32 0, metadata !1, i32 17} ; [ DW_TAG_lexical_block ]
!283 = metadata !{i32 524340, i32 0, metadata !129, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 644, metadata !67, i1 true, i1 true, i32* @n_calls.4755} ; [ DW_TAG_variable ]
!284 = metadata !{i32 524340, i32 0, metadata !143, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 1218, metadata !67, i1 true, i1 true, i32* @n_calls.5273} ; [ DW_TAG_variable ]
!285 = metadata !{i32 524340, i32 0, metadata !180, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 202, metadata !67, i1 true, i1 true, i32* @n_calls.4387} ; [ DW_TAG_variable ]
!286 = metadata !{i32 524340, i32 0, metadata !214, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 234, metadata !67, i1 true, i1 true, i32* @n_calls.4407} ; [ DW_TAG_variable ]
!287 = metadata !{i32 524340, i32 0, metadata !220, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 543, metadata !67, i1 true, i1 true, i32* @n_calls.4662} ; [ DW_TAG_variable ]
!288 = metadata !{i32 524340, i32 0, metadata !223, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 521, metadata !67, i1 true, i1 true, i32* @n_calls.4639} ; [ DW_TAG_variable ]
!289 = metadata !{i32 524340, i32 0, metadata !226, metadata !"n_calls", metadata !"n_calls", metadata !"", metadata !1, i32 302, metadata !67, i1 true, i1 true, i32* @n_calls.4466} ; [ DW_TAG_variable ]
!290 = metadata !{i32 524545, metadata !132, metadata !"fd", metadata !1, i32 588, metadata !67} ; [ DW_TAG_arg_variable ]
!291 = metadata !{i32 524545, metadata !132, metadata !"owner", metadata !1, i32 588, metadata !88} ; [ DW_TAG_arg_variable ]
!292 = metadata !{i32 524545, metadata !132, metadata !"group", metadata !1, i32 588, metadata !89} ; [ DW_TAG_arg_variable ]
!293 = metadata !{i32 524544, metadata !294, metadata !"f", metadata !1, i32 589, metadata !62} ; [ DW_TAG_auto_variable ]
!294 = metadata !{i32 524299, metadata !132, i32 588, i32 0, metadata !1, i32 18} ; [ DW_TAG_lexical_block ]
!295 = metadata !{i32 524544, metadata !296, metadata !"r", metadata !1, i32 599, metadata !67} ; [ DW_TAG_auto_variable ]
!296 = metadata !{i32 524299, metadata !294, i32 599, i32 0, metadata !1, i32 19} ; [ DW_TAG_lexical_block ]
!297 = metadata !{i32 524545, metadata !135, metadata !"fd", metadata !1, i32 486, metadata !67} ; [ DW_TAG_arg_variable ]
!298 = metadata !{i32 524544, metadata !299, metadata !"f", metadata !1, i32 487, metadata !62} ; [ DW_TAG_auto_variable ]
!299 = metadata !{i32 524299, metadata !135, i32 486, i32 0, metadata !1, i32 20} ; [ DW_TAG_lexical_block ]
!300 = metadata !{i32 524544, metadata !301, metadata !"r", metadata !1, i32 499, metadata !67} ; [ DW_TAG_auto_variable ]
!301 = metadata !{i32 524299, metadata !299, i32 499, i32 0, metadata !1, i32 21} ; [ DW_TAG_lexical_block ]
!302 = metadata !{i32 524545, metadata !136, metadata !"p", metadata !1, i32 1252, metadata !139} ; [ DW_TAG_arg_variable ]
!303 = metadata !{i32 524544, metadata !304, metadata !"pc", metadata !1, i32 1254, metadata !13} ; [ DW_TAG_auto_variable ]
!304 = metadata !{i32 524299, metadata !136, i32 1252, i32 0, metadata !1, i32 22} ; [ DW_TAG_lexical_block ]
!305 = metadata !{i32 524545, metadata !140, metadata !"s", metadata !1, i32 1259, metadata !94} ; [ DW_TAG_arg_variable ]
!306 = metadata !{i32 524544, metadata !307, metadata !"sc", metadata !1, i32 1260, metadata !94} ; [ DW_TAG_auto_variable ]
!307 = metadata !{i32 524299, metadata !140, i32 1259, i32 0, metadata !1, i32 23} ; [ DW_TAG_lexical_block ]
!308 = metadata !{i32 524545, metadata !143, metadata !"buf", metadata !1, i32 1217, metadata !13} ; [ DW_TAG_arg_variable ]
!309 = metadata !{i32 524545, metadata !143, metadata !"size", metadata !1, i32 1217, metadata !94} ; [ DW_TAG_arg_variable ]
!310 = metadata !{i32 524544, metadata !311, metadata !"r", metadata !1, i32 1219, metadata !67} ; [ DW_TAG_auto_variable ]
!311 = metadata !{i32 524299, metadata !143, i32 1217, i32 0, metadata !1, i32 24} ; [ DW_TAG_lexical_block ]
!312 = metadata !{i32 524545, metadata !146, metadata !"s", metadata !1, i32 1265, metadata !57} ; [ DW_TAG_arg_variable ]
!313 = metadata !{i32 524544, metadata !314, metadata !"sc", metadata !1, i32 1266, metadata !13} ; [ DW_TAG_auto_variable ]
!314 = metadata !{i32 524299, metadata !146, i32 1265, i32 0, metadata !1, i32 25} ; [ DW_TAG_lexical_block ]
!315 = metadata !{i32 524544, metadata !314, metadata !"i", metadata !1, i32 1267, metadata !11} ; [ DW_TAG_auto_variable ]
!316 = metadata !{i32 524544, metadata !317, metadata !"c", metadata !1, i32 1270, metadata !14} ; [ DW_TAG_auto_variable ]
!317 = metadata !{i32 524299, metadata !314, i32 1270, i32 0, metadata !1, i32 26} ; [ DW_TAG_lexical_block ]
!318 = metadata !{i32 524544, metadata !319, metadata !"cc", metadata !1, i32 1279, metadata !14} ; [ DW_TAG_auto_variable ]
!319 = metadata !{i32 524299, metadata !317, i32 1279, i32 0, metadata !1, i32 27} ; [ DW_TAG_lexical_block ]
!320 = metadata !{i32 524545, metadata !149, metadata !"path", metadata !1, i32 963, metadata !57} ; [ DW_TAG_arg_variable ]
!321 = metadata !{i32 524545, metadata !149, metadata !"buf", metadata !1, i32 963, metadata !101} ; [ DW_TAG_arg_variable ]
!322 = metadata !{i32 524544, metadata !323, metadata !"dfile", metadata !1, i32 964, metadata !5} ; [ DW_TAG_auto_variable ]
!323 = metadata !{i32 524299, metadata !149, i32 963, i32 0, metadata !1, i32 28} ; [ DW_TAG_lexical_block ]
!324 = metadata !{i32 524544, metadata !325, metadata !"r", metadata !1, i32 973, metadata !67} ; [ DW_TAG_auto_variable ]
!325 = metadata !{i32 524299, metadata !323, i32 973, i32 0, metadata !1, i32 29} ; [ DW_TAG_lexical_block ]
!326 = metadata !{i32 524545, metadata !152, metadata !"path", metadata !1, i32 606, metadata !57} ; [ DW_TAG_arg_variable ]
!327 = metadata !{i32 524545, metadata !152, metadata !"owner", metadata !1, i32 606, metadata !88} ; [ DW_TAG_arg_variable ]
!328 = metadata !{i32 524545, metadata !152, metadata !"group", metadata !1, i32 606, metadata !89} ; [ DW_TAG_arg_variable ]
!329 = metadata !{i32 524544, metadata !330, metadata !"df", metadata !1, i32 608, metadata !5} ; [ DW_TAG_auto_variable ]
!330 = metadata !{i32 524299, metadata !152, i32 606, i32 0, metadata !1, i32 30} ; [ DW_TAG_lexical_block ]
!331 = metadata !{i32 524544, metadata !332, metadata !"r", metadata !1, i32 613, metadata !67} ; [ DW_TAG_auto_variable ]
!332 = metadata !{i32 524299, metadata !330, i32 613, i32 0, metadata !1, i32 31} ; [ DW_TAG_lexical_block ]
!333 = metadata !{i32 524545, metadata !155, metadata !"path", metadata !1, i32 575, metadata !57} ; [ DW_TAG_arg_variable ]
!334 = metadata !{i32 524545, metadata !155, metadata !"owner", metadata !1, i32 575, metadata !88} ; [ DW_TAG_arg_variable ]
!335 = metadata !{i32 524545, metadata !155, metadata !"group", metadata !1, i32 575, metadata !89} ; [ DW_TAG_arg_variable ]
!336 = metadata !{i32 524544, metadata !337, metadata !"df", metadata !1, i32 576, metadata !5} ; [ DW_TAG_auto_variable ]
!337 = metadata !{i32 524299, metadata !155, i32 575, i32 0, metadata !1, i32 32} ; [ DW_TAG_lexical_block ]
!338 = metadata !{i32 524544, metadata !339, metadata !"r", metadata !1, i32 581, metadata !67} ; [ DW_TAG_auto_variable ]
!339 = metadata !{i32 524299, metadata !337, i32 581, i32 0, metadata !1, i32 33} ; [ DW_TAG_lexical_block ]
!340 = metadata !{i32 524545, metadata !156, metadata !"path", metadata !1, i32 468, metadata !57} ; [ DW_TAG_arg_variable ]
!341 = metadata !{i32 524544, metadata !342, metadata !"dfile", metadata !1, i32 469, metadata !5} ; [ DW_TAG_auto_variable ]
!342 = metadata !{i32 524299, metadata !156, i32 468, i32 0, metadata !1, i32 34} ; [ DW_TAG_lexical_block ]
!343 = metadata !{i32 524544, metadata !344, metadata !"r", metadata !1, i32 479, metadata !67} ; [ DW_TAG_auto_variable ]
!344 = metadata !{i32 524299, metadata !342, i32 479, i32 0, metadata !1, i32 35} ; [ DW_TAG_lexical_block ]
!345 = metadata !{i32 524545, metadata !157, metadata !"pathname", metadata !1, i32 73, metadata !57} ; [ DW_TAG_arg_variable ]
!346 = metadata !{i32 524545, metadata !157, metadata !"mode", metadata !1, i32 73, metadata !67} ; [ DW_TAG_arg_variable ]
!347 = metadata !{i32 524544, metadata !348, metadata !"dfile", metadata !1, i32 74, metadata !5} ; [ DW_TAG_auto_variable ]
!348 = metadata !{i32 524299, metadata !157, i32 73, i32 0, metadata !1, i32 36} ; [ DW_TAG_lexical_block ]
!349 = metadata !{i32 524544, metadata !350, metadata !"r", metadata !1, i32 81, metadata !67} ; [ DW_TAG_auto_variable ]
!350 = metadata !{i32 524299, metadata !348, i32 81, i32 0, metadata !1, i32 37} ; [ DW_TAG_lexical_block ]
!351 = metadata !{i32 524545, metadata !160, metadata !"nfds", metadata !1, i32 1131, metadata !67} ; [ DW_TAG_arg_variable ]
!352 = metadata !{i32 524545, metadata !160, metadata !"read", metadata !1, i32 1131, metadata !163} ; [ DW_TAG_arg_variable ]
!353 = metadata !{i32 524545, metadata !160, metadata !"write", metadata !1, i32 1131, metadata !163} ; [ DW_TAG_arg_variable ]
!354 = metadata !{i32 524545, metadata !160, metadata !"except", metadata !1, i32 1132, metadata !163} ; [ DW_TAG_arg_variable ]
!355 = metadata !{i32 524545, metadata !160, metadata !"timeout", metadata !1, i32 1132, metadata !173} ; [ DW_TAG_arg_variable ]
!356 = metadata !{i32 524544, metadata !357, metadata !"in_read", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!357 = metadata !{i32 524299, metadata !160, i32 1132, i32 0, metadata !1, i32 38} ; [ DW_TAG_lexical_block ]
!358 = metadata !{i32 524544, metadata !357, metadata !"in_write", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!359 = metadata !{i32 524544, metadata !357, metadata !"in_except", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!360 = metadata !{i32 524544, metadata !357, metadata !"os_read", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!361 = metadata !{i32 524544, metadata !357, metadata !"os_write", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!362 = metadata !{i32 524544, metadata !357, metadata !"os_except", metadata !1, i32 1133, metadata !164} ; [ DW_TAG_auto_variable ]
!363 = metadata !{i32 524544, metadata !357, metadata !"i", metadata !1, i32 1134, metadata !67} ; [ DW_TAG_auto_variable ]
!364 = metadata !{i32 524544, metadata !357, metadata !"count", metadata !1, i32 1134, metadata !67} ; [ DW_TAG_auto_variable ]
!365 = metadata !{i32 524544, metadata !357, metadata !"os_nfds", metadata !1, i32 1134, metadata !67} ; [ DW_TAG_auto_variable ]
!366 = metadata !{i32 524544, metadata !367, metadata !"f", metadata !1, i32 1164, metadata !62} ; [ DW_TAG_auto_variable ]
!367 = metadata !{i32 524299, metadata !357, i32 1164, i32 0, metadata !1, i32 39} ; [ DW_TAG_lexical_block ]
!368 = metadata !{i32 524544, metadata !369, metadata !"tv", metadata !1, i32 1186, metadata !174} ; [ DW_TAG_auto_variable ]
!369 = metadata !{i32 524299, metadata !357, i32 1186, i32 0, metadata !1, i32 40} ; [ DW_TAG_lexical_block ]
!370 = metadata !{i32 524544, metadata !369, metadata !"r", metadata !1, i32 1187, metadata !67} ; [ DW_TAG_auto_variable ]
!371 = metadata !{i32 524544, metadata !372, metadata !"f", metadata !1, i32 1202, metadata !62} ; [ DW_TAG_auto_variable ]
!372 = metadata !{i32 524299, metadata !369, i32 1202, i32 0, metadata !1, i32 41} ; [ DW_TAG_lexical_block ]
!373 = metadata !{i32 524545, metadata !180, metadata !"fd", metadata !1, i32 201, metadata !67} ; [ DW_TAG_arg_variable ]
!374 = metadata !{i32 524544, metadata !375, metadata !"f", metadata !1, i32 203, metadata !62} ; [ DW_TAG_auto_variable ]
!375 = metadata !{i32 524299, metadata !180, i32 201, i32 0, metadata !1, i32 42} ; [ DW_TAG_lexical_block ]
!376 = metadata !{i32 524544, metadata !375, metadata !"r", metadata !1, i32 204, metadata !67} ; [ DW_TAG_auto_variable ]
!377 = metadata !{i32 524545, metadata !181, metadata !"oldfd", metadata !1, i32 1016, metadata !67} ; [ DW_TAG_arg_variable ]
!378 = metadata !{i32 524545, metadata !181, metadata !"newfd", metadata !1, i32 1016, metadata !67} ; [ DW_TAG_arg_variable ]
!379 = metadata !{i32 524544, metadata !380, metadata !"f", metadata !1, i32 1017, metadata !62} ; [ DW_TAG_auto_variable ]
!380 = metadata !{i32 524299, metadata !181, i32 1016, i32 0, metadata !1, i32 43} ; [ DW_TAG_lexical_block ]
!381 = metadata !{i32 524544, metadata !382, metadata !"f2", metadata !1, i32 1023, metadata !62} ; [ DW_TAG_auto_variable ]
!382 = metadata !{i32 524299, metadata !380, i32 1023, i32 0, metadata !1, i32 44} ; [ DW_TAG_lexical_block ]
!383 = metadata !{i32 524545, metadata !184, metadata !"oldfd", metadata !1, i32 1041, metadata !67} ; [ DW_TAG_arg_variable ]
!384 = metadata !{i32 524544, metadata !385, metadata !"f", metadata !1, i32 1042, metadata !62} ; [ DW_TAG_auto_variable ]
!385 = metadata !{i32 524299, metadata !184, i32 1041, i32 0, metadata !1, i32 45} ; [ DW_TAG_lexical_block ]
!386 = metadata !{i32 524544, metadata !387, metadata !"fd", metadata !1, i32 1047, metadata !67} ; [ DW_TAG_auto_variable ]
!387 = metadata !{i32 524299, metadata !385, i32 1048, i32 0, metadata !1, i32 46} ; [ DW_TAG_lexical_block ]
!388 = metadata !{i32 524545, metadata !185, metadata !"fd", metadata !1, i32 908, metadata !67} ; [ DW_TAG_arg_variable ]
!389 = metadata !{i32 524545, metadata !185, metadata !"cmd", metadata !1, i32 908, metadata !67} ; [ DW_TAG_arg_variable ]
!390 = metadata !{i32 524544, metadata !391, metadata !"f", metadata !1, i32 909, metadata !62} ; [ DW_TAG_auto_variable ]
!391 = metadata !{i32 524299, metadata !185, i32 908, i32 0, metadata !1, i32 47} ; [ DW_TAG_lexical_block ]
!392 = metadata !{i32 524544, metadata !391, metadata !"ap", metadata !1, i32 910, metadata !393} ; [ DW_TAG_auto_variable ]
!393 = metadata !{i32 524310, metadata !394, metadata !"va_list", metadata !394, i32 111, i64 0, i64 0, i64 0, i32 0, metadata !13} ; [ DW_TAG_typedef ]
!394 = metadata !{i32 524329, metadata !"stdio.h", metadata !"/usr/include", metadata !2} ; [ DW_TAG_file_type ]
!395 = metadata !{i32 524544, metadata !391, metadata !"arg", metadata !1, i32 911, metadata !11} ; [ DW_TAG_auto_variable ]
!396 = metadata !{i32 524544, metadata !397, metadata !"flags", metadata !1, i32 930, metadata !67} ; [ DW_TAG_auto_variable ]
!397 = metadata !{i32 524299, metadata !391, i32 930, i32 0, metadata !1, i32 48} ; [ DW_TAG_lexical_block ]
!398 = metadata !{i32 524544, metadata !399, metadata !"r", metadata !1, i32 956, metadata !67} ; [ DW_TAG_auto_variable ]
!399 = metadata !{i32 524299, metadata !391, i32 956, i32 0, metadata !1, i32 49} ; [ DW_TAG_lexical_block ]
!400 = metadata !{i32 524545, metadata !186, metadata !"fd", metadata !1, i32 760, metadata !67} ; [ DW_TAG_arg_variable ]
!401 = metadata !{i32 524545, metadata !186, metadata !"request", metadata !1, i32 760, metadata !27} ; [ DW_TAG_arg_variable ]
!402 = metadata !{i32 524544, metadata !403, metadata !"f", metadata !1, i32 762, metadata !62} ; [ DW_TAG_auto_variable ]
!403 = metadata !{i32 524299, metadata !186, i32 760, i32 0, metadata !1, i32 50} ; [ DW_TAG_lexical_block ]
!404 = metadata !{i32 524544, metadata !403, metadata !"ap", metadata !1, i32 763, metadata !393} ; [ DW_TAG_auto_variable ]
!405 = metadata !{i32 524544, metadata !403, metadata !"buf", metadata !1, i32 764, metadata !139} ; [ DW_TAG_auto_variable ]
!406 = metadata !{i32 524544, metadata !407, metadata !"stat", metadata !1, i32 780, metadata !408} ; [ DW_TAG_auto_variable ]
!407 = metadata !{i32 524299, metadata !403, i32 780, i32 0, metadata !1, i32 51} ; [ DW_TAG_lexical_block ]
!408 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !409} ; [ DW_TAG_pointer_type ]
!409 = metadata !{i32 524307, metadata !1, metadata !"stat", metadata !20, i32 40, i64 704, i64 32, i64 0, i32 0, null, metadata !410, i32 0, null} ; [ DW_TAG_structure_type ]
!410 = metadata !{metadata !411, metadata !412, metadata !413, metadata !414, metadata !415, metadata !416, metadata !417, metadata !418, metadata !419, metadata !420, metadata !422, metadata !423, metadata !425, metadata !426, metadata !427, metadata !428, metadata !429}
!411 = metadata !{i32 524301, metadata !409, metadata !"st_dev", metadata !20, i32 41, i64 64, i64 64, i64 0, i32 0, metadata !21} ; [ DW_TAG_member ]
!412 = metadata !{i32 524301, metadata !409, metadata !"__pad1", metadata !20, i32 42, i64 16, i64 16, i64 64, i32 0, metadata !199} ; [ DW_TAG_member ]
!413 = metadata !{i32 524301, metadata !409, metadata !"st_ino", metadata !20, i32 44, i64 32, i64 32, i64 96, i32 0, metadata !26} ; [ DW_TAG_member ]
!414 = metadata !{i32 524301, metadata !409, metadata !"st_mode", metadata !20, i32 48, i64 32, i64 32, i64 128, i32 0, metadata !29} ; [ DW_TAG_member ]
!415 = metadata !{i32 524301, metadata !409, metadata !"st_nlink", metadata !20, i32 49, i64 32, i64 32, i64 160, i32 0, metadata !31} ; [ DW_TAG_member ]
!416 = metadata !{i32 524301, metadata !409, metadata !"st_uid", metadata !20, i32 50, i64 32, i64 32, i64 192, i32 0, metadata !33} ; [ DW_TAG_member ]
!417 = metadata !{i32 524301, metadata !409, metadata !"st_gid", metadata !20, i32 51, i64 32, i64 32, i64 224, i32 0, metadata !35} ; [ DW_TAG_member ]
!418 = metadata !{i32 524301, metadata !409, metadata !"st_rdev", metadata !20, i32 52, i64 64, i64 64, i64 256, i32 0, metadata !21} ; [ DW_TAG_member ]
!419 = metadata !{i32 524301, metadata !409, metadata !"__pad2", metadata !20, i32 53, i64 16, i64 16, i64 320, i32 0, metadata !199} ; [ DW_TAG_member ]
!420 = metadata !{i32 524301, metadata !409, metadata !"st_size", metadata !20, i32 55, i64 32, i64 32, i64 352, i32 0, metadata !421} ; [ DW_TAG_member ]
!421 = metadata !{i32 524310, metadata !22, metadata !"__off_t", metadata !22, i32 142, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!422 = metadata !{i32 524301, metadata !409, metadata !"st_blksize", metadata !20, i32 59, i64 32, i64 32, i64 384, i32 0, metadata !42} ; [ DW_TAG_member ]
!423 = metadata !{i32 524301, metadata !409, metadata !"st_blocks", metadata !20, i32 62, i64 32, i64 32, i64 416, i32 0, metadata !424} ; [ DW_TAG_member ]
!424 = metadata !{i32 524310, metadata !22, metadata !"__blkcnt_t", metadata !22, i32 170, i64 0, i64 0, i64 0, i32 0, metadata !43} ; [ DW_TAG_typedef ]
!425 = metadata !{i32 524301, metadata !409, metadata !"st_atim", metadata !20, i32 73, i64 64, i64 32, i64 448, i32 0, metadata !47} ; [ DW_TAG_member ]
!426 = metadata !{i32 524301, metadata !409, metadata !"st_mtim", metadata !20, i32 74, i64 64, i64 32, i64 512, i32 0, metadata !47} ; [ DW_TAG_member ]
!427 = metadata !{i32 524301, metadata !409, metadata !"st_ctim", metadata !20, i32 75, i64 64, i64 32, i64 576, i32 0, metadata !47} ; [ DW_TAG_member ]
!428 = metadata !{i32 524301, metadata !409, metadata !"__unused4", metadata !20, i32 88, i64 32, i64 32, i64 640, i32 0, metadata !27} ; [ DW_TAG_member ]
!429 = metadata !{i32 524301, metadata !409, metadata !"__unused5", metadata !20, i32 89, i64 32, i64 32, i64 672, i32 0, metadata !27} ; [ DW_TAG_member ]
!430 = metadata !{i32 524544, metadata !431, metadata !"ts", metadata !1, i32 784, metadata !432} ; [ DW_TAG_auto_variable ]
!431 = metadata !{i32 524299, metadata !407, i32 784, i32 0, metadata !1, i32 52} ; [ DW_TAG_lexical_block ]
!432 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !433} ; [ DW_TAG_pointer_type ]
!433 = metadata !{i32 524307, metadata !1, metadata !"termios", metadata !434, i32 30, i64 480, i64 32, i64 0, i32 0, null, metadata !435, i32 0, null} ; [ DW_TAG_structure_type ]
!434 = metadata !{i32 524329, metadata !"termios.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!435 = metadata !{metadata !436, metadata !438, metadata !439, metadata !440, metadata !441, metadata !443, metadata !445, metadata !447}
!436 = metadata !{i32 524301, metadata !433, metadata !"c_iflag", metadata !434, i32 31, i64 32, i64 32, i64 0, i32 0, metadata !437} ; [ DW_TAG_member ]
!437 = metadata !{i32 524310, metadata !434, metadata !"tcflag_t", metadata !434, i32 30, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!438 = metadata !{i32 524301, metadata !433, metadata !"c_oflag", metadata !434, i32 32, i64 32, i64 32, i64 32, i32 0, metadata !437} ; [ DW_TAG_member ]
!439 = metadata !{i32 524301, metadata !433, metadata !"c_cflag", metadata !434, i32 33, i64 32, i64 32, i64 64, i32 0, metadata !437} ; [ DW_TAG_member ]
!440 = metadata !{i32 524301, metadata !433, metadata !"c_lflag", metadata !434, i32 34, i64 32, i64 32, i64 96, i32 0, metadata !437} ; [ DW_TAG_member ]
!441 = metadata !{i32 524301, metadata !433, metadata !"c_line", metadata !434, i32 35, i64 8, i64 8, i64 128, i32 0, metadata !442} ; [ DW_TAG_member ]
!442 = metadata !{i32 524310, metadata !434, metadata !"cc_t", metadata !434, i32 25, i64 0, i64 0, i64 0, i32 0, metadata !201} ; [ DW_TAG_typedef ]
!443 = metadata !{i32 524301, metadata !433, metadata !"c_cc", metadata !434, i32 36, i64 256, i64 8, i64 136, i32 0, metadata !444} ; [ DW_TAG_member ]
!444 = metadata !{i32 524289, metadata !1, metadata !"", metadata !1, i32 0, i64 256, i64 8, i64 0, i32 0, metadata !442, metadata !171, i32 0, null} ; [ DW_TAG_array_type ]
!445 = metadata !{i32 524301, metadata !433, metadata !"c_ispeed", metadata !434, i32 37, i64 32, i64 32, i64 416, i32 0, metadata !446} ; [ DW_TAG_member ]
!446 = metadata !{i32 524310, metadata !434, metadata !"speed_t", metadata !434, i32 26, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!447 = metadata !{i32 524301, metadata !433, metadata !"c_ospeed", metadata !434, i32 38, i64 32, i64 32, i64 448, i32 0, metadata !446} ; [ DW_TAG_member ]
!448 = metadata !{i32 524544, metadata !449, metadata !"ws", metadata !1, i32 853, metadata !450} ; [ DW_TAG_auto_variable ]
!449 = metadata !{i32 524299, metadata !407, i32 853, i32 0, metadata !1, i32 53} ; [ DW_TAG_lexical_block ]
!450 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !451} ; [ DW_TAG_pointer_type ]
!451 = metadata !{i32 524307, metadata !1, metadata !"winsize", metadata !452, i32 29, i64 64, i64 16, i64 0, i32 0, null, metadata !453, i32 0, null} ; [ DW_TAG_structure_type ]
!452 = metadata !{i32 524329, metadata !"ioctl-types.h", metadata !"/usr/include/i386-linux-gnu/bits", metadata !2} ; [ DW_TAG_file_type ]
!453 = metadata !{metadata !454, metadata !455, metadata !456, metadata !457}
!454 = metadata !{i32 524301, metadata !451, metadata !"ws_row", metadata !452, i32 30, i64 16, i64 16, i64 0, i32 0, metadata !199} ; [ DW_TAG_member ]
!455 = metadata !{i32 524301, metadata !451, metadata !"ws_col", metadata !452, i32 31, i64 16, i64 16, i64 16, i32 0, metadata !199} ; [ DW_TAG_member ]
!456 = metadata !{i32 524301, metadata !451, metadata !"ws_xpixel", metadata !452, i32 32, i64 16, i64 16, i64 32, i32 0, metadata !199} ; [ DW_TAG_member ]
!457 = metadata !{i32 524301, metadata !451, metadata !"ws_ypixel", metadata !452, i32 33, i64 16, i64 16, i64 48, i32 0, metadata !199} ; [ DW_TAG_member ]
!458 = metadata !{i32 524544, metadata !459, metadata !"res", metadata !1, i32 876, metadata !460} ; [ DW_TAG_auto_variable ]
!459 = metadata !{i32 524299, metadata !407, i32 876, i32 0, metadata !1, i32 54} ; [ DW_TAG_lexical_block ]
!460 = metadata !{i32 524303, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !67} ; [ DW_TAG_pointer_type ]
!461 = metadata !{i32 524544, metadata !462, metadata !"r", metadata !1, i32 901, metadata !67} ; [ DW_TAG_auto_variable ]
!462 = metadata !{i32 524299, metadata !403, i32 901, i32 0, metadata !1, i32 55} ; [ DW_TAG_lexical_block ]
!463 = metadata !{i32 524545, metadata !189, metadata !"fd", metadata !1, i32 676, metadata !11} ; [ DW_TAG_arg_variable ]
!464 = metadata !{i32 524545, metadata !189, metadata !"dirp", metadata !1, i32 676, metadata !192} ; [ DW_TAG_arg_variable ]
!465 = metadata !{i32 524545, metadata !189, metadata !"count", metadata !1, i32 676, metadata !11} ; [ DW_TAG_arg_variable ]
!466 = metadata !{i32 524544, metadata !467, metadata !"f", metadata !1, i32 677, metadata !62} ; [ DW_TAG_auto_variable ]
!467 = metadata !{i32 524299, metadata !189, i32 676, i32 0, metadata !1, i32 56} ; [ DW_TAG_lexical_block ]
!468 = metadata !{i32 524544, metadata !469, metadata !"i", metadata !1, i32 691, metadata !11} ; [ DW_TAG_auto_variable ]
!469 = metadata !{i32 524299, metadata !467, i32 691, i32 0, metadata !1, i32 57} ; [ DW_TAG_lexical_block ]
!470 = metadata !{i32 524544, metadata !469, metadata !"pad", metadata !1, i32 691, metadata !11} ; [ DW_TAG_auto_variable ]
!471 = metadata !{i32 524544, metadata !469, metadata !"bytes", metadata !1, i32 691, metadata !11} ; [ DW_TAG_auto_variable ]
!472 = metadata !{i32 524544, metadata !473, metadata !"df", metadata !1, i32 701, metadata !5} ; [ DW_TAG_auto_variable ]
!473 = metadata !{i32 524299, metadata !469, i32 701, i32 0, metadata !1, i32 58} ; [ DW_TAG_lexical_block ]
!474 = metadata !{i32 524544, metadata !475, metadata !"os_pos", metadata !1, i32 723, metadata !11} ; [ DW_TAG_auto_variable ]
!475 = metadata !{i32 524299, metadata !467, i32 723, i32 0, metadata !1, i32 59} ; [ DW_TAG_lexical_block ]
!476 = metadata !{i32 524544, metadata !475, metadata !"res", metadata !1, i32 724, metadata !67} ; [ DW_TAG_auto_variable ]
!477 = metadata !{i32 524544, metadata !475, metadata !"s", metadata !1, i32 724, metadata !67} ; [ DW_TAG_auto_variable ]
!478 = metadata !{i32 524544, metadata !479, metadata !"pos", metadata !1, i32 740, metadata !67} ; [ DW_TAG_auto_variable ]
!479 = metadata !{i32 524299, metadata !475, i32 740, i32 0, metadata !1, i32 60} ; [ DW_TAG_lexical_block ]
!480 = metadata !{i32 524544, metadata !481, metadata !"dp", metadata !1, i32 747, metadata !192} ; [ DW_TAG_auto_variable ]
!481 = metadata !{i32 524299, metadata !479, i32 747, i32 0, metadata !1, i32 61} ; [ DW_TAG_lexical_block ]
!482 = metadata !{i32 524545, metadata !206, metadata !"fd", metadata !1, i32 373, metadata !67} ; [ DW_TAG_arg_variable ]
!483 = metadata !{i32 524545, metadata !206, metadata !"offset", metadata !1, i32 373, metadata !70} ; [ DW_TAG_arg_variable ]
!484 = metadata !{i32 524545, metadata !206, metadata !"whence", metadata !1, i32 373, metadata !67} ; [ DW_TAG_arg_variable ]
!485 = metadata !{i32 524544, metadata !486, metadata !"new_off", metadata !1, i32 374, metadata !70} ; [ DW_TAG_auto_variable ]
!486 = metadata !{i32 524299, metadata !206, i32 373, i32 0, metadata !1, i32 62} ; [ DW_TAG_lexical_block ]
!487 = metadata !{i32 524544, metadata !486, metadata !"f", metadata !1, i32 375, metadata !62} ; [ DW_TAG_auto_variable ]
!488 = metadata !{i32 524545, metadata !209, metadata !"fd", metadata !1, i32 620, metadata !67} ; [ DW_TAG_arg_variable ]
!489 = metadata !{i32 524545, metadata !209, metadata !"buf", metadata !1, i32 620, metadata !16} ; [ DW_TAG_arg_variable ]
!490 = metadata !{i32 524544, metadata !491, metadata !"f", metadata !1, i32 621, metadata !62} ; [ DW_TAG_auto_variable ]
!491 = metadata !{i32 524299, metadata !209, i32 620, i32 0, metadata !1, i32 63} ; [ DW_TAG_lexical_block ]
!492 = metadata !{i32 524544, metadata !493, metadata !"r", metadata !1, i32 632, metadata !67} ; [ DW_TAG_auto_variable ]
!493 = metadata !{i32 524299, metadata !491, i32 632, i32 0, metadata !1, i32 64} ; [ DW_TAG_lexical_block ]
!494 = metadata !{i32 524545, metadata !210, metadata !"path", metadata !1, i32 449, metadata !57} ; [ DW_TAG_arg_variable ]
!495 = metadata !{i32 524545, metadata !210, metadata !"buf", metadata !1, i32 449, metadata !16} ; [ DW_TAG_arg_variable ]
!496 = metadata !{i32 524544, metadata !497, metadata !"dfile", metadata !1, i32 450, metadata !5} ; [ DW_TAG_auto_variable ]
!497 = metadata !{i32 524299, metadata !210, i32 449, i32 0, metadata !1, i32 65} ; [ DW_TAG_lexical_block ]
!498 = metadata !{i32 524544, metadata !499, metadata !"r", metadata !1, i32 460, metadata !67} ; [ DW_TAG_auto_variable ]
!499 = metadata !{i32 524299, metadata !497, i32 460, i32 0, metadata !1, i32 66} ; [ DW_TAG_lexical_block ]
!500 = metadata !{i32 524545, metadata !213, metadata !"path", metadata !1, i32 430, metadata !57} ; [ DW_TAG_arg_variable ]
!501 = metadata !{i32 524545, metadata !213, metadata !"buf", metadata !1, i32 430, metadata !16} ; [ DW_TAG_arg_variable ]
!502 = metadata !{i32 524544, metadata !503, metadata !"dfile", metadata !1, i32 431, metadata !5} ; [ DW_TAG_auto_variable ]
!503 = metadata !{i32 524299, metadata !213, i32 430, i32 0, metadata !1, i32 67} ; [ DW_TAG_lexical_block ]
!504 = metadata !{i32 524544, metadata !505, metadata !"r", metadata !1, i32 441, metadata !67} ; [ DW_TAG_auto_variable ]
!505 = metadata !{i32 524299, metadata !503, i32 441, i32 0, metadata !1, i32 68} ; [ DW_TAG_lexical_block ]
!506 = metadata !{i32 524545, metadata !214, metadata !"fd", metadata !1, i32 233, metadata !67} ; [ DW_TAG_arg_variable ]
!507 = metadata !{i32 524545, metadata !214, metadata !"buf", metadata !1, i32 233, metadata !139} ; [ DW_TAG_arg_variable ]
!508 = metadata !{i32 524545, metadata !214, metadata !"count", metadata !1, i32 233, metadata !94} ; [ DW_TAG_arg_variable ]
!509 = metadata !{i32 524544, metadata !510, metadata !"f", metadata !1, i32 235, metadata !62} ; [ DW_TAG_auto_variable ]
!510 = metadata !{i32 524299, metadata !214, i32 233, i32 0, metadata !1, i32 69} ; [ DW_TAG_lexical_block ]
!511 = metadata !{i32 524544, metadata !512, metadata !"r", metadata !1, i32 262, metadata !67} ; [ DW_TAG_auto_variable ]
!512 = metadata !{i32 524299, metadata !510, i32 263, i32 0, metadata !1, i32 70} ; [ DW_TAG_lexical_block ]
!513 = metadata !{i32 524545, metadata !217, metadata !"df", metadata !1, i32 507, metadata !5} ; [ DW_TAG_arg_variable ]
!514 = metadata !{i32 524545, metadata !217, metadata !"mode", metadata !1, i32 507, metadata !76} ; [ DW_TAG_arg_variable ]
!515 = metadata !{i32 524545, metadata !220, metadata !"fd", metadata !1, i32 542, metadata !67} ; [ DW_TAG_arg_variable ]
!516 = metadata !{i32 524545, metadata !220, metadata !"mode", metadata !1, i32 542, metadata !76} ; [ DW_TAG_arg_variable ]
!517 = metadata !{i32 524544, metadata !518, metadata !"f", metadata !1, i32 545, metadata !62} ; [ DW_TAG_auto_variable ]
!518 = metadata !{i32 524299, metadata !220, i32 542, i32 0, metadata !1, i32 72} ; [ DW_TAG_lexical_block ]
!519 = metadata !{i32 524544, metadata !520, metadata !"r", metadata !1, i32 562, metadata !67} ; [ DW_TAG_auto_variable ]
!520 = metadata !{i32 524299, metadata !518, i32 562, i32 0, metadata !1, i32 73} ; [ DW_TAG_lexical_block ]
!521 = metadata !{i32 524545, metadata !223, metadata !"path", metadata !1, i32 520, metadata !57} ; [ DW_TAG_arg_variable ]
!522 = metadata !{i32 524545, metadata !223, metadata !"mode", metadata !1, i32 520, metadata !76} ; [ DW_TAG_arg_variable ]
!523 = metadata !{i32 524544, metadata !524, metadata !"dfile", metadata !1, i32 523, metadata !5} ; [ DW_TAG_auto_variable ]
!524 = metadata !{i32 524299, metadata !223, i32 520, i32 0, metadata !1, i32 74} ; [ DW_TAG_lexical_block ]
!525 = metadata !{i32 524544, metadata !526, metadata !"r", metadata !1, i32 535, metadata !67} ; [ DW_TAG_auto_variable ]
!526 = metadata !{i32 524299, metadata !524, i32 535, i32 0, metadata !1, i32 75} ; [ DW_TAG_lexical_block ]
!527 = metadata !{i32 524545, metadata !226, metadata !"fd", metadata !1, i32 301, metadata !67} ; [ DW_TAG_arg_variable ]
!528 = metadata !{i32 524545, metadata !226, metadata !"buf", metadata !1, i32 301, metadata !139} ; [ DW_TAG_arg_variable ]
!529 = metadata !{i32 524545, metadata !226, metadata !"count", metadata !1, i32 301, metadata !94} ; [ DW_TAG_arg_variable ]
!530 = metadata !{i32 524544, metadata !531, metadata !"f", metadata !1, i32 303, metadata !62} ; [ DW_TAG_auto_variable ]
!531 = metadata !{i32 524299, metadata !226, i32 301, i32 0, metadata !1, i32 76} ; [ DW_TAG_lexical_block ]
!532 = metadata !{i32 524544, metadata !533, metadata !"r", metadata !1, i32 321, metadata !67} ; [ DW_TAG_auto_variable ]
!533 = metadata !{i32 524299, metadata !531, i32 323, i32 0, metadata !1, i32 77} ; [ DW_TAG_lexical_block ]
!534 = metadata !{i32 524544, metadata !535, metadata !"actual_count", metadata !1, i32 346, metadata !94} ; [ DW_TAG_auto_variable ]
!535 = metadata !{i32 524299, metadata !531, i32 346, i32 0, metadata !1, i32 78} ; [ DW_TAG_lexical_block ]
!536 = metadata !{i32 524545, metadata !227, metadata !"pathname", metadata !1, i32 128, metadata !57} ; [ DW_TAG_arg_variable ]
!537 = metadata !{i32 524545, metadata !227, metadata !"flags", metadata !1, i32 128, metadata !67} ; [ DW_TAG_arg_variable ]
!538 = metadata !{i32 524545, metadata !227, metadata !"mode", metadata !1, i32 128, metadata !76} ; [ DW_TAG_arg_variable ]
!539 = metadata !{i32 524544, metadata !540, metadata !"df", metadata !1, i32 129, metadata !5} ; [ DW_TAG_auto_variable ]
!540 = metadata !{i32 524299, metadata !227, i32 128, i32 0, metadata !1, i32 79} ; [ DW_TAG_lexical_block ]
!541 = metadata !{i32 524544, metadata !540, metadata !"f", metadata !1, i32 130, metadata !62} ; [ DW_TAG_auto_variable ]
!542 = metadata !{i32 524544, metadata !540, metadata !"fd", metadata !1, i32 131, metadata !67} ; [ DW_TAG_auto_variable ]
!543 = metadata !{i32 524544, metadata !544, metadata !"os_fd", metadata !1, i32 181, metadata !67} ; [ DW_TAG_auto_variable ]
!544 = metadata !{i32 524299, metadata !540, i32 181, i32 0, metadata !1, i32 80} ; [ DW_TAG_lexical_block ]
!545 = metadata !{i32 73, i32 0, metadata !157, null}
!546 = metadata !{i32 39, i32 0, metadata !0, metadata !547}
!547 = metadata !{i32 74, i32 0, metadata !348, null}
!548 = metadata !{i32 40, i32 0, metadata !232, metadata !547}
!549 = metadata !{i32 43, i32 0, metadata !232, metadata !547}
!550 = metadata !{i32 46, i32 0, metadata !232, metadata !547}
!551 = metadata !{i32 47, i32 0, metadata !232, metadata !547}
!552 = metadata !{i32 48, i32 0, metadata !235, metadata !547}
!553 = metadata !{null}
!554 = metadata !{i32 49, i32 0, metadata !235, metadata !547}
!555 = metadata !{i32 76, i32 0, metadata !348, null}
!556 = metadata !{i32 1265, i32 0, metadata !146, metadata !557}
!557 = metadata !{i32 81, i32 0, metadata !350, null}
!558 = metadata !{i32 1252, i32 0, metadata !136, metadata !559}
!559 = metadata !{i32 1266, i32 0, metadata !314, metadata !557}
!560 = metadata !{i32 1254, i32 0, metadata !304, metadata !559}
!561 = metadata !{i32 1255, i32 0, metadata !304, metadata !559}
!562 = metadata !{i32 0}
!563 = metadata !{i32 1269, i32 0, metadata !314, metadata !557}
!564 = metadata !{i32 1270, i32 0, metadata !317, metadata !557}
!565 = metadata !{i32 1271, i32 0, metadata !317, metadata !557}
!566 = metadata !{i32 1273, i32 0, metadata !317, metadata !557}
!567 = metadata !{i32 1276, i32 0, metadata !317, metadata !557}
!568 = metadata !{i32 1279, i32 0, metadata !319, metadata !557}
!569 = metadata !{i32 1280, i32 0, metadata !319, metadata !557}
!570 = metadata !{i32 1281, i32 0, metadata !319, metadata !557}
!571 = metadata !{i32 1282, i32 0, metadata !319, metadata !557}
!572 = metadata !{i32 82, i32 0, metadata !350, null}
!573 = metadata !{i32 83, i32 0, metadata !350, null}
!574 = metadata !{i32 79, i32 0, metadata !348, null}
!575 = metadata !{i32 88, i32 0, metadata !73, null}
!576 = metadata !{i32 89, i32 0, metadata !242, null}
!577 = metadata !{i32 90, i32 0, metadata !242, null}
!578 = metadata !{i32 91, i32 0, metadata !242, null}
!579 = metadata !{i32 1294, i32 0, metadata !80, null}
!580 = metadata !{i32 1295, i32 0, metadata !581, null}
!581 = metadata !{i32 524299, metadata !80, i32 1294, i32 0, metadata !1, i32 6} ; [ DW_TAG_lexical_block ]
!582 = metadata !{i32 1296, i32 0, metadata !581, null}
!583 = metadata !{i32 1297, i32 0, metadata !581, null}
!584 = metadata !{i32 1300, i32 0, metadata !581, null}
!585 = metadata !{i32 1304, i32 0, metadata !581, null}
!586 = metadata !{i32 1305, i32 0, metadata !581, null}
!587 = metadata !{i32 1078, i32 0, metadata !83, null}
!588 = metadata !{i32 39, i32 0, metadata !0, metadata !589}
!589 = metadata !{i32 1079, i32 0, metadata !252, null}
!590 = metadata !{i32 40, i32 0, metadata !232, metadata !589}
!591 = metadata !{i32 43, i32 0, metadata !232, metadata !589}
!592 = metadata !{i32 46, i32 0, metadata !232, metadata !589}
!593 = metadata !{i32 47, i32 0, metadata !232, metadata !589}
!594 = metadata !{i32 48, i32 0, metadata !235, metadata !589}
!595 = metadata !{i32 49, i32 0, metadata !235, metadata !589}
!596 = metadata !{i32 1080, i32 0, metadata !252, null}
!597 = metadata !{i32 1082, i32 0, metadata !252, null}
!598 = metadata !{i32 1083, i32 0, metadata !252, null}
!599 = metadata !{i32 1084, i32 0, metadata !252, null}
!600 = metadata !{i32 1085, i32 0, metadata !252, null}
!601 = metadata !{i32 1086, i32 0, metadata !252, null}
!602 = metadata !{i32 1089, i32 0, metadata !252, null}
!603 = metadata !{i32 1094, i32 0, metadata !252, null}
!604 = metadata !{i32 1095, i32 0, metadata !252, null}
!605 = metadata !{i32 1060, i32 0, metadata !84, null}
!606 = metadata !{i32 39, i32 0, metadata !0, metadata !607}
!607 = metadata !{i32 1061, i32 0, metadata !255, null}
!608 = metadata !{i32 40, i32 0, metadata !232, metadata !607}
!609 = metadata !{i32 43, i32 0, metadata !232, metadata !607}
!610 = metadata !{i32 46, i32 0, metadata !232, metadata !607}
!611 = metadata !{i32 47, i32 0, metadata !232, metadata !607}
!612 = metadata !{i32 48, i32 0, metadata !235, metadata !607}
!613 = metadata !{i32 49, i32 0, metadata !235, metadata !607}
!614 = metadata !{i32 1062, i32 0, metadata !255, null}
!615 = metadata !{i32 1064, i32 0, metadata !255, null}
!616 = metadata !{i32 1065, i32 0, metadata !255, null}
!617 = metadata !{i32 1066, i32 0, metadata !255, null}
!618 = metadata !{i32 1068, i32 0, metadata !255, null}
!619 = metadata !{i32 1073, i32 0, metadata !255, null}
!620 = metadata !{i32 1074, i32 0, metadata !255, null}
!621 = metadata !{i32 1099, i32 0, metadata !90, null}
!622 = metadata !{i32 39, i32 0, metadata !0, metadata !623}
!623 = metadata !{i32 1100, i32 0, metadata !263, null}
!624 = metadata !{i32 40, i32 0, metadata !232, metadata !623}
!625 = metadata !{i32 43, i32 0, metadata !232, metadata !623}
!626 = metadata !{i32 46, i32 0, metadata !232, metadata !623}
!627 = metadata !{i32 47, i32 0, metadata !232, metadata !623}
!628 = metadata !{i32 48, i32 0, metadata !235, metadata !623}
!629 = metadata !{i32 49, i32 0, metadata !235, metadata !623}
!630 = metadata !{i32 1101, i32 0, metadata !263, null}
!631 = metadata !{i32 1104, i32 0, metadata !263, null}
!632 = metadata !{i32 1105, i32 0, metadata !263, null}
!633 = metadata !{i32 1106, i32 0, metadata !263, null}
!634 = metadata !{i32 1107, i32 0, metadata !263, null}
!635 = metadata !{i32 1108, i32 0, metadata !263, null}
!636 = metadata !{i32 1109, i32 0, metadata !263, null}
!637 = metadata !{i32 1110, i32 0, metadata !263, null}
!638 = metadata !{i32 1112, i32 0, metadata !263, null}
!639 = metadata !{i32 1116, i32 0, metadata !265, null}
!640 = metadata !{i32 1117, i32 0, metadata !265, null}
!641 = metadata !{i32 1118, i32 0, metadata !265, null}
!642 = metadata !{i32 1000, i32 0, metadata !95, null}
!643 = metadata !{i32 63, i32 0, metadata !59, metadata !644}
!644 = metadata !{i32 1001, i32 0, metadata !268, null}
!645 = metadata !{i32 64, i32 0, metadata !239, metadata !644}
!646 = metadata !{i32 65, i32 0, metadata !238, metadata !644}
!647 = metadata !{i32 66, i32 0, metadata !238, metadata !644}
!648 = metadata !{i32 1003, i32 0, metadata !268, null}
!649 = metadata !{i32 1004, i32 0, metadata !268, null}
!650 = metadata !{i32 1005, i32 0, metadata !268, null}
!651 = metadata !{i32 1006, i32 0, metadata !268, null}
!652 = metadata !{i32 1009, i32 0, metadata !270, null}
!653 = metadata !{i32 1010, i32 0, metadata !270, null}
!654 = metadata !{i32 1011, i32 0, metadata !270, null}
!655 = metadata !{i32 980, i32 0, metadata !98, null}
!656 = metadata !{i32 63, i32 0, metadata !59, metadata !657}
!657 = metadata !{i32 981, i32 0, metadata !274, null}
!658 = metadata !{i32 64, i32 0, metadata !239, metadata !657}
!659 = metadata !{i32 65, i32 0, metadata !238, metadata !657}
!660 = metadata !{i32 66, i32 0, metadata !238, metadata !657}
!661 = metadata !{i32 983, i32 0, metadata !274, null}
!662 = metadata !{i32 984, i32 0, metadata !274, null}
!663 = metadata !{i32 985, i32 0, metadata !274, null}
!664 = metadata !{i32 988, i32 0, metadata !274, null}
!665 = metadata !{i32 989, i32 0, metadata !274, null}
!666 = metadata !{i32 990, i32 0, metadata !274, null}
!667 = metadata !{i32 993, i32 0, metadata !276, null}
!668 = metadata !{i32 994, i32 0, metadata !276, null}
!669 = metadata !{i32 995, i32 0, metadata !276, null}
!670 = metadata !{i32 643, i32 0, metadata !129, null}
!671 = metadata !{i32 63, i32 0, metadata !59, metadata !672}
!672 = metadata !{i32 645, i32 0, metadata !280, null}
!673 = metadata !{i32 64, i32 0, metadata !239, metadata !672}
!674 = metadata !{i32 65, i32 0, metadata !238, metadata !672}
!675 = metadata !{i32 66, i32 0, metadata !238, metadata !672}
!676 = metadata !{i32 647, i32 0, metadata !280, null}
!677 = metadata !{i32 649, i32 0, metadata !280, null}
!678 = metadata !{i32 650, i32 0, metadata !280, null}
!679 = metadata !{i32 651, i32 0, metadata !280, null}
!680 = metadata !{i32 654, i32 0, metadata !280, null}
!681 = metadata !{i32 655, i32 0, metadata !280, null}
!682 = metadata !{i32 656, i32 0, metadata !280, null}
!683 = metadata !{i32 660, i32 0, metadata !280, null}
!684 = metadata !{i32 661, i32 0, metadata !280, null}
!685 = metadata !{i32 662, i32 0, metadata !280, null}
!686 = metadata !{i32 668, i32 0, metadata !282, null}
!687 = metadata !{i32 670, i32 0, metadata !282, null}
!688 = metadata !{i32 671, i32 0, metadata !282, null}
!689 = metadata !{i32 588, i32 0, metadata !132, null}
!690 = metadata !{i32 63, i32 0, metadata !59, metadata !691}
!691 = metadata !{i32 589, i32 0, metadata !294, null}
!692 = metadata !{i32 64, i32 0, metadata !239, metadata !691}
!693 = metadata !{i32 65, i32 0, metadata !238, metadata !691}
!694 = metadata !{i32 66, i32 0, metadata !238, metadata !691}
!695 = metadata !{i32 591, i32 0, metadata !294, null}
!696 = metadata !{i32 592, i32 0, metadata !294, null}
!697 = metadata !{i32 593, i32 0, metadata !294, null}
!698 = metadata !{i32 596, i32 0, metadata !294, null}
!699 = metadata !{%struct.exe_disk_file_t* null}
!700 = metadata !{i32 569, i32 0, metadata !85, metadata !701}
!701 = metadata !{i32 597, i32 0, metadata !294, null}
!702 = metadata !{i32 570, i32 0, metadata !703, metadata !701}
!703 = metadata !{i32 524299, metadata !85, i32 569, i32 0, metadata !1, i32 9} ; [ DW_TAG_lexical_block ]
!704 = metadata !{i32 571, i32 0, metadata !703, metadata !701}
!705 = metadata !{i32 599, i32 0, metadata !296, null}
!706 = metadata !{i32 600, i32 0, metadata !296, null}
!707 = metadata !{i32 601, i32 0, metadata !296, null}
!708 = metadata !{i32 486, i32 0, metadata !135, null}
!709 = metadata !{i32 63, i32 0, metadata !59, metadata !710}
!710 = metadata !{i32 487, i32 0, metadata !299, null}
!711 = metadata !{i32 64, i32 0, metadata !239, metadata !710}
!712 = metadata !{i32 65, i32 0, metadata !238, metadata !710}
!713 = metadata !{i32 66, i32 0, metadata !238, metadata !710}
!714 = metadata !{i32 489, i32 0, metadata !299, null}
!715 = metadata !{i32 490, i32 0, metadata !299, null}
!716 = metadata !{i32 491, i32 0, metadata !299, null}
!717 = metadata !{i32 494, i32 0, metadata !299, null}
!718 = metadata !{i32 495, i32 0, metadata !299, null}
!719 = metadata !{i32 496, i32 0, metadata !299, null}
!720 = metadata !{i32 499, i32 0, metadata !301, null}
!721 = metadata !{i32 500, i32 0, metadata !301, null}
!722 = metadata !{i32 501, i32 0, metadata !301, null}
!723 = metadata !{i32 1217, i32 0, metadata !143, null}
!724 = metadata !{i32 1221, i32 0, metadata !311, null}
!725 = metadata !{i32 1223, i32 0, metadata !311, null}
!726 = metadata !{i32 1224, i32 0, metadata !311, null}
!727 = metadata !{i32 1225, i32 0, metadata !311, null}
!728 = metadata !{i32 1226, i32 0, metadata !311, null}
!729 = metadata !{i32 1229, i32 0, metadata !311, null}
!730 = metadata !{i32 1230, i32 0, metadata !311, null}
!731 = metadata !{i32 1024}
!732 = metadata !{i32 1231, i32 0, metadata !311, null}
!733 = metadata !{i32 1232, i32 0, metadata !311, null}
!734 = metadata !{i32 1252, i32 0, metadata !136, metadata !735}
!735 = metadata !{i32 1235, i32 0, metadata !311, null}
!736 = metadata !{i32 1254, i32 0, metadata !304, metadata !735}
!737 = metadata !{i32 1255, i32 0, metadata !304, metadata !735}
!738 = metadata !{i32 1259, i32 0, metadata !140, metadata !739}
!739 = metadata !{i32 1236, i32 0, metadata !311, null}
!740 = metadata !{i32 1260, i32 0, metadata !307, metadata !739}
!741 = metadata !{i32 1261, i32 0, metadata !307, metadata !739}
!742 = metadata !{i32 1240, i32 0, metadata !311, null}
!743 = metadata !{i32 1241, i32 0, metadata !311, null}
!744 = metadata !{i32 1242, i32 0, metadata !311, null}
!745 = metadata !{i32 1243, i32 0, metadata !311, null}
!746 = metadata !{i32 963, i32 0, metadata !149, null}
!747 = metadata !{i32 39, i32 0, metadata !0, metadata !748}
!748 = metadata !{i32 964, i32 0, metadata !323, null}
!749 = metadata !{i32 40, i32 0, metadata !232, metadata !748}
!750 = metadata !{i32 43, i32 0, metadata !232, metadata !748}
!751 = metadata !{i32 46, i32 0, metadata !232, metadata !748}
!752 = metadata !{i32 47, i32 0, metadata !232, metadata !748}
!753 = metadata !{i32 48, i32 0, metadata !235, metadata !748}
!754 = metadata !{i32 49, i32 0, metadata !235, metadata !748}
!755 = metadata !{i32 965, i32 0, metadata !323, null}
!756 = metadata !{i32 967, i32 0, metadata !323, null}
!757 = metadata !{i32 968, i32 0, metadata !323, null}
!758 = metadata !{i32 969, i32 0, metadata !323, null}
!759 = metadata !{i32 1265, i32 0, metadata !146, metadata !760}
!760 = metadata !{i32 973, i32 0, metadata !325, null}
!761 = metadata !{i32 1252, i32 0, metadata !136, metadata !762}
!762 = metadata !{i32 1266, i32 0, metadata !314, metadata !760}
!763 = metadata !{i32 1254, i32 0, metadata !304, metadata !762}
!764 = metadata !{i32 1255, i32 0, metadata !304, metadata !762}
!765 = metadata !{i32 1269, i32 0, metadata !314, metadata !760}
!766 = metadata !{i32 1270, i32 0, metadata !317, metadata !760}
!767 = metadata !{i32 1271, i32 0, metadata !317, metadata !760}
!768 = metadata !{i32 1273, i32 0, metadata !317, metadata !760}
!769 = metadata !{i32 1276, i32 0, metadata !317, metadata !760}
!770 = metadata !{i32 1279, i32 0, metadata !319, metadata !760}
!771 = metadata !{i32 1280, i32 0, metadata !319, metadata !760}
!772 = metadata !{i32 1281, i32 0, metadata !319, metadata !760}
!773 = metadata !{i32 1282, i32 0, metadata !319, metadata !760}
!774 = metadata !{i32 974, i32 0, metadata !325, null}
!775 = metadata !{i32 975, i32 0, metadata !325, null}
!776 = metadata !{i32 606, i32 0, metadata !152, null}
!777 = metadata !{i32 39, i32 0, metadata !0, metadata !778}
!778 = metadata !{i32 608, i32 0, metadata !330, null}
!779 = metadata !{i32 40, i32 0, metadata !232, metadata !778}
!780 = metadata !{i32 43, i32 0, metadata !232, metadata !778}
!781 = metadata !{i32 46, i32 0, metadata !232, metadata !778}
!782 = metadata !{i32 47, i32 0, metadata !232, metadata !778}
!783 = metadata !{i32 48, i32 0, metadata !235, metadata !778}
!784 = metadata !{i32 49, i32 0, metadata !235, metadata !778}
!785 = metadata !{i32 610, i32 0, metadata !330, null}
!786 = metadata !{i32 569, i32 0, metadata !85, metadata !787}
!787 = metadata !{i32 611, i32 0, metadata !330, null}
!788 = metadata !{i32 570, i32 0, metadata !703, metadata !787}
!789 = metadata !{i32 571, i32 0, metadata !703, metadata !787}
!790 = metadata !{i32 1265, i32 0, metadata !146, metadata !791}
!791 = metadata !{i32 613, i32 0, metadata !332, null}
!792 = metadata !{i32 1252, i32 0, metadata !136, metadata !793}
!793 = metadata !{i32 1266, i32 0, metadata !314, metadata !791}
!794 = metadata !{i32 1254, i32 0, metadata !304, metadata !793}
!795 = metadata !{i32 1255, i32 0, metadata !304, metadata !793}
!796 = metadata !{i32 1269, i32 0, metadata !314, metadata !791}
!797 = metadata !{i32 1270, i32 0, metadata !317, metadata !791}
!798 = metadata !{i32 1271, i32 0, metadata !317, metadata !791}
!799 = metadata !{i32 1273, i32 0, metadata !317, metadata !791}
!800 = metadata !{i32 1276, i32 0, metadata !317, metadata !791}
!801 = metadata !{i32 1279, i32 0, metadata !319, metadata !791}
!802 = metadata !{i32 1280, i32 0, metadata !319, metadata !791}
!803 = metadata !{i32 1281, i32 0, metadata !319, metadata !791}
!804 = metadata !{i32 1282, i32 0, metadata !319, metadata !791}
!805 = metadata !{i32 614, i32 0, metadata !332, null}
!806 = metadata !{i32 615, i32 0, metadata !332, null}
!807 = metadata !{i32 575, i32 0, metadata !155, null}
!808 = metadata !{i32 39, i32 0, metadata !0, metadata !809}
!809 = metadata !{i32 576, i32 0, metadata !337, null}
!810 = metadata !{i32 40, i32 0, metadata !232, metadata !809}
!811 = metadata !{i32 43, i32 0, metadata !232, metadata !809}
!812 = metadata !{i32 46, i32 0, metadata !232, metadata !809}
!813 = metadata !{i32 47, i32 0, metadata !232, metadata !809}
!814 = metadata !{i32 48, i32 0, metadata !235, metadata !809}
!815 = metadata !{i32 49, i32 0, metadata !235, metadata !809}
!816 = metadata !{i32 578, i32 0, metadata !337, null}
!817 = metadata !{i32 569, i32 0, metadata !85, metadata !818}
!818 = metadata !{i32 579, i32 0, metadata !337, null}
!819 = metadata !{i32 570, i32 0, metadata !703, metadata !818}
!820 = metadata !{i32 571, i32 0, metadata !703, metadata !818}
!821 = metadata !{i32 1265, i32 0, metadata !146, metadata !822}
!822 = metadata !{i32 581, i32 0, metadata !339, null}
!823 = metadata !{i32 1252, i32 0, metadata !136, metadata !824}
!824 = metadata !{i32 1266, i32 0, metadata !314, metadata !822}
!825 = metadata !{i32 1254, i32 0, metadata !304, metadata !824}
!826 = metadata !{i32 1255, i32 0, metadata !304, metadata !824}
!827 = metadata !{i32 1269, i32 0, metadata !314, metadata !822}
!828 = metadata !{i32 1270, i32 0, metadata !317, metadata !822}
!829 = metadata !{i32 1271, i32 0, metadata !317, metadata !822}
!830 = metadata !{i32 1273, i32 0, metadata !317, metadata !822}
!831 = metadata !{i32 1276, i32 0, metadata !317, metadata !822}
!832 = metadata !{i32 1279, i32 0, metadata !319, metadata !822}
!833 = metadata !{i32 1280, i32 0, metadata !319, metadata !822}
!834 = metadata !{i32 1281, i32 0, metadata !319, metadata !822}
!835 = metadata !{i32 1282, i32 0, metadata !319, metadata !822}
!836 = metadata !{i32 582, i32 0, metadata !339, null}
!837 = metadata !{i32 583, i32 0, metadata !339, null}
!838 = metadata !{i32 468, i32 0, metadata !156, null}
!839 = metadata !{i32 39, i32 0, metadata !0, metadata !840}
!840 = metadata !{i32 469, i32 0, metadata !342, null}
!841 = metadata !{i32 40, i32 0, metadata !232, metadata !840}
!842 = metadata !{i32 43, i32 0, metadata !232, metadata !840}
!843 = metadata !{i32 46, i32 0, metadata !232, metadata !840}
!844 = metadata !{i32 47, i32 0, metadata !232, metadata !840}
!845 = metadata !{i32 48, i32 0, metadata !235, metadata !840}
!846 = metadata !{i32 49, i32 0, metadata !235, metadata !840}
!847 = metadata !{i32 471, i32 0, metadata !342, null}
!848 = metadata !{i32 473, i32 0, metadata !342, null}
!849 = metadata !{i32 474, i32 0, metadata !342, null}
!850 = metadata !{i32 475, i32 0, metadata !342, null}
!851 = metadata !{i32 1265, i32 0, metadata !146, metadata !852}
!852 = metadata !{i32 479, i32 0, metadata !344, null}
!853 = metadata !{i32 1252, i32 0, metadata !136, metadata !854}
!854 = metadata !{i32 1266, i32 0, metadata !314, metadata !852}
!855 = metadata !{i32 1254, i32 0, metadata !304, metadata !854}
!856 = metadata !{i32 1255, i32 0, metadata !304, metadata !854}
!857 = metadata !{i32 1269, i32 0, metadata !314, metadata !852}
!858 = metadata !{i32 1270, i32 0, metadata !317, metadata !852}
!859 = metadata !{i32 1271, i32 0, metadata !317, metadata !852}
!860 = metadata !{i32 1273, i32 0, metadata !317, metadata !852}
!861 = metadata !{i32 1276, i32 0, metadata !317, metadata !852}
!862 = metadata !{i32 1279, i32 0, metadata !319, metadata !852}
!863 = metadata !{i32 1280, i32 0, metadata !319, metadata !852}
!864 = metadata !{i32 1281, i32 0, metadata !319, metadata !852}
!865 = metadata !{i32 1282, i32 0, metadata !319, metadata !852}
!866 = metadata !{i32 480, i32 0, metadata !344, null}
!867 = metadata !{i32 481, i32 0, metadata !344, null}
!868 = metadata !{i32 1131, i32 0, metadata !160, null}
!869 = metadata !{i32 1132, i32 0, metadata !160, null}
!870 = metadata !{i32 1133, i32 0, metadata !357, null}
!871 = metadata !{i32 1134, i32 0, metadata !357, null}
!872 = metadata !{i32 1136, i32 0, metadata !357, null}
!873 = metadata !{i32 1140, i32 0, metadata !357, null}
!874 = metadata !{i32 1137, i32 0, metadata !357, null}
!875 = metadata !{i32 1138, i32 0, metadata !357, null}
!876 = metadata !{i32 1143, i32 0, metadata !357, null}
!877 = metadata !{i32 1147, i32 0, metadata !357, null}
!878 = metadata !{i32 1144, i32 0, metadata !357, null}
!879 = metadata !{i32 1145, i32 0, metadata !357, null}
!880 = metadata !{i32 1150, i32 0, metadata !357, null}
!881 = metadata !{i32 1154, i32 0, metadata !357, null}
!882 = metadata !{i32 1151, i32 0, metadata !357, null}
!883 = metadata !{i32 1152, i32 0, metadata !357, null}
!884 = metadata !{i32 1157, i32 0, metadata !357, null}
!885 = metadata !{i32 1158, i32 0, metadata !357, null}
!886 = metadata !{i32 1159, i32 0, metadata !357, null}
!887 = metadata !{i32 1162, i32 0, metadata !357, null}
!888 = metadata !{i32 1163, i32 0, metadata !357, null}
!889 = metadata !{i32 64, i32 0, metadata !239, metadata !890}
!890 = metadata !{i32 1164, i32 0, metadata !367, null}
!891 = metadata !{i32 66, i32 0, metadata !238, metadata !890}
!892 = metadata !{i32 63, i32 0, metadata !59, metadata !890}
!893 = metadata !{i32 1166, i32 0, metadata !367, null}
!894 = metadata !{i32 1167, i32 0, metadata !367, null}
!895 = metadata !{i32 1168, i32 0, metadata !367, null}
!896 = metadata !{i32 1170, i32 0, metadata !367, null}
!897 = metadata !{i32 1171, i32 0, metadata !367, null}
!898 = metadata !{i32 1172, i32 0, metadata !367, null}
!899 = metadata !{i32 1173, i32 0, metadata !367, null}
!900 = metadata !{i32 1175, i32 0, metadata !367, null}
!901 = metadata !{i32 1176, i32 0, metadata !367, null}
!902 = metadata !{i32 1177, i32 0, metadata !367, null}
!903 = metadata !{i32 1178, i32 0, metadata !367, null}
!904 = metadata !{i32 1183, i32 0, metadata !357, null}
!905 = metadata !{i32 1186, i32 0, metadata !369, null}
!906 = metadata !{i32 1188, i32 0, metadata !369, null}
!907 = metadata !{i32 1190, i32 0, metadata !369, null}
!908 = metadata !{i32 1193, i32 0, metadata !369, null}
!909 = metadata !{i32 1194, i32 0, metadata !369, null}
!910 = metadata !{i32 1198, i32 0, metadata !369, null}
!911 = metadata !{i32 1201, i32 0, metadata !369, null}
!912 = metadata !{i32 64, i32 0, metadata !239, metadata !913}
!913 = metadata !{i32 1202, i32 0, metadata !372, null}
!914 = metadata !{i32 66, i32 0, metadata !238, metadata !913}
!915 = metadata !{i32 1203, i32 0, metadata !372, null}
!916 = metadata !{i32 1204, i32 0, metadata !372, null}
!917 = metadata !{i32 1205, i32 0, metadata !372, null}
!918 = metadata !{i32 1206, i32 0, metadata !372, null}
!919 = metadata !{i32 201, i32 0, metadata !180, null}
!920 = metadata !{i32 204, i32 0, metadata !375, null}
!921 = metadata !{i32 206, i32 0, metadata !375, null}
!922 = metadata !{i32 63, i32 0, metadata !59, metadata !923}
!923 = metadata !{i32 208, i32 0, metadata !375, null}
!924 = metadata !{i32 64, i32 0, metadata !239, metadata !923}
!925 = metadata !{i32 65, i32 0, metadata !238, metadata !923}
!926 = metadata !{i32 66, i32 0, metadata !238, metadata !923}
!927 = metadata !{i32 209, i32 0, metadata !375, null}
!928 = metadata !{i32 210, i32 0, metadata !375, null}
!929 = metadata !{i32 211, i32 0, metadata !375, null}
!930 = metadata !{i32 214, i32 0, metadata !375, null}
!931 = metadata !{i32 215, i32 0, metadata !375, null}
!932 = metadata !{i32 216, i32 0, metadata !375, null}
!933 = metadata !{i32 228, i32 0, metadata !375, null}
!934 = metadata !{i32 1016, i32 0, metadata !181, null}
!935 = metadata !{i32 63, i32 0, metadata !59, metadata !936}
!936 = metadata !{i32 1017, i32 0, metadata !380, null}
!937 = metadata !{i32 64, i32 0, metadata !239, metadata !936}
!938 = metadata !{i32 65, i32 0, metadata !238, metadata !936}
!939 = metadata !{i32 66, i32 0, metadata !238, metadata !936}
!940 = metadata !{i32 1019, i32 0, metadata !380, null}
!941 = metadata !{i32 1020, i32 0, metadata !380, null}
!942 = metadata !{i32 1021, i32 0, metadata !380, null}
!943 = metadata !{i32 1023, i32 0, metadata !382, null}
!944 = metadata !{i32 1024, i32 0, metadata !382, null}
!945 = metadata !{i32 201, i32 0, metadata !180, metadata !944}
!946 = metadata !{i32 204, i32 0, metadata !375, metadata !944}
!947 = metadata !{i32 206, i32 0, metadata !375, metadata !944}
!948 = metadata !{i32 63, i32 0, metadata !59, metadata !949}
!949 = metadata !{i32 208, i32 0, metadata !375, metadata !944}
!950 = metadata !{i32 64, i32 0, metadata !239, metadata !949}
!951 = metadata !{i32 209, i32 0, metadata !375, metadata !944}
!952 = metadata !{i32 210, i32 0, metadata !375, metadata !944}
!953 = metadata !{i32 214, i32 0, metadata !375, metadata !944}
!954 = metadata !{i32 215, i32 0, metadata !375, metadata !944}
!955 = metadata !{i32 216, i32 0, metadata !375, metadata !944}
!956 = metadata !{i32 228, i32 0, metadata !375, metadata !944}
!957 = metadata !{i32 1028, i32 0, metadata !382, null}
!958 = metadata !{i32 1030, i32 0, metadata !382, null}
!959 = metadata !{i32 1041, i32 0, metadata !184, null}
!960 = metadata !{i32 63, i32 0, metadata !59, metadata !961}
!961 = metadata !{i32 1042, i32 0, metadata !385, null}
!962 = metadata !{i32 64, i32 0, metadata !239, metadata !961}
!963 = metadata !{i32 65, i32 0, metadata !238, metadata !961}
!964 = metadata !{i32 66, i32 0, metadata !238, metadata !961}
!965 = metadata !{i32 1043, i32 0, metadata !385, null}
!966 = metadata !{i32 1044, i32 0, metadata !385, null}
!967 = metadata !{i32 1045, i32 0, metadata !385, null}
!968 = metadata !{i32 1049, i32 0, metadata !387, null}
!969 = metadata !{i32 1048, i32 0, metadata !387, null}
!970 = metadata !{i32 1052, i32 0, metadata !387, null}
!971 = metadata !{i32 1055, i32 0, metadata !387, null}
!972 = metadata !{i32 908, i32 0, metadata !185, null}
!973 = metadata !{i32 910, i32 0, metadata !391, null}
!974 = metadata !{i32 63, i32 0, metadata !59, metadata !975}
!975 = metadata !{i32 909, i32 0, metadata !391, null}
!976 = metadata !{i32 64, i32 0, metadata !239, metadata !975}
!977 = metadata !{i32 65, i32 0, metadata !238, metadata !975}
!978 = metadata !{i32 66, i32 0, metadata !238, metadata !975}
!979 = metadata !{i32 913, i32 0, metadata !391, null}
!980 = metadata !{i32 914, i32 0, metadata !391, null}
!981 = metadata !{i32 915, i32 0, metadata !391, null}
!982 = metadata !{i32 918, i32 0, metadata !391, null}
!983 = metadata !{i32 922, i32 0, metadata !391, null}
!984 = metadata !{i32 923, i32 0, metadata !391, null}
!985 = metadata !{i32 924, i32 0, metadata !391, null}
!986 = metadata !{i32 927, i32 0, metadata !391, null}
!987 = metadata !{i32 928, i32 0, metadata !391, null}
!988 = metadata !{i32 930, i32 0, metadata !397, null}
!989 = metadata !{i32 931, i32 0, metadata !397, null}
!990 = metadata !{i32 1}
!991 = metadata !{i32 932, i32 0, metadata !397, null}
!992 = metadata !{i32 936, i32 0, metadata !391, null}
!993 = metadata !{i32 937, i32 0, metadata !391, null}
!994 = metadata !{i32 938, i32 0, metadata !391, null}
!995 = metadata !{i32 951, i32 0, metadata !391, null}
!996 = metadata !{i32 952, i32 0, metadata !391, null}
!997 = metadata !{i32 956, i32 0, metadata !399, null}
!998 = metadata !{i32 957, i32 0, metadata !399, null}
!999 = metadata !{i32 958, i32 0, metadata !399, null}
!1000 = metadata !{i32 760, i32 0, metadata !186, null}
!1001 = metadata !{i32 763, i32 0, metadata !403, null}
!1002 = metadata !{i32 63, i32 0, metadata !59, metadata !1003}
!1003 = metadata !{i32 762, i32 0, metadata !403, null}
!1004 = metadata !{i32 64, i32 0, metadata !239, metadata !1003}
!1005 = metadata !{i32 65, i32 0, metadata !238, metadata !1003}
!1006 = metadata !{i32 66, i32 0, metadata !238, metadata !1003}
!1007 = metadata !{i32 770, i32 0, metadata !403, null}
!1008 = metadata !{i32 771, i32 0, metadata !403, null}
!1009 = metadata !{i32 772, i32 0, metadata !403, null}
!1010 = metadata !{i32 775, i32 0, metadata !403, null}
!1011 = metadata !{i32 776, i32 0, metadata !403, null}
!1012 = metadata !{i32 777, i32 0, metadata !403, null}
!1013 = metadata !{i32 779, i32 0, metadata !403, null}
!1014 = metadata !{i32 780, i32 0, metadata !407, null}
!1015 = metadata !{i32 782, i32 0, metadata !407, null}
!1016 = metadata !{i32 784, i32 0, metadata !431, null}
!1017 = metadata !{i32 786, i32 0, metadata !431, null}
!1018 = metadata !{i32 789, i32 0, metadata !431, null}
!1019 = metadata !{i32 792, i32 0, metadata !431, null}
!1020 = metadata !{i32 793, i32 0, metadata !431, null}
!1021 = metadata !{i32 794, i32 0, metadata !431, null}
!1022 = metadata !{i32 795, i32 0, metadata !431, null}
!1023 = metadata !{i32 796, i32 0, metadata !431, null}
!1024 = metadata !{i32 797, i32 0, metadata !431, null}
!1025 = metadata !{i32 798, i32 0, metadata !431, null}
!1026 = metadata !{i32 799, i32 0, metadata !431, null}
!1027 = metadata !{i32 800, i32 0, metadata !431, null}
!1028 = metadata !{i32 801, i32 0, metadata !431, null}
!1029 = metadata !{i32 802, i32 0, metadata !431, null}
!1030 = metadata !{i32 803, i32 0, metadata !431, null}
!1031 = metadata !{i32 804, i32 0, metadata !431, null}
!1032 = metadata !{i32 805, i32 0, metadata !431, null}
!1033 = metadata !{i32 806, i32 0, metadata !431, null}
!1034 = metadata !{i32 807, i32 0, metadata !431, null}
!1035 = metadata !{i32 808, i32 0, metadata !431, null}
!1036 = metadata !{i32 809, i32 0, metadata !431, null}
!1037 = metadata !{i32 810, i32 0, metadata !431, null}
!1038 = metadata !{i32 811, i32 0, metadata !431, null}
!1039 = metadata !{i32 812, i32 0, metadata !431, null}
!1040 = metadata !{i32 813, i32 0, metadata !431, null}
!1041 = metadata !{i32 814, i32 0, metadata !431, null}
!1042 = metadata !{i32 815, i32 0, metadata !431, null}
!1043 = metadata !{i32 818, i32 0, metadata !431, null}
!1044 = metadata !{i32 824, i32 0, metadata !407, null}
!1045 = metadata !{i32 825, i32 0, metadata !407, null}
!1046 = metadata !{i32 828, i32 0, metadata !407, null}
!1047 = metadata !{i32 834, i32 0, metadata !407, null}
!1048 = metadata !{i32 835, i32 0, metadata !407, null}
!1049 = metadata !{i32 838, i32 0, metadata !407, null}
!1050 = metadata !{i32 844, i32 0, metadata !407, null}
!1051 = metadata !{i32 845, i32 0, metadata !407, null}
!1052 = metadata !{i32 848, i32 0, metadata !407, null}
!1053 = metadata !{i32 853, i32 0, metadata !449, null}
!1054 = metadata !{i32 854, i32 0, metadata !449, null}
!1055 = metadata !{i32 855, i32 0, metadata !449, null}
!1056 = metadata !{i32 856, i32 0, metadata !449, null}
!1057 = metadata !{i32 857, i32 0, metadata !449, null}
!1058 = metadata !{i32 860, i32 0, metadata !449, null}
!1059 = metadata !{i32 866, i32 0, metadata !407, null}
!1060 = metadata !{i32 867, i32 0, metadata !407, null}
!1061 = metadata !{i32 868, i32 0, metadata !407, null}
!1062 = metadata !{i32 871, i32 0, metadata !407, null}
!1063 = metadata !{i32 876, i32 0, metadata !459, null}
!1064 = metadata !{i32 877, i32 0, metadata !459, null}
!1065 = metadata !{i32 878, i32 0, metadata !459, null}
!1066 = metadata !{i32 879, i32 0, metadata !459, null}
!1067 = metadata !{i32 880, i32 0, metadata !459, null}
!1068 = metadata !{i32 886, i32 0, metadata !459, null}
!1069 = metadata !{i32 891, i32 0, metadata !407, null}
!1070 = metadata !{i32 892, i32 0, metadata !407, null}
!1071 = metadata !{i32 896, i32 0, metadata !407, null}
!1072 = metadata !{i32 897, i32 0, metadata !407, null}
!1073 = metadata !{i32 901, i32 0, metadata !462, null}
!1074 = metadata !{i32 902, i32 0, metadata !462, null}
!1075 = metadata !{i32 903, i32 0, metadata !462, null}
!1076 = metadata !{i32 676, i32 0, metadata !189, null}
!1077 = metadata !{i32 63, i32 0, metadata !59, metadata !1078}
!1078 = metadata !{i32 677, i32 0, metadata !467, null}
!1079 = metadata !{i32 64, i32 0, metadata !239, metadata !1078}
!1080 = metadata !{i32 65, i32 0, metadata !238, metadata !1078}
!1081 = metadata !{i32 66, i32 0, metadata !238, metadata !1078}
!1082 = metadata !{i32 679, i32 0, metadata !467, null}
!1083 = metadata !{i32 680, i32 0, metadata !467, null}
!1084 = metadata !{i32 681, i32 0, metadata !467, null}
!1085 = metadata !{i32 684, i32 0, metadata !467, null}
!1086 = metadata !{i32 685, i32 0, metadata !467, null}
!1087 = metadata !{i32 686, i32 0, metadata !467, null}
!1088 = metadata !{i32 689, i32 0, metadata !467, null}
!1089 = metadata !{i32 691, i32 0, metadata !469, null}
!1090 = metadata !{i32 694, i32 0, metadata !469, null}
!1091 = metadata !{i32 695, i32 0, metadata !469, null}
!1092 = metadata !{i32 700, i32 0, metadata !469, null}
!1093 = metadata !{i32 697, i32 0, metadata !469, null}
!1094 = metadata !{i32 701, i32 0, metadata !473, null}
!1095 = metadata !{i32 702, i32 0, metadata !473, null}
!1096 = metadata !{i32 703, i32 0, metadata !473, null}
!1097 = metadata !{i32 704, i32 0, metadata !473, null}
!1098 = metadata !{i32 705, i32 0, metadata !473, null}
!1099 = metadata !{i32 706, i32 0, metadata !473, null}
!1100 = metadata !{i32 707, i32 0, metadata !473, null}
!1101 = metadata !{i32 708, i32 0, metadata !473, null}
!1102 = metadata !{i32 709, i32 0, metadata !473, null}
!1103 = metadata !{i32 713, i32 0, metadata !469, null}
!1104 = metadata !{i32 714, i32 0, metadata !469, null}
!1105 = metadata !{i32 715, i32 0, metadata !469, null}
!1106 = metadata !{i32 716, i32 0, metadata !469, null}
!1107 = metadata !{i32 717, i32 0, metadata !469, null}
!1108 = metadata !{i32 718, i32 0, metadata !469, null}
!1109 = metadata !{i32 719, i32 0, metadata !469, null}
!1110 = metadata !{i32 720, i32 0, metadata !469, null}
!1111 = metadata !{i32 723, i32 0, metadata !475, null}
!1112 = metadata !{i32 733, i32 0, metadata !475, null}
!1113 = metadata !{i32 734, i32 0, metadata !475, null}
!1114 = metadata !{i32 735, i32 0, metadata !475, null}
!1115 = metadata !{i32 736, i32 0, metadata !475, null}
!1116 = metadata !{i32 737, i32 0, metadata !475, null}
!1117 = metadata !{i32 738, i32 0, metadata !475, null}
!1118 = metadata !{i32 740, i32 0, metadata !479, null}
!1119 = metadata !{i32 742, i32 0, metadata !479, null}
!1120 = metadata !{i32 746, i32 0, metadata !479, null}
!1121 = metadata !{i32 748, i32 0, metadata !481, null}
!1122 = metadata !{i32 749, i32 0, metadata !481, null}
!1123 = metadata !{i32 373, i32 0, metadata !206, null}
!1124 = metadata !{i32 63, i32 0, metadata !59, metadata !1125}
!1125 = metadata !{i32 375, i32 0, metadata !486, null}
!1126 = metadata !{i32 64, i32 0, metadata !239, metadata !1125}
!1127 = metadata !{i32 65, i32 0, metadata !238, metadata !1125}
!1128 = metadata !{i32 66, i32 0, metadata !238, metadata !1125}
!1129 = metadata !{i32 377, i32 0, metadata !486, null}
!1130 = metadata !{i32 378, i32 0, metadata !486, null}
!1131 = metadata !{i32 379, i32 0, metadata !486, null}
!1132 = metadata !{i32 382, i32 0, metadata !486, null}
!1133 = metadata !{i32 389, i32 0, metadata !486, null}
!1134 = metadata !{i32 390, i32 0, metadata !486, null}
!1135 = metadata !{i32 392, i32 0, metadata !486, null}
!1136 = metadata !{i32 396, i32 0, metadata !486, null}
!1137 = metadata !{i32 397, i32 0, metadata !486, null}
!1138 = metadata !{i32 398, i32 0, metadata !486, null}
!1139 = metadata !{i32 402, i32 0, metadata !486, null}
!1140 = metadata !{i32 403, i32 0, metadata !486, null}
!1141 = metadata !{i32 407, i32 0, metadata !486, null}
!1142 = metadata !{i32 411, i32 0, metadata !486, null}
!1143 = metadata !{i32 413, i32 0, metadata !486, null}
!1144 = metadata !{i32 414, i32 0, metadata !486, null}
!1145 = metadata !{i32 416, i32 0, metadata !486, null}
!1146 = metadata !{i32 421, i32 0, metadata !486, null}
!1147 = metadata !{i32 422, i32 0, metadata !486, null}
!1148 = metadata !{i32 426, i32 0, metadata !486, null}
!1149 = metadata !{i32 620, i32 0, metadata !209, null}
!1150 = metadata !{i32 63, i32 0, metadata !59, metadata !1151}
!1151 = metadata !{i32 621, i32 0, metadata !491, null}
!1152 = metadata !{i32 64, i32 0, metadata !239, metadata !1151}
!1153 = metadata !{i32 65, i32 0, metadata !238, metadata !1151}
!1154 = metadata !{i32 66, i32 0, metadata !238, metadata !1151}
!1155 = metadata !{i32 623, i32 0, metadata !491, null}
!1156 = metadata !{i32 624, i32 0, metadata !491, null}
!1157 = metadata !{i32 625, i32 0, metadata !491, null}
!1158 = metadata !{i32 628, i32 0, metadata !491, null}
!1159 = metadata !{i32 632, i32 0, metadata !493, null}
!1160 = metadata !{i32 634, i32 0, metadata !493, null}
!1161 = metadata !{i32 635, i32 0, metadata !493, null}
!1162 = metadata !{i32 639, i32 0, metadata !491, null}
!1163 = metadata !{i32 449, i32 0, metadata !210, null}
!1164 = metadata !{i32 39, i32 0, metadata !0, metadata !1165}
!1165 = metadata !{i32 450, i32 0, metadata !497, null}
!1166 = metadata !{i32 40, i32 0, metadata !232, metadata !1165}
!1167 = metadata !{i32 43, i32 0, metadata !232, metadata !1165}
!1168 = metadata !{i32 46, i32 0, metadata !232, metadata !1165}
!1169 = metadata !{i32 47, i32 0, metadata !232, metadata !1165}
!1170 = metadata !{i32 48, i32 0, metadata !235, metadata !1165}
!1171 = metadata !{i32 49, i32 0, metadata !235, metadata !1165}
!1172 = metadata !{i32 451, i32 0, metadata !497, null}
!1173 = metadata !{i32 452, i32 0, metadata !497, null}
!1174 = metadata !{i32 453, i32 0, metadata !497, null}
!1175 = metadata !{i32 1265, i32 0, metadata !146, metadata !1176}
!1176 = metadata !{i32 460, i32 0, metadata !499, null}
!1177 = metadata !{i32 1252, i32 0, metadata !136, metadata !1178}
!1178 = metadata !{i32 1266, i32 0, metadata !314, metadata !1176}
!1179 = metadata !{i32 1254, i32 0, metadata !304, metadata !1178}
!1180 = metadata !{i32 1255, i32 0, metadata !304, metadata !1178}
!1181 = metadata !{i32 1269, i32 0, metadata !314, metadata !1176}
!1182 = metadata !{i32 1270, i32 0, metadata !317, metadata !1176}
!1183 = metadata !{i32 1271, i32 0, metadata !317, metadata !1176}
!1184 = metadata !{i32 1273, i32 0, metadata !317, metadata !1176}
!1185 = metadata !{i32 1276, i32 0, metadata !317, metadata !1176}
!1186 = metadata !{i32 1279, i32 0, metadata !319, metadata !1176}
!1187 = metadata !{i32 1280, i32 0, metadata !319, metadata !1176}
!1188 = metadata !{i32 1281, i32 0, metadata !319, metadata !1176}
!1189 = metadata !{i32 1282, i32 0, metadata !319, metadata !1176}
!1190 = metadata !{i32 462, i32 0, metadata !499, null}
!1191 = metadata !{i32 463, i32 0, metadata !499, null}
!1192 = metadata !{i32 430, i32 0, metadata !213, null}
!1193 = metadata !{i32 39, i32 0, metadata !0, metadata !1194}
!1194 = metadata !{i32 431, i32 0, metadata !503, null}
!1195 = metadata !{i32 40, i32 0, metadata !232, metadata !1194}
!1196 = metadata !{i32 43, i32 0, metadata !232, metadata !1194}
!1197 = metadata !{i32 46, i32 0, metadata !232, metadata !1194}
!1198 = metadata !{i32 47, i32 0, metadata !232, metadata !1194}
!1199 = metadata !{i32 48, i32 0, metadata !235, metadata !1194}
!1200 = metadata !{i32 49, i32 0, metadata !235, metadata !1194}
!1201 = metadata !{i32 432, i32 0, metadata !503, null}
!1202 = metadata !{i32 433, i32 0, metadata !503, null}
!1203 = metadata !{i32 434, i32 0, metadata !503, null}
!1204 = metadata !{i32 1265, i32 0, metadata !146, metadata !1205}
!1205 = metadata !{i32 441, i32 0, metadata !505, null}
!1206 = metadata !{i32 1252, i32 0, metadata !136, metadata !1207}
!1207 = metadata !{i32 1266, i32 0, metadata !314, metadata !1205}
!1208 = metadata !{i32 1254, i32 0, metadata !304, metadata !1207}
!1209 = metadata !{i32 1255, i32 0, metadata !304, metadata !1207}
!1210 = metadata !{i32 1269, i32 0, metadata !314, metadata !1205}
!1211 = metadata !{i32 1270, i32 0, metadata !317, metadata !1205}
!1212 = metadata !{i32 1271, i32 0, metadata !317, metadata !1205}
!1213 = metadata !{i32 1273, i32 0, metadata !317, metadata !1205}
!1214 = metadata !{i32 1276, i32 0, metadata !317, metadata !1205}
!1215 = metadata !{i32 1279, i32 0, metadata !319, metadata !1205}
!1216 = metadata !{i32 1280, i32 0, metadata !319, metadata !1205}
!1217 = metadata !{i32 1281, i32 0, metadata !319, metadata !1205}
!1218 = metadata !{i32 1282, i32 0, metadata !319, metadata !1205}
!1219 = metadata !{i32 443, i32 0, metadata !505, null}
!1220 = metadata !{i32 444, i32 0, metadata !505, null}
!1221 = metadata !{i32 233, i32 0, metadata !214, null}
!1222 = metadata !{i32 237, i32 0, metadata !510, null}
!1223 = metadata !{i32 239, i32 0, metadata !510, null}
!1224 = metadata !{i32 242, i32 0, metadata !510, null}
!1225 = metadata !{i32 243, i32 0, metadata !510, null}
!1226 = metadata !{i32 240, i32 0, metadata !510, null}
!1227 = metadata !{i32 63, i32 0, metadata !59, metadata !1228}
!1228 = metadata !{i32 247, i32 0, metadata !510, null}
!1229 = metadata !{i32 64, i32 0, metadata !239, metadata !1228}
!1230 = metadata !{i32 65, i32 0, metadata !238, metadata !1228}
!1231 = metadata !{i32 66, i32 0, metadata !238, metadata !1228}
!1232 = metadata !{i32 249, i32 0, metadata !510, null}
!1233 = metadata !{i32 250, i32 0, metadata !510, null}
!1234 = metadata !{i32 254, i32 0, metadata !510, null}
!1235 = metadata !{i32 255, i32 0, metadata !510, null}
!1236 = metadata !{i32 256, i32 0, metadata !510, null}
!1237 = metadata !{i32 260, i32 0, metadata !510, null}
!1238 = metadata !{i32 1252, i32 0, metadata !136, metadata !1239}
!1239 = metadata !{i32 263, i32 0, metadata !512, null}
!1240 = metadata !{i32 1254, i32 0, metadata !304, metadata !1239}
!1241 = metadata !{i32 1255, i32 0, metadata !304, metadata !1239}
!1242 = metadata !{i32 1259, i32 0, metadata !140, metadata !1243}
!1243 = metadata !{i32 264, i32 0, metadata !512, null}
!1244 = metadata !{i32 1260, i32 0, metadata !307, metadata !1243}
!1245 = metadata !{i32 1261, i32 0, metadata !307, metadata !1243}
!1246 = metadata !{i32 268, i32 0, metadata !512, null}
!1247 = metadata !{i32 269, i32 0, metadata !512, null}
!1248 = metadata !{i32 270, i32 0, metadata !512, null}
!1249 = metadata !{i32 272, i32 0, metadata !512, null}
!1250 = metadata !{i32 274, i32 0, metadata !512, null}
!1251 = metadata !{i32 275, i32 0, metadata !512, null}
!1252 = metadata !{i32 279, i32 0, metadata !512, null}
!1253 = metadata !{i32 280, i32 0, metadata !512, null}
!1254 = metadata !{i32 284, i32 0, metadata !510, null}
!1255 = metadata !{i32 285, i32 0, metadata !510, null}
!1256 = metadata !{i32 289, i32 0, metadata !510, null}
!1257 = metadata !{i32 290, i32 0, metadata !510, null}
!1258 = metadata !{i32 293, i32 0, metadata !510, null}
!1259 = metadata !{i32 294, i32 0, metadata !510, null}
!1260 = metadata !{i32 542, i32 0, metadata !220, null}
!1261 = metadata !{i32 63, i32 0, metadata !59, metadata !1262}
!1262 = metadata !{i32 545, i32 0, metadata !518, null}
!1263 = metadata !{i32 64, i32 0, metadata !239, metadata !1262}
!1264 = metadata !{i32 65, i32 0, metadata !238, metadata !1262}
!1265 = metadata !{i32 66, i32 0, metadata !238, metadata !1262}
!1266 = metadata !{i32 547, i32 0, metadata !518, null}
!1267 = metadata !{i32 548, i32 0, metadata !518, null}
!1268 = metadata !{i32 549, i32 0, metadata !518, null}
!1269 = metadata !{i32 552, i32 0, metadata !518, null}
!1270 = metadata !{i32 553, i32 0, metadata !518, null}
!1271 = metadata !{i32 554, i32 0, metadata !518, null}
!1272 = metadata !{i32 555, i32 0, metadata !518, null}
!1273 = metadata !{i32 559, i32 0, metadata !518, null}
!1274 = metadata !{i32 507, i32 0, metadata !217, metadata !1275}
!1275 = metadata !{i32 560, i32 0, metadata !518, null}
!1276 = metadata !{i32 508, i32 0, metadata !1277, metadata !1275}
!1277 = metadata !{i32 524299, metadata !217, i32 507, i32 0, metadata !1, i32 71} ; [ DW_TAG_lexical_block ]
!1278 = metadata !{i32 509, i32 0, metadata !1277, metadata !1275}
!1279 = metadata !{i32 510, i32 0, metadata !1277, metadata !1275}
!1280 = metadata !{i32 511, i32 0, metadata !1277, metadata !1275}
!1281 = metadata !{i32 515, i32 0, metadata !1277, metadata !1275}
!1282 = metadata !{i32 562, i32 0, metadata !520, null}
!1283 = metadata !{i32 563, i32 0, metadata !520, null}
!1284 = metadata !{i32 564, i32 0, metadata !520, null}
!1285 = metadata !{i32 520, i32 0, metadata !223, null}
!1286 = metadata !{i32 39, i32 0, metadata !0, metadata !1287}
!1287 = metadata !{i32 523, i32 0, metadata !524, null}
!1288 = metadata !{i32 40, i32 0, metadata !232, metadata !1287}
!1289 = metadata !{i32 43, i32 0, metadata !232, metadata !1287}
!1290 = metadata !{i32 46, i32 0, metadata !232, metadata !1287}
!1291 = metadata !{i32 47, i32 0, metadata !232, metadata !1287}
!1292 = metadata !{i32 48, i32 0, metadata !235, metadata !1287}
!1293 = metadata !{i32 49, i32 0, metadata !235, metadata !1287}
!1294 = metadata !{i32 525, i32 0, metadata !524, null}
!1295 = metadata !{i32 526, i32 0, metadata !524, null}
!1296 = metadata !{i32 527, i32 0, metadata !524, null}
!1297 = metadata !{i32 528, i32 0, metadata !524, null}
!1298 = metadata !{i32 529, i32 0, metadata !524, null}
!1299 = metadata !{i32 532, i32 0, metadata !524, null}
!1300 = metadata !{i32 507, i32 0, metadata !217, metadata !1301}
!1301 = metadata !{i32 533, i32 0, metadata !524, null}
!1302 = metadata !{i32 508, i32 0, metadata !1277, metadata !1301}
!1303 = metadata !{i32 509, i32 0, metadata !1277, metadata !1301}
!1304 = metadata !{i32 510, i32 0, metadata !1277, metadata !1301}
!1305 = metadata !{i32 511, i32 0, metadata !1277, metadata !1301}
!1306 = metadata !{i32 515, i32 0, metadata !1277, metadata !1301}
!1307 = metadata !{i32 1265, i32 0, metadata !146, metadata !1308}
!1308 = metadata !{i32 535, i32 0, metadata !526, null}
!1309 = metadata !{i32 1252, i32 0, metadata !136, metadata !1310}
!1310 = metadata !{i32 1266, i32 0, metadata !314, metadata !1308}
!1311 = metadata !{i32 1254, i32 0, metadata !304, metadata !1310}
!1312 = metadata !{i32 1255, i32 0, metadata !304, metadata !1310}
!1313 = metadata !{i32 1269, i32 0, metadata !314, metadata !1308}
!1314 = metadata !{i32 1270, i32 0, metadata !317, metadata !1308}
!1315 = metadata !{i32 1271, i32 0, metadata !317, metadata !1308}
!1316 = metadata !{i32 1273, i32 0, metadata !317, metadata !1308}
!1317 = metadata !{i32 1276, i32 0, metadata !317, metadata !1308}
!1318 = metadata !{i32 1279, i32 0, metadata !319, metadata !1308}
!1319 = metadata !{i32 1280, i32 0, metadata !319, metadata !1308}
!1320 = metadata !{i32 1281, i32 0, metadata !319, metadata !1308}
!1321 = metadata !{i32 1282, i32 0, metadata !319, metadata !1308}
!1322 = metadata !{i32 536, i32 0, metadata !526, null}
!1323 = metadata !{i32 537, i32 0, metadata !526, null}
!1324 = metadata !{i32 301, i32 0, metadata !226, null}
!1325 = metadata !{i32 305, i32 0, metadata !531, null}
!1326 = metadata !{i32 63, i32 0, metadata !59, metadata !1327}
!1327 = metadata !{i32 307, i32 0, metadata !531, null}
!1328 = metadata !{i32 64, i32 0, metadata !239, metadata !1327}
!1329 = metadata !{i32 65, i32 0, metadata !238, metadata !1327}
!1330 = metadata !{i32 66, i32 0, metadata !238, metadata !1327}
!1331 = metadata !{i32 309, i32 0, metadata !531, null}
!1332 = metadata !{i32 310, i32 0, metadata !531, null}
!1333 = metadata !{i32 311, i32 0, metadata !531, null}
!1334 = metadata !{i32 314, i32 0, metadata !531, null}
!1335 = metadata !{i32 315, i32 0, metadata !531, null}
!1336 = metadata !{i32 316, i32 0, metadata !531, null}
!1337 = metadata !{i32 320, i32 0, metadata !531, null}
!1338 = metadata !{i32 1252, i32 0, metadata !136, metadata !1339}
!1339 = metadata !{i32 323, i32 0, metadata !533, null}
!1340 = metadata !{i32 1254, i32 0, metadata !304, metadata !1339}
!1341 = metadata !{i32 1255, i32 0, metadata !304, metadata !1339}
!1342 = metadata !{i32 1259, i32 0, metadata !140, metadata !1343}
!1343 = metadata !{i32 324, i32 0, metadata !533, null}
!1344 = metadata !{i32 1260, i32 0, metadata !307, metadata !1343}
!1345 = metadata !{i32 1261, i32 0, metadata !307, metadata !1343}
!1346 = metadata !{i32 328, i32 0, metadata !533, null}
!1347 = metadata !{i32 329, i32 0, metadata !533, null}
!1348 = metadata !{i32 330, i32 0, metadata !533, null}
!1349 = metadata !{i32 331, i32 0, metadata !533, null}
!1350 = metadata !{i32 333, i32 0, metadata !533, null}
!1351 = metadata !{i32 334, i32 0, metadata !533, null}
!1352 = metadata !{i32 338, i32 0, metadata !533, null}
!1353 = metadata !{i32 339, i32 0, metadata !533, null}
!1354 = metadata !{i32 340, i32 0, metadata !533, null}
!1355 = metadata !{i32 346, i32 0, metadata !535, null}
!1356 = metadata !{i32 347, i32 0, metadata !535, null}
!1357 = metadata !{i32 350, i32 0, metadata !535, null}
!1358 = metadata !{i32 351, i32 0, metadata !535, null}
!1359 = metadata !{i32 353, i32 0, metadata !535, null}
!1360 = metadata !{i32 354, i32 0, metadata !535, null}
!1361 = metadata !{i32 358, i32 0, metadata !535, null}
!1362 = metadata !{i32 359, i32 0, metadata !535, null}
!1363 = metadata !{i32 361, i32 0, metadata !535, null}
!1364 = metadata !{i32 362, i32 0, metadata !535, null}
!1365 = metadata !{i32 364, i32 0, metadata !535, null}
!1366 = metadata !{i32 365, i32 0, metadata !535, null}
!1367 = metadata !{i32 367, i32 0, metadata !535, null}
!1368 = metadata !{i32 128, i32 0, metadata !227, null}
!1369 = metadata !{i32 133, i32 0, metadata !540, null}
!1370 = metadata !{i32 134, i32 0, metadata !540, null}
!1371 = metadata !{i32 137, i32 0, metadata !540, null}
!1372 = metadata !{i32 138, i32 0, metadata !540, null}
!1373 = metadata !{i32 141, i32 0, metadata !540, null}
!1374 = metadata !{i32 144, i32 0, metadata !540, null}
!1375 = metadata !{i32 39, i32 0, metadata !0, metadata !1376}
!1376 = metadata !{i32 146, i32 0, metadata !540, null}
!1377 = metadata !{i32 40, i32 0, metadata !232, metadata !1376}
!1378 = metadata !{i32 43, i32 0, metadata !232, metadata !1376}
!1379 = metadata !{i32 46, i32 0, metadata !232, metadata !1376}
!1380 = metadata !{i32 47, i32 0, metadata !232, metadata !1376}
!1381 = metadata !{i32 48, i32 0, metadata !235, metadata !1376}
!1382 = metadata !{i32 49, i32 0, metadata !235, metadata !1376}
!1383 = metadata !{i32 147, i32 0, metadata !540, null}
!1384 = metadata !{i32 150, i32 0, metadata !540, null}
!1385 = metadata !{i32 152, i32 0, metadata !540, null}
!1386 = metadata !{i32 153, i32 0, metadata !540, null}
!1387 = metadata !{i32 165, i32 0, metadata !540, null}
!1388 = metadata !{i32 168, i32 0, metadata !540, null}
!1389 = metadata !{i32 169, i32 0, metadata !540, null}
!1390 = metadata !{i32 173, i32 0, metadata !540, null}
!1391 = metadata !{i32 97, i32 0, metadata !77, metadata !1390}
!1392 = metadata !{i32 99, i32 0, metadata !246, metadata !1390}
!1393 = metadata !{i32 101, i32 0, metadata !246, metadata !1390}
!1394 = metadata !{i32 103, i32 0, metadata !246, metadata !1390}
!1395 = metadata !{i32 107, i32 0, metadata !246, metadata !1390}
!1396 = metadata !{i32 118, i32 0, metadata !246, metadata !1390}
!1397 = metadata !{i32 121, i32 0, metadata !246, metadata !1390}
!1398 = metadata !{i32 174, i32 0, metadata !540, null}
!1399 = metadata !{i32 178, i32 0, metadata !540, null}
!1400 = metadata !{i32 1265, i32 0, metadata !146, metadata !1401}
!1401 = metadata !{i32 181, i32 0, metadata !544, null}
!1402 = metadata !{i32 1252, i32 0, metadata !136, metadata !1403}
!1403 = metadata !{i32 1266, i32 0, metadata !314, metadata !1401}
!1404 = metadata !{i32 1254, i32 0, metadata !304, metadata !1403}
!1405 = metadata !{i32 1255, i32 0, metadata !304, metadata !1403}
!1406 = metadata !{i32 1269, i32 0, metadata !314, metadata !1401}
!1407 = metadata !{i32 1270, i32 0, metadata !317, metadata !1401}
!1408 = metadata !{i32 1271, i32 0, metadata !317, metadata !1401}
!1409 = metadata !{i32 1273, i32 0, metadata !317, metadata !1401}
!1410 = metadata !{i32 1276, i32 0, metadata !317, metadata !1401}
!1411 = metadata !{i32 1279, i32 0, metadata !319, metadata !1401}
!1412 = metadata !{i32 1280, i32 0, metadata !319, metadata !1401}
!1413 = metadata !{i32 1281, i32 0, metadata !319, metadata !1401}
!1414 = metadata !{i32 1282, i32 0, metadata !319, metadata !1401}
!1415 = metadata !{i32 182, i32 0, metadata !544, null}
!1416 = metadata !{i32 183, i32 0, metadata !544, null}
!1417 = metadata !{i32 186, i32 0, metadata !544, null}
!1418 = metadata !{i32 189, i32 0, metadata !540, null}
!1419 = metadata !{i32 190, i32 0, metadata !540, null}
!1420 = metadata !{i32 191, i32 0, metadata !540, null}
!1421 = metadata !{i32 193, i32 0, metadata !540, null}
!1422 = metadata !{i32 195, i32 0, metadata !540, null}
